package org.jeecg.weixin.common.api;

import org.jeecg.weixin.common.error.WxErrorException;

/**
 * WxErrorException处理器.
 *
 * @author Daniel Qian
 */
public interface WxErrorExceptionHandler {

	void handle(WxErrorException e);

}
