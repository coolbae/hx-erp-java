package org.jeecg.codegenerate.generate.pojo.onetomany;

import java.util.Objects;
import java.util.StringJoiner;

public class ForeignKeyInfo {
    private String columnName;
    private String mainTableName;
    private String mainTableColumn;
    private String dbColumnName;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getMainTableName() {
        return mainTableName;
    }

    public void setMainTableName(String mainTableName) {
        this.mainTableName = mainTableName;
    }

    public String getMainTableColumn() {
        return mainTableColumn;
    }

    public void setMainTableColumn(String mainTableColumn) {
        this.mainTableColumn = mainTableColumn;
    }

    public String getDbColumnName() {
        return dbColumnName;
    }

    public void setDbColumnName(String dbColumnName) {
        this.dbColumnName = dbColumnName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ForeignKeyInfo info = (ForeignKeyInfo) o;
        return Objects.equals(columnName, info.columnName) &&
                Objects.equals(mainTableName, info.mainTableName) &&
                Objects.equals(mainTableColumn, info.mainTableColumn) &&
                Objects.equals(dbColumnName, info.dbColumnName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(columnName, mainTableName, mainTableColumn, dbColumnName);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ForeignKeyInfo.class.getSimpleName() + "[", "]")
                .add("columnName='" + columnName + "'")
                .add("mainTableName='" + mainTableName + "'")
                .add("mainTableColumn='" + mainTableColumn + "'")
                .add("dbColumnName='" + dbColumnName + "'")
                .toString();
    }
}