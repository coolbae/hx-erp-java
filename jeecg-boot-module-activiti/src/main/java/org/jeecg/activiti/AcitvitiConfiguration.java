package org.jeecg.activiti;

import java.util.Collections;
import java.util.List;
import org.activiti.api.process.runtime.ProcessAdminRuntime;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.api.process.runtime.conf.ProcessRuntimeConfiguration;
import org.activiti.api.process.runtime.events.ProcessUpdatedEvent;
import org.activiti.api.process.runtime.events.listener.ProcessRuntimeEventListener;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.runtime.api.model.impl.APIProcessDefinitionConverter;
import org.activiti.runtime.api.model.impl.APIProcessInstanceConverter;
import org.activiti.runtime.api.model.impl.APIVariableInstanceConverter;
import org.jeecg.activiti.api.process.runtime.JeecgProcessAdminRuntimeImpl;
import org.jeecg.activiti.api.process.runtime.JeecgProcessRuntimeImpl;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigurationPackage
@ComponentScan
@MapperScan({"org.jeecg.activiti.mapper"})
	public class AcitvitiConfiguration {

	@Bean("jeecgActivitiProcessRuntime")
	public ProcessRuntime processRuntime(RepositoryService repositoryService,
			APIProcessDefinitionConverter processDefinitionConverter, RuntimeService runtimeService,
			APIProcessInstanceConverter processInstanceConverter,
			APIVariableInstanceConverter variableInstanceConverter,
			ProcessRuntimeConfiguration processRuntimeConfiguration,
			@Autowired(required = false) List<ProcessRuntimeEventListener<ProcessUpdatedEvent>> listeners) {
		return new JeecgProcessRuntimeImpl(repositoryService, processDefinitionConverter, runtimeService,
				processInstanceConverter, variableInstanceConverter,
				processRuntimeConfiguration, getInitializedProcessRuntimeEventListeners(listeners));
	}
	
	@Bean("jeecgActivitiProcessAdminRuntime")
    public ProcessAdminRuntime processAdminRuntime(RepositoryService repositoryService,
                                                   APIProcessDefinitionConverter processDefinitionConverter,
                                                   RuntimeService runtimeService,
                                                   APIProcessInstanceConverter processInstanceConverter) {
        return new JeecgProcessAdminRuntimeImpl(repositoryService,
                processDefinitionConverter,
                runtimeService,
                processInstanceConverter
        );
    }

	private <T> List<T> getInitializedProcessRuntimeEventListeners(List<T> processRuntimeEventListeners) {
		return processRuntimeEventListeners != null ? processRuntimeEventListeners : Collections.emptyList();
	}
	
}
