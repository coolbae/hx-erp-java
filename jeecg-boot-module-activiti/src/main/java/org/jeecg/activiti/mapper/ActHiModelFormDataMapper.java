package org.jeecg.activiti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.activiti.entity.ActHiModelFormData;

/**
 * TODO 流程表单数据历史表
 *
 * @author dousw
 * @version 1.0
 * @date 2020/9/15 11:02
 */
public interface ActHiModelFormDataMapper extends BaseMapper<ActHiModelFormData> {
}
