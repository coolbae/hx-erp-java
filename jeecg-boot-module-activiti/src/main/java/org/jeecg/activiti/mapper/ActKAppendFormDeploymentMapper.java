package org.jeecg.activiti.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.activiti.entity.ActKAppendFormDeployment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface ActKAppendFormDeploymentMapper extends BaseMapper<ActKAppendFormDeployment> {

}
