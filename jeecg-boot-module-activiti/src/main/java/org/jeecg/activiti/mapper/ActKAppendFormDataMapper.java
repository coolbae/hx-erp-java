package org.jeecg.activiti.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.activiti.entity.ActKAppendFormData;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * TODO 流程节点附加表单数据
 *
 * @author dousw
 * @version 1.0
 * @date 2020/10/10 10:41
 */
@Mapper
public interface ActKAppendFormDataMapper extends BaseMapper<ActKAppendFormData> {
}
