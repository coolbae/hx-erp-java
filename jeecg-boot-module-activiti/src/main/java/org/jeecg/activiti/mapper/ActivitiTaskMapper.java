package org.jeecg.activiti.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.activiti.entity.ActivitiTaskVo;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

public interface ActivitiTaskMapper extends BaseMapper<ActivitiTaskVo> {

	List<ActivitiTaskVo> getMyTaskList(Page<ActivitiTaskVo> page, @Param("activitiTaskVo")ActivitiTaskVo activitiTaskVo);

}
