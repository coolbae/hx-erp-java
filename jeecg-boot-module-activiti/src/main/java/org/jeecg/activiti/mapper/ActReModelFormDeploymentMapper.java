package org.jeecg.activiti.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.activiti.entity.ActReModelFormDeployment;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface ActReModelFormDeploymentMapper extends BaseMapper<ActReModelFormDeployment> {

}
