package org.jeecg.activiti.entity;

import java.util.Map;

import lombok.Data;

@Data
public class ProcessParams {
	
	private String processInstanceId;
	
	private String processDefinitionKey;
	
	private Map<String,Object> parameter;

}
