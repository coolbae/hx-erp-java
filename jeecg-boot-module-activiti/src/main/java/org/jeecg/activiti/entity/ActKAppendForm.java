package org.jeecg.activiti.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import org.jeecg.common.system.base.entity.JeecgEntity;

/**
 * 流程节点表单管理表
 *
 * @author dousw
 * @version 1.0
 * @date 2020/9/18 14:36
 */

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("act_k_append_form")
public class ActKAppendForm extends JeecgEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * 节点ID
     */
    private String nodeId;
    /**
     * 附加表单
     */
    private String formJson;
    /**
     * 流程ID
     */
    private String modelId;
    /**
     * 流程定义ID
     */
    private String processDefinitionId;

}
