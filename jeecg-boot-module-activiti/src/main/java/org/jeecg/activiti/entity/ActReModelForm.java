package org.jeecg.activiti.entity;


import java.io.Serializable;

import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 流程模型扩展表
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("act_re_model_form")
public class ActReModelForm extends JeecgEntity implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


    /**
     * 流程模型ID
     */
    private String modelId;

    /**
     * 表单模型
     */
    private String htmlJson;

    /**
     * 主表ID
     */
    private String masterTableId;

    /**
     * 子表ID（可以是多个，是多个的时候用","隔开）
     */
    private String slaveTableId;
    
    private String tableName;
    
    private String formType;
    
    private String flowStatusCol;
}
