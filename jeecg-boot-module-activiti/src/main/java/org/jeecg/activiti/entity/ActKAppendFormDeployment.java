package org.jeecg.activiti.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("act_k_append_form_depl")
public class ActKAppendFormDeployment extends ActKAppendForm implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7216972370652070079L;

	/** 流程定义ID */
	private String processDefinitionId;
	
	/** 流程部署ID */
	private String deploymentId;
	
	/** 流程版本号 */
	private Integer version;

}
