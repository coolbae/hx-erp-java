package org.jeecg.activiti.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import org.jeecg.common.system.base.entity.JeecgEntity;

/**
 * TODO 流程抄送实体类
 *
 * @author dousw
 * @version 1.0
 * @date 2020/11/24 16:06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("act_k_process_duplicate")
public class ActKProcessDuplicate extends JeecgEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 抄送用户
     */
    private String username;

    /**
     * 流程模型ID
     */
    private String modelId;

    /**
     * 流程实例ID
     */
    private String processInstanceId;

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 业务标题
     */
    private String title;
    /**
     * 流程名称
     */
    private String modelName;
    /**
     * 流程key
     */
    private String modelKey;
    /**
     * 发起人
     */
    private String initiator;
    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 结束时间
     */
    private String endTime;
    /**
     * 耗时
     */
    private String useTime;
    
    @TableField(exist = false)
    private ActReModelFormData actReModelFormData;

}
