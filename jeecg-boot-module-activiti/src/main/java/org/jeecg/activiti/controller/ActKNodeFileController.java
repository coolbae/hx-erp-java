package org.jeecg.activiti.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.jeecg.activiti.entity.ActKNodeFile;
import org.jeecg.activiti.service.IActKNodeFileService;
import org.jeecg.common.api.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * TODO 流程任务处理上传附件Controller
 *
 * @author dousw
 * @version 1.0
 * @date 2020/11/28 12:19
 */
@RestController
@RequestMapping("/activiti/nodeFile")
@Api(tags = "流程任务节点附件管理")
public class ActKNodeFileController {

    @Autowired
    private IActKNodeFileService iActKNodeFileService;

    @GetMapping("/getFileData")
    @ApiOperation(value = "查询流程节点附加表单数据", notes = "查询流程节点附加表单数据")
    public Result<Object> getActKAppendFormData(@ApiParam("流程实例id") @RequestParam String processInstanceId,
                                                @ApiParam("流程节点id") @RequestParam String nodeId){
        List<ActKNodeFile> actKNodeFiles = iActKNodeFileService.list(new QueryWrapper<ActKNodeFile>().eq("process_instance_id", processInstanceId)
                .eq("node_id", nodeId));
        return Result.OK(actKNodeFiles);
    }
}
