package org.jeecg.activiti.service;

import org.jeecg.activiti.entity.ActKAppendFormDeployment;

import com.baomidou.mybatisplus.extension.service.IService;

public interface IActKAppendFormDeploymentService extends IService<ActKAppendFormDeployment> {

}
