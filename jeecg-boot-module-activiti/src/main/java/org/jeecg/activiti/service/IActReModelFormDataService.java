package org.jeecg.activiti.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.activiti.entity.ActReModelFormData;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;


public interface IActReModelFormDataService extends IService<ActReModelFormData> {

    /**
     * 查询流程表单数据
     * @param page
     * @param actReModelFormData
     * @return
     */
    Page<ActReModelFormData> pageList(Page<ActReModelFormData> page, @Param("actReModelFormData") ActReModelFormData actReModelFormData);

    /**
     * 处理附加表单
     * @param taskId
     * @param processInstanceId
     */
    List<Map<String, Object>> handleNodeAppendForm(String taskId, String processInstanceId);
}
