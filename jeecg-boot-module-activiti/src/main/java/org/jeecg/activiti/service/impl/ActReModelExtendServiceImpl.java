package org.jeecg.activiti.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.activiti.entity.ActReModelExtend;
import org.jeecg.activiti.entity.ModelExtend;
import org.jeecg.activiti.mapper.ActReModelExtendMapper;
import org.jeecg.activiti.service.IActReModelExtendService;
import org.springframework.stereotype.Service;

@Service
public class ActReModelExtendServiceImpl extends ServiceImpl<ActReModelExtendMapper, ActReModelExtend> implements IActReModelExtendService {
    @Override
    public IPage<ModelExtend> selectListByPage(Page<ActReModelExtend> page, ModelExtend modelE) {
        return this.baseMapper.selectListByPage(page, modelE);
    }
}
