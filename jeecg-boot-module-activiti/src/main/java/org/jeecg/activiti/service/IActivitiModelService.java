package org.jeecg.activiti.service;

import java.util.List;
import java.util.Map;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.repository.ProcessDefinition;
import org.jeecg.activiti.entity.ModelExtend;
import org.jeecg.common.api.vo.Result;

/**
 * 流程模型service
 */
public interface IActivitiModelService {

	public Result<Object> deploy(String modelId) throws Exception;

	/**
	 * 处理流程节点
	 * 
	 * @param model
	 * @param definition
	 */
	public void handelModelNode(String modelId, BpmnModel model, ProcessDefinition definition);
	
	public List<ModelExtend> getModelListAll(ModelExtend modelExtend);

	/**
	 * 根据流程定义key查询数据
	 * @param key
	 * @return
	 */
    List<Map<String, Object>> selectModelByKey(String key);
}
