package org.jeecg.activiti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.activiti.entity.ActKProcessDuplicate;
import org.jeecg.activiti.mapper.ActKProcessDuplicateMapper;
import org.jeecg.activiti.service.IActKProcessDuplicateService;
import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author dousw
 * @version 1.0
 * @date 2020/11/24 16:11
 */

@Service
public class ActKProcessDuplicateServiceImpl extends ServiceImpl<ActKProcessDuplicateMapper, ActKProcessDuplicate> implements IActKProcessDuplicateService {
}
