package org.jeecg.activiti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.activiti.entity.ActKProcessDuplicate;

public interface IActKProcessDuplicateService extends IService<ActKProcessDuplicate> {
}
