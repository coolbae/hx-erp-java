package org.jeecg.kettle;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigurationPackage
@ComponentScan
@MapperScan({"org.jeecg.kettle.**.mapper"})
public class KettleConfiguration {

}
