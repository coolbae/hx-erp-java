package org.jeecg.hr.service;

import java.util.List;

import org.jeecg.hr.entity.SalaryTemplateDetail;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: SalaryTemplateDetail
 * @Author: MxpIO
 */
public interface SalaryTemplateDetailService extends IService<SalaryTemplateDetail> {

	public List<SalaryTemplateDetail> selectByTemplateCode(String templateCode);

	public void handleTransientVariable(List<SalaryTemplateDetail> records);
}
