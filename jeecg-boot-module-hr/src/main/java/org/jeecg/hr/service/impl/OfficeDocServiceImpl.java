package org.jeecg.hr.service.impl;

import org.jeecg.hr.entity.OfficeDoc;
import org.jeecg.hr.mapper.OfficeDocMapper;
import org.jeecg.hr.service.OfficeDocService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class OfficeDocServiceImpl extends ServiceImpl<OfficeDocMapper, OfficeDoc> implements OfficeDocService {

}
