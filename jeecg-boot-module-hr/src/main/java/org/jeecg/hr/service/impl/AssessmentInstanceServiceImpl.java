package org.jeecg.hr.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jeecg.hr.entity.AssessmentIndex;
import org.jeecg.hr.entity.AssessmentInstance;
import org.jeecg.hr.entity.AssessmentPlan;
import org.jeecg.hr.entity.AssessmentProc;
import org.jeecg.hr.entity.AssessmentTask;
import org.jeecg.hr.entity.AssessmentTaskResult;
import org.jeecg.hr.entity.AssessmentUser;
import org.jeecg.hr.mapper.AssessmentIndexMapper;
import org.jeecg.hr.mapper.AssessmentInstanceMapper;
import org.jeecg.hr.mapper.AssessmentPlanMapper;
import org.jeecg.hr.mapper.AssessmentProcMapper;
import org.jeecg.hr.mapper.AssessmentTaskMapper;
import org.jeecg.hr.mapper.AssessmentTaskResultMapper;
import org.jeecg.hr.mapper.AssessmentUserMapper;
import org.jeecg.hr.service.AssessmentInstanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import cn.hutool.core.lang.UUID;

@Service
public class AssessmentInstanceServiceImpl extends ServiceImpl<AssessmentInstanceMapper, AssessmentInstance> implements AssessmentInstanceService {

	@Autowired
	private AssessmentInstanceMapper assessmentInstanceMapper;
	
	@Autowired
	private AssessmentTaskMapper assessmentTaskMapper;
	
	@Autowired
	private AssessmentTaskResultMapper assessmentTaskResultMapper;
	
	@Autowired
	private AssessmentPlanMapper assessmentPlanMapper;
	
	@Autowired
	private AssessmentIndexMapper assessmentIndexMapper;
	
	@Autowired
	private AssessmentProcMapper assessmentProcMapper;
	
	@Autowired
	private AssessmentUserMapper assessmentUserMapper;
	
	@Override
	public void handleDetails(IPage<AssessmentInstance> pageList) {
		
		for(AssessmentInstance instance : pageList.getRecords()){
			List<AssessmentTask> tasks = assessmentTaskMapper.selectByInstanceCode(instance.getInstanceCode());
			instance.setPlanQuantity(tasks.size());
			instance.setCompleteQuantity(assessmentTaskMapper.selectCount(new QueryWrapper<AssessmentTask>().eq("instance_code", instance.getInstanceCode()).ne("task_status", 0)));
			for(AssessmentTask task : tasks){
				List<AssessmentTaskResult> results = assessmentTaskResultMapper.selectByTaskCode(task.getTaskCode());
				task.setResults(results);
			}
			instance.setTasks(tasks);
		}
		
	}

	@Override
	public AssessmentInstance startInstance(String planCode) throws Exception {
		
		AssessmentPlan assessmentPlan = assessmentPlanMapper.selectByPlanCode(planCode);
		Calendar c = Calendar.getInstance();
		String time = "" + c.get(Calendar.YEAR) + (c.get(Calendar.MONTH) + 1) + c.get(Calendar.DAY_OF_MONTH);
		String instanceCode = assessmentPlan.getPlanCode() + "-" + time;
		String instanceName = time + assessmentPlan.getPlanName();
		
		Integer count = assessmentInstanceMapper.selectCount(new QueryWrapper<AssessmentInstance>().eq("instance_code", instanceCode));
		if(count > 0){
			throw new Exception("不可重复生成！");
		}
		List<AssessmentIndex> indexes = assessmentIndexMapper.selectByPlanCode(assessmentPlan.getPlanCode());
		List<AssessmentProc> procs = assessmentProcMapper.selectByPlanCode(assessmentPlan.getPlanCode());
		List<AssessmentUser> users = assessmentUserMapper.selectByPlanCode(assessmentPlan.getPlanCode());
		
		AssessmentInstance assessmentInstance = new AssessmentInstance();
		assessmentInstance.setInstanceCode(instanceCode);
		assessmentInstance.setInstanceName(instanceName);
		assessmentInstance.setInstanceStatus("0");
		assessmentInstance.setPlanCode(planCode);
		assessmentInstance.setRemark(assessmentPlan.getRemark());
		assessmentInstanceMapper.insert(assessmentInstance);
		List<AssessmentTask> tasks = new ArrayList<>();
		for(AssessmentUser user : users){
			for(AssessmentProc proc : procs){
				String taskCode = UUID.randomUUID().toString(true);
				AssessmentTask task = new AssessmentTask();
				task.setInstanceCode(instanceCode);
				task.setTaskCode(taskCode);
				task.setTaskStatus("0");
				task.setLeader(proc.getAssignee());
				task.setUsername(user.getUsername());
				task.setWeight(proc.getWeight());
				
				List<AssessmentTaskResult> results = new ArrayList<>();
				for(AssessmentIndex index : indexes){
					AssessmentTaskResult result = new AssessmentTaskResult();
					result.setTaskCode(taskCode);
					result.setIndexName(index.getIndexName());
					result.setIndexDesc(index.getIndexDesc());
					result.setWeight(index.getWeight());
					assessmentTaskResultMapper.insert(result);
					results.add(result);
				}
				task.setResults(results);
				assessmentTaskMapper.insert(task);
			}
		}
		assessmentInstance.setTasks(tasks);
		
		return assessmentInstance;
	}


}
