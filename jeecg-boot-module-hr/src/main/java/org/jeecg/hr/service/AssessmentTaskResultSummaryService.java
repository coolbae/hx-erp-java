package org.jeecg.hr.service;

import org.jeecg.hr.entity.AssessmentTaskResultSummary;

import com.baomidou.mybatisplus.extension.service.IService;

public interface AssessmentTaskResultSummaryService extends IService<AssessmentTaskResultSummary> {

}
