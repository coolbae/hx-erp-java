package org.jeecg.hr.service;

import org.jeecg.common.system.base.service.JeecgService;
import org.jeecg.hr.entity.SalaryAttendanceRule;

public interface SalaryAttendanceRuleService extends JeecgService<SalaryAttendanceRule> {

}
