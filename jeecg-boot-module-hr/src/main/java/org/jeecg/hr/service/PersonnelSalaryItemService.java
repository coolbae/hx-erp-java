package org.jeecg.hr.service;

import java.util.List;

import org.jeecg.hr.entity.PersonnelSalaryItem;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: PersonnelSalaryItemService
 * @Author: MxpIO
 */
public interface PersonnelSalaryItemService extends IService<PersonnelSalaryItem> {

	public List<PersonnelSalaryItem> selectByUsername(String username);

	public void handleTransientVariable(List<PersonnelSalaryItem> records);
}
