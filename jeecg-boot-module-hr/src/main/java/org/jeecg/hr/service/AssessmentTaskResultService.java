package org.jeecg.hr.service;

import org.jeecg.hr.entity.AssessmentTaskResult;

import com.baomidou.mybatisplus.extension.service.IService;

public interface AssessmentTaskResultService extends IService<AssessmentTaskResult> {

}
