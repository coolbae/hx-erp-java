package org.jeecg.hr.service.impl;

import java.util.List;

import org.jeecg.hr.entity.AssessmentProc;
import org.jeecg.hr.mapper.AssessmentProcMapper;
import org.jeecg.hr.service.AssessmentProcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class AssessmentProcServiceImpl extends ServiceImpl<AssessmentProcMapper, AssessmentProc> implements AssessmentProcService {

	@Autowired
	private AssessmentProcMapper assessmentProcMapper;
	
	@Override
	public List<AssessmentProc> selectByPlanCode(String planCode) {
		return assessmentProcMapper.selectByPlanCode(planCode);
	}

}
