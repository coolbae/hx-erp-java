package org.jeecg.hr.service.impl;

import org.jeecg.hr.entity.AssessmentTaskResultSummary;
import org.jeecg.hr.mapper.AssessmentTaskResultSummaryMapper;
import org.jeecg.hr.service.AssessmentTaskResultSummaryService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class AssessmentTaskResultSummaryServiceImpl extends ServiceImpl<AssessmentTaskResultSummaryMapper, AssessmentTaskResultSummary> implements AssessmentTaskResultSummaryService {

}
