package org.jeecg.hr.service;

import java.util.List;

import org.jeecg.hr.entity.AssessmentIndex;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: AssessmentIndex
 * @Author: MxpIO
 */
public interface AssessmentIndexService extends IService<AssessmentIndex> {

	public List<AssessmentIndex> selectByPlanCode(String planCode);
}
