package org.jeecg.hr.service;

import java.io.Serializable;
import java.util.Collection;

import org.jeecg.hr.entity.SalaryTemplate;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: ${tableVo.ftlDescription}
 * @Author: jeecg-boot
 * @Date:   ${.now?string["yyyy-MM-dd"]}
 * @Version: V1.0
 */
public interface SalaryTemplateService extends IService<SalaryTemplate> {

	/**
	 * 删除一对多
	 */
	public void delSalaryTemplate (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);

	/**
	 * 新增一对多
	 * @param salaryTemplate
	 * @param salaryTemplateDetails
	 */
	public void saveSalaryTemplateWithDetail(SalaryTemplate salaryTemplate);

	/**
	 * 修改一对多
	 * @param salaryTemplate
	 * @param salaryTemplateDetails
	 */
	public void updateSalaryTemplateWithDetail(SalaryTemplate salaryTemplate);

}
