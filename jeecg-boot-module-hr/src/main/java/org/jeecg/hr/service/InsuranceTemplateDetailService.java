package org.jeecg.hr.service;

import java.util.List;

import org.jeecg.hr.entity.InsuranceTemplateDetail;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: InsuranceTemplateDetail
 * @Author: MxpIO
 */
public interface InsuranceTemplateDetailService extends IService<InsuranceTemplateDetail> {

	public List<InsuranceTemplateDetail> selectByTemplateCode(String templateCode);
}
