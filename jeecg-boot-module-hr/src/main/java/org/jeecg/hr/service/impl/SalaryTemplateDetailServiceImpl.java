package org.jeecg.hr.service.impl;

import java.util.List;

import org.jeecg.hr.entity.SalaryItem;
import org.jeecg.hr.entity.SalaryTemplateDetail;
import org.jeecg.hr.mapper.SalaryItemMapper;
import org.jeecg.hr.mapper.SalaryTemplateDetailMapper;
import org.jeecg.hr.service.SalaryTemplateDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class SalaryTemplateDetailServiceImpl extends ServiceImpl<SalaryTemplateDetailMapper, SalaryTemplateDetail> implements SalaryTemplateDetailService {

	@Autowired
	private SalaryTemplateDetailMapper salaryTemplateDetailMapper;
	
	@Autowired
	private SalaryItemMapper salaryItemMapper;
	
	@Override
	public List<SalaryTemplateDetail> selectByTemplateCode(String templateCode) {
		return salaryTemplateDetailMapper.selectByTemplateCode(templateCode);
	}

	@Override
	public void handleTransientVariable(List<SalaryTemplateDetail> records) {
		for(SalaryTemplateDetail detail : records){
			QueryWrapper<SalaryItem> qw = new QueryWrapper<>();
			qw.eq("item_code", detail.getSalaryItemCode());
			List<SalaryItem> list = salaryItemMapper.selectList(qw);
			if(list != null && list.size() > 0){
				detail.setSalaryItem(list.get(0));
			}
		}
	}

}
