package org.jeecg.hr.service.impl;

import java.util.List;

import org.jeecg.hr.entity.AssessmentIndex;
import org.jeecg.hr.mapper.AssessmentIndexMapper;
import org.jeecg.hr.service.AssessmentIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class AssessmentIndexServiceImpl extends ServiceImpl<AssessmentIndexMapper, AssessmentIndex> implements AssessmentIndexService {

	@Autowired
	private AssessmentIndexMapper assessmentIndexMapper;
	
	@Override
	public List<AssessmentIndex> selectByPlanCode(String planCode) {
		return assessmentIndexMapper.selectByPlanCode(planCode);
	}

}
