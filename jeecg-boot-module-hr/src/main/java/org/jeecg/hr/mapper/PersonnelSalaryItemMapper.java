package org.jeecg.hr.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.hr.entity.PersonnelSalaryItem;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface PersonnelSalaryItemMapper extends BaseMapper<PersonnelSalaryItem> {
	
	public boolean deleteByUsername(@Param("username") String username);
    
	public List<PersonnelSalaryItem> selectByUsername(@Param("username") String username);

}
