package org.jeecg.hr.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.hr.entity.PersonnelInsuranceItem;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface PersonnelInsuranceItemMapper extends BaseMapper<PersonnelInsuranceItem> {
	
	public boolean deleteByUsername(@Param("username") String username);
    
	public List<PersonnelInsuranceItem> selectByUsername(@Param("username") String username);

}
