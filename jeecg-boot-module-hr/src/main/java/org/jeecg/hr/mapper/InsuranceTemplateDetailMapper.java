package org.jeecg.hr.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.hr.entity.InsuranceTemplateDetail;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface InsuranceTemplateDetailMapper extends BaseMapper<InsuranceTemplateDetail> {

	public boolean deleteByTemplateCode(@Param("templateCode") String templateCode);
    
	public List<InsuranceTemplateDetail> selectByTemplateCode(@Param("templateCode") String templateCode);

}
