package org.jeecg.hr.entity;

import java.math.BigDecimal;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_salary_attendance_rule")
public class SalaryAttendanceRule extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "项目编号")
	@Excel(name="项目编号",width=15)
	private String itemCode;
	
	@ApiModelProperty(value = "规则类型")
	@Excel(name="规则类型",width=15)
	private String ruleType;
	
	@ApiModelProperty(value = "是否启用")
	@Excel(name="是否启用",width=15)
	private String disabled;
	
	@ApiModelProperty(value = "权重")
	@Excel(name="权重",width=15)
	private BigDecimal weight;
	
	@ApiModelProperty(value = "计算方式")
	@Excel(name="计算方式",width=15)
	private String calculationType;

}
