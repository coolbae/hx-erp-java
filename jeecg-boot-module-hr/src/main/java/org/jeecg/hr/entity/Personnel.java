package org.jeecg.hr.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;
import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_personnel")
public class Personnel extends JeecgEntity {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "系统账号")
	@Excel(name="系统账号",width=15)
	private String username;
	
	@ApiModelProperty(value = "姓名")
	@Excel(name="姓名",width=15)
	private String realname;
	
	@ApiModelProperty(value = "身份证")
	@Excel(name="身份证",width=15)
	private String identityCard;
	
	@ApiModelProperty(value = "性别")
	@Excel(name="性别",width=15)
	private String sex;
	
	@ApiModelProperty(value = "工资卡号")
	@Excel(name="工资卡号",width=15)
	private String wageCard;
	
	@ApiModelProperty(value = "入职日期")
	@Excel(name="入职日期", width=20, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date entryDate;
	
	@ApiModelProperty(value = "转正日期")
	@Excel(name="转正日期", width=20, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date confirmationDate;
	
	@ApiModelProperty(value = "离职日期")
	@Excel(name="离职日期", width=20, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date departureDate;
	
	@ApiModelProperty(value = "停薪日期")
	@Excel(name="停薪日期", width=20, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date nopayDate;
	
	@ApiModelProperty(value = "执行日期")
	@Excel(name="执行日期", width=20, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date executeDate;
	
	@ApiModelProperty(value = "薪资方案")
	@Excel(name="薪资方案",width=15)
	private String salaryTemplateCode;
	
	@ApiModelProperty(value = "社保方案")
	@Excel(name="社保方案",width=15)
	private String insuranceTemplateCode;
	
	@ApiModelProperty(value = "试用期比例")
	@Excel(name="试用期比例",width=15)
	private BigDecimal probationScale;
	
	@TableField(exist = false)
	private List<PersonnelInsuranceItem> personnelInsuranceItems;
	
	@TableField(exist = false)
	private List<PersonnelSalaryItem> personnelSalaryItems;

}
