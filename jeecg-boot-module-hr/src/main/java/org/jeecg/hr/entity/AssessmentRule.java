package org.jeecg.hr.entity;

import java.math.BigDecimal;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_assessment_rule")
public class AssessmentRule extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "计划编号")
	@Excel(name="计划编号",width=15)
	private String planCode;
	
	@ApiModelProperty(value = "得分上限")
	@Excel(name="得分上限",width=15)
	private BigDecimal scoreSuper;
	
	@ApiModelProperty(value = "得分下限")
	@Excel(name="得分下限",width=15)
	private BigDecimal scoreLower;
	
	@ApiModelProperty(value = "级别")
	@Excel(name="级别",width=15)
	private String levelDesc;
	
	@ApiModelProperty(value = "绩效比例")
	@Excel(name="绩效比例",width=15)
	private BigDecimal assessmentScore;
	
}
