package org.jeecg.hr.entity;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_assessment_user")
public class AssessmentUser extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "计划编号")
	@Excel(name="计划编号",width=15)
	private String planCode;
	
	@ApiModelProperty(value = "被考核人")
	@Excel(name="被考核人",width=15)
	private String username;
	
	@TableField(exist = false)
	private String userTxt;
	
	@TableField(exist = false)
	private String deptTxt;
	
}
