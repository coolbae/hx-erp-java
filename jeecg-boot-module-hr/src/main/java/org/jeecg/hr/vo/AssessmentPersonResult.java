package org.jeecg.hr.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import org.jeecg.modules.system.entity.SysUser;

import lombok.Data;

@Data
public class AssessmentPersonResult implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String instanceCode;
	
	private String username;
	
	private BigDecimal score;
	
	private BigDecimal assessmentScore;
	
	private String levelDesc;
	
	private SysUser user;

}
