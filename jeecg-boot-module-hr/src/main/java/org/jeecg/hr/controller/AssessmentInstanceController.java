package org.jeecg.hr.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.hr.entity.AssessmentInstance;
import org.jeecg.hr.entity.AssessmentTaskResultSummary;
import org.jeecg.hr.service.AssessmentInstanceService;
import org.jeecg.hr.service.AssessmentRuleService;
import org.jeecg.hr.service.AssessmentTaskResultSummaryService;
import org.jeecg.hr.service.AssessmentTaskService;
import org.jeecg.modules.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: 绩效考评任务
 * @Author: MxpIO
 */
@Api(tags = "绩效考评任务")
@RestController
@RequestMapping("/hr/assessment/instance")
public class AssessmentInstanceController extends JeecgController<AssessmentInstance, AssessmentInstanceService> {

	@Autowired
	private AssessmentInstanceService assessmentInstanceService;
	
	@Autowired
	private AssessmentTaskService assessmentTaskService;
	
	@Autowired
	private AssessmentTaskResultSummaryService assessmentTaskResultSummaryService;
	
	@Autowired
	private ISysUserService sysUserService;
	
	@Autowired
	private AssessmentRuleService assessmentRuleService;

	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * 
	 * @param assessmentInstance
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "绩效考评任务-分页列表查询")
	@ApiOperation(value = "绩效考评任务-分页列表查询", notes = "绩效考评任务-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(AssessmentInstance assessmentInstance,
			@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest req) {
		QueryWrapper<AssessmentInstance> queryWrapper = QueryGenerator.initQueryWrapper(assessmentInstance,
				req.getParameterMap());
		Page<AssessmentInstance> page = new Page<AssessmentInstance>(pageNo, pageSize);
		IPage<AssessmentInstance> pageList = assessmentInstanceService.page(page, queryWrapper);
		assessmentInstanceService.handleDetails(pageList);
		return Result.OK(pageList);
	}
	
	@AutoLog(value = "绩效考评任务-查询结果")
	@ApiOperation(value = "绩效考评任务-查询结果", notes = "绩效考评任务-查询结果")
	@GetMapping(value = "/queryResult")
	public Result<?> queryResult(@RequestParam(name = "instanceCode", required = true) String instanceCode,
			@RequestParam(name = "username", required = false) String username){
		QueryWrapper<AssessmentTaskResultSummary> eq = new QueryWrapper<>();
		eq.eq("instance_code", instanceCode);
		if(username != null){
			eq.eq("username", username);
		}
		/*List<AssessmentTask> tasks = assessmentTaskService.list(eq);
		Map<String, AssessmentPersonResult> results = new HashMap<>();
		for(AssessmentTask task : tasks){
			AssessmentPersonResult result = results.get(task.getUsername());
			if(result == null){
				result = new AssessmentPersonResult();
				result.setInstanceCode(instanceCode);
				result.setUsername(task.getUsername());
				result.setUser(sysUserService.getById(task.getUsername()));
				result.setScore(BigDecimal.ZERO);
			}
			result.setScore(result.getScore().add(task.getWeight().divide(new BigDecimal(100)).multiply(task.getScale()==null?BigDecimal.ZERO:task.getScale())));
			results.put(task.getUsername(), result);
		}
		List<AssessmentPersonResult> list = results.entrySet().stream().map(e -> e.getValue()).collect(Collectors.toList());*/
		List<AssessmentTaskResultSummary> list = assessmentTaskResultSummaryService.list(eq);
		for(AssessmentTaskResultSummary summary : list){
			summary.setUser(sysUserService.getUserByName(summary.getUsername()));
		}
		return Result.OK(list);
	}
	
	/**
	 * 启动考评任务
	 * @param planCode
	 * @return
	 */
	@AutoLog(value = "绩效考评任务-启动考评任务")
	@ApiOperation(value = "绩效考评任务-启动考评任务", notes = "绩效考评任务-启动考评任务")
	@PostMapping(value = "/startInstance")
	public Result<?> startInstance(@RequestParam(name = "planCode", required = true) String planCode) {
		AssessmentInstance assessmentInstance;
		try {
			assessmentInstance = assessmentInstanceService.startInstance(planCode);
			return Result.OK("启动成功", assessmentInstance);
		} catch (Exception e) {
			return Result.error(e.getMessage());
		}
		
	}
	
	@AutoLog(value = "绩效考评任务-审核")
	@ApiOperation(value = "绩效考评任务-审核", notes = "绩效考评任务-审核")
	@PostMapping(value = "/verifyTask")
	public Result<?> verifyTask(@RequestParam(name = "instanceId", required = true) String instanceId){
		AssessmentInstance instance = assessmentInstanceService.getById(instanceId);
		if("3".equals(instance.getInstanceStatus())){
			return Result.error("不能重复审核！");
		}else if("2".equals(instance.getInstanceStatus())){
			instance.setInstanceStatus("3");
			assessmentInstanceService.updateById(instance);
			assessmentRuleService.saveResult(instance.getPlanCode(),instance.getInstanceCode());
			return Result.OK("审核成功", instance);
		}else{
			return Result.error("全部考评结束才可审核！");
		}
	}
	
	

	/**
	 * 导出
	 * 
	 * @return
	 */
	@RequestMapping(value = "/exportXls")
	public ModelAndView exportXls(HttpServletRequest request, AssessmentInstance assessmentInstance) {
		return super.exportXls(request, assessmentInstance, AssessmentInstance.class, "绩效考评任务");
	}

	/**
	 * 导入
	 * 
	 * @return
	 */
	@RequestMapping(value = "/importExcel", method = RequestMethod.POST)
	public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
		return super.importExcel(request, response, AssessmentInstance.class);
	}
	/*---------------------------------主表处理-end-------------------------------------*/

}