package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 刷新token请求参数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WebRefreshTokenCondition {
    /**
     * 授权类型，本流程中，此值为："refresh_token"
     */
    @JSONField(name = "grant_type")
    private String grantType;
    /**
     *
     * 来自获取登录用户身份 或 本接口返回值
     * 示例值："ur-t9HHgRCsMqGqIU9vw5Zhof"
     */
    @JSONField(name = "refresh_token")
    private String refreshToken;
}
