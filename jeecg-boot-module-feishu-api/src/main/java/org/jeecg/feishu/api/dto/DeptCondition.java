package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 部门查询条件
 */
@Data
@ToString
@NoArgsConstructor
public class DeptCondition implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 此次调用中使用的部门ID的类型
     *
     * 示例值："open_department_id"
     *
     * 可选值有：
     *
     * department_id：以自定义department_id来标识部门
     * open_department_id：以open_department_id来标识部门
     */
    @JSONField(name = "department_id_type")
    private String departmentIdType;

    @JSONField(name = "department_id_")
    private String departmentId;

    /**
     * 用于V3接口,父部门的ID，填上获取部门下所有子部门
     */
    @JSONField(name = "parent_department_id")
    private String parentDepartmentId;

    /**
     * 用于V1接口,部门 ID，最多同时查询 100 条
     */
    @JSONField(name = "department_ids")
    private List<String> departmentIds;

    public DeptCondition(List<String> departmentIds) {
        this.departmentIds = departmentIds;
    }

    public DeptCondition(String departmentIdType, String departmentId) {
        this.departmentIdType = departmentIdType;
        this.departmentId = departmentId;
    }


}
