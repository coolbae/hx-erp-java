package org.jeecg.feishu.api.service.impl;

import org.jeecg.feishu.api.config.FeishuConfig;
import org.jeecg.feishu.api.constant.FeishuApiConstants;
import org.jeecg.feishu.api.dto.FeishuRequest;
import org.jeecg.feishu.api.dto.Message;
import org.jeecg.feishu.api.dto.MessageCondition;
import org.jeecg.feishu.api.service.FeishuApiService;
import org.jeecg.feishu.api.service.FeishuMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("feishuMessageService")
public class FeishuMessageServiceImpl implements FeishuMessageService {
	
	@Autowired
    private FeishuApiService feishuApiService;
    @Autowired
    private FeishuConfig feishuConfig;

	@Override
	public Message send(MessageCondition condition) {
		FeishuRequest<MessageCondition, Message> request = FeishuRequest.newRequestByApp(FeishuApiConstants.MESSAGE_BATCH_SEND_PATH, "POST", condition, new Message());
        try {
            return feishuApiService.send(feishuConfig, request);
        } catch (Exception e) {
            log.error("发送消息失败", e);
            return null;
        }
	}

}
