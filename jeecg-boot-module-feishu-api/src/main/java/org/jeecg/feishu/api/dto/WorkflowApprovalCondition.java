package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

import java.io.Serializable;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class WorkflowApprovalCondition implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * 审批实例 Code
     */
    @JSONField(name = "instance_code")
    private String instanceCode;
    /**
     * zh-CN - 中文
     * en-US - 英文
     * ja-JP - 日文
     */
    @JSONField(name = "locale")
    private String locale;
    /**
     * 发起审批用户,平台级审批时使用
     */
    @JSONField(name = "user_id")
    private String userId;
    /**
     * 发起审批用户 open id
     */
    @JSONField(name = "open_id")
    private String openId;
}
