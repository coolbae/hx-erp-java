package org.jeecg.feishu.api.utils;

import org.apache.commons.codec.binary.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 飞书签名工具类
 */
public class FeishuSignatureUtils {
    /**
     * 飞书事件订阅签名：将请求头 X-Lark-Request-Timestamp、X-Lark-Request-Nonce 与 encrypt_key 拼接后 按照 encode('utf-8') 编码得到 byte[] b1，再拼接上 body, 得到一个 byte[] b。
     * <p>
     * 将 b 用 sha256 加密，得到字符串 s， 业务方校验 s 是否和请求头 X-Lark-Signature 一致。
     *
     * @param timestamp
     * @param nonce
     * @param encryptKey
     * @param bodyString
     * @return
     * @throws NoSuchAlgorithmException
     */
    public String calculateSignature(String timestamp, String nonce, String encryptKey, String bodyString) throws NoSuchAlgorithmException {
        StringBuilder content = new StringBuilder();
        content.append(timestamp).append(nonce).append(encryptKey).append(bodyString);
        MessageDigest alg = MessageDigest.getInstance("SHA-256");
        String sign = Hex.encodeHexString(alg.digest(content.toString().getBytes()));
        return sign;
    }
}
