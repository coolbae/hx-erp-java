package org.jeecg.feishu.api.service.impl;

import org.jeecg.feishu.api.config.FeishuConfig;
import org.jeecg.feishu.api.constant.FeishuApiConstants;
import org.jeecg.feishu.api.dto.*;
import org.jeecg.feishu.api.service.FeishuApiService;
import org.jeecg.feishu.api.service.FeishuApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class FeishuApprovalServiceImpl implements FeishuApprovalService {
    @Autowired
    private FeishuApiService feishuApiService;
    @Autowired
    private FeishuConfig feishuConfig;

    @Override
    public WorkflowApproval getWorkApprovalInstance(WorkflowApprovalCondition condition) {
        FeishuRequest<WorkflowApprovalCondition, WorkflowApproval> request = FeishuRequest.newRequestByTenant(FeishuApiConstants.WORKFLOW_APPROVAL_INSTANCE, "POST", condition, new WorkflowApproval());
        request.setDomain(feishuConfig.getApprovalDomain());
        request.setOpenApi(false);
        try {
            return feishuApiService.send(feishuConfig, request);
        } catch (Exception e) {
            log.error("根据code获取登录用户身份失败", e);
        }
        return null;
    }
}
