package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 用于V1接口, 批量查询员工列表
 */
@Data
@ToString
public class EmployeePageResponse implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JSONField(name = "user_infos")
    private List<EmployeeInfo> userInfos;

    @Data
    @ToString
    public static class EmployeeInfo implements Serializable {

        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		/**
         * 用户名
         */
        private String name;
        /**
         * 用户的 employee_id，申请了"获取用户 user_id"权限的应用返回该字段
         */
        @JSONField(name = "employee_id")
        private String employeeId;
        /**
         * 工号
         */
        @JSONField(name = "employee_no")
        private String employeeNo;
        /**
         * 用户的 open_id
         */
        @JSONField(name = "open_id")
        private String openId;
        /**
         * 用户的 union_id
         */
        @JSONField(name = "union_id")
        private String unionId;
        /**
         * 用户状态，bit0(最低位): 1冻结，0未冻结；bit1:1离职，0在职；bit2:1未激活，0已激活
         */
        private int status;
        /**
         * 员工类型。1:正式员工；2:实习生；3:外包；4:劳务；5:顾问
         */
        @JSONField(name = "employee_type")
        private int employeeType;
        /**
         * 性别，未设置不返回该字段。1:男；2:女
         */
        private int gender;
        /**
         * 用户邮箱地址，已申请"获取用户邮箱"权限返回该字段
         */
        private String email;
        /**
         * 用户手机号，已申请"获取用户手机号"权限的企业自建应用返回该字段
         */
        private String mobile;
        /**
         * 用户个人签名
         */
        private String description;
        /**
         * 用户所在国家
         */
        private String country;
        /**
         * 用户所在城市
         */
        private String city;
        /**
         * 工位
         */
        @JSONField(name = "workStation")
        private String workStation;
        /**
         * 是否是企业超级管理员
         */
        @JSONField(name = "is_tenant_manager")
        private boolean isTenantManager;
        /**
         *
         */
        @JSONField(name = "join_time")
        private long joinTime;
        /**
         * 更新时间
         */
        @JSONField(name = "updateTime")
        private long updateTime;
        /**
         * 用户直接领导的 employee_id，企业自建应用返回，应用商店应用申请了 employee_id 权限时才返回
         */
        @JSONField(name = "leader_employee_id")
        private String leaderEmployeeId;
        /**
         * 用户直接领导的 open_id
         */
        @JSONField(name = "leader_open_id")
        private String leaderOpenId;
        /**
         * 用户直接领导的 union_id
         */
        @JSONField(name = "leader_union_id")
        private String leaderUnionId;
        /**
         * 用户所在部门自定义 ID列表，用户可能同时存在于多个部门
         */
        private List<String> departments;
        /**
         * 用户所在部门 openID 列表，用户可能同时存在于多个部门
         */
        @JSONField(name = "open_departments")
        private List<String> openDepartments;
        /**
         * 用户的自定义属性信息。
         * 该字段返回的每一个属性包括自定义属性 ID 和自定义属性值。
         * 企业开放了自定义用户属性且为该用户设置了自定义属性的值，才会返回该字段
         */
        @JSONField(name = "custom_attrs")
        private Map<String, String> customAttrs;
    }

}
