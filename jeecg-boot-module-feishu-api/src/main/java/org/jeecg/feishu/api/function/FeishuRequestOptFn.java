package org.jeecg.feishu.api.function;

import org.jeecg.feishu.api.dto.FeishuRequestOpt;

public interface FeishuRequestOptFn {
    void fn(FeishuRequestOpt opt);
}