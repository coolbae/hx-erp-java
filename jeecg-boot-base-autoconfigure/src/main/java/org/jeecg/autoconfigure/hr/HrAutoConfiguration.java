package org.jeecg.autoconfigure.hr;

import org.jeecg.autoconfigure.weixin.web.WxWebAutoConfiguration;
import org.jeecg.hr.HrConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(HrConfiguration.class)
@AutoConfigureAfter({WxWebAutoConfiguration.class})
@Import(HrConfiguration.class)
@Slf4j
public class HrAutoConfiguration {

	public HrAutoConfiguration() {
		log.info("[AutoConfiguration==>]:Hr Module Loading");
	}
}
