package org.jeecg.autoconfigure.codegenerate;

import org.apache.shiro.spring.boot.autoconfigure.ShiroAnnotationProcessorAutoConfiguration;
import org.apache.shiro.spring.boot.autoconfigure.ShiroAutoConfiguration;
import org.jeecg.autoconfigure.common.CommonAutoConfiguration;
import org.jeecg.codegenerate.CodegenerateConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(CodegenerateConfiguration.class)
@AutoConfigureBefore({ShiroAutoConfiguration.class,ShiroAnnotationProcessorAutoConfiguration.class})
@AutoConfigureAfter({CommonAutoConfiguration.class})
@Import(CodegenerateConfiguration.class)
@Slf4j
public class CodegenerateAutoConfiguration {

	public CodegenerateAutoConfiguration() {
		log.info("[AutoConfiguration==>]:Codegenerate Module Loading");
	}
}
