package org.jeecg.autoconfigure.weixin.cp;

import org.jeecg.modules.SystemConfiguration;
import org.jeecg.weixin.common.WxCommonConfiguration;
import org.jeecg.weixin.cp.WxCpConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(WxCpConfiguration.class)
@AutoConfigureAfter({WxCommonConfiguration.class,SystemConfiguration.class})
@Import(WxCpConfiguration.class)
@Slf4j
public class WxCpAutoConfiguration {

	public WxCpAutoConfiguration() {
		log.info("[AutoConfiguration==>]:WxCp Module Loading");
	}
}
