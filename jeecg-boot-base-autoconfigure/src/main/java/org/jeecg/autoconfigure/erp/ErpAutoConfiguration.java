package org.jeecg.autoconfigure.erp;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.autoconfigure.system.SystemAutoConfiguration;
import org.jeecg.erp.ErpConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnClass(ErpConfiguration.class)
@AutoConfigureBefore({SystemAutoConfiguration.class})
@Import(ErpConfiguration.class)
@Slf4j
public class ErpAutoConfiguration {

	public ErpAutoConfiguration() {
		log.info("[AutoConfiguration==>]:erp Module Loading");
	}
}
