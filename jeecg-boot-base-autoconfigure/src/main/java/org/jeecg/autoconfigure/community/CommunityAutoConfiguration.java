package org.jeecg.autoconfigure.community;

import org.jeecg.autoconfigure.weixin.web.WxWebAutoConfiguration;
import org.jeecg.community.CommunityConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(CommunityConfiguration.class)
@AutoConfigureAfter({WxWebAutoConfiguration.class})
@Import(CommunityConfiguration.class)
@Slf4j
public class CommunityAutoConfiguration {

	public CommunityAutoConfiguration() {
		log.info("[AutoConfiguration==>]:Community Module Loading");
	}
}
