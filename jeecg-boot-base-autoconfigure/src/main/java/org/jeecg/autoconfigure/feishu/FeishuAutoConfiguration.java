package org.jeecg.autoconfigure.feishu;

import org.jeecg.autoconfigure.system.SystemAutoConfiguration;
import org.jeecg.feishu.FeishuConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(FeishuConfiguration.class)
@AutoConfigureAfter({SystemAutoConfiguration.class})
@Import(FeishuConfiguration.class)
@Slf4j
public class FeishuAutoConfiguration {

	public FeishuAutoConfiguration() {
		log.info("[AutoConfiguration==>]:WxCommon Module Loading");
	}
}
