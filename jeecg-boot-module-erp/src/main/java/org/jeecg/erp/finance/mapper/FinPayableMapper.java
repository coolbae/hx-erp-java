package org.jeecg.erp.finance.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.erp.finance.entity.FinPayable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 * @Description: 应付单
 * @Author: jeecg-boot
 * @Date:   2020-04-13
 * @Version: V1.0
 */
public interface FinPayableMapper extends BaseMapper<FinPayable> {

}
