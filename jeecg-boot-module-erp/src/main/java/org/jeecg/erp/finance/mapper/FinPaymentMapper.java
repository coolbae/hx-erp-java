package org.jeecg.erp.finance.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.erp.finance.entity.FinPayment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 付款单
 * @Author: jeecg-boot
 * @Date:   2020-04-14
 * @Version: V1.0
 */
public interface FinPaymentMapper extends BaseMapper<FinPayment> {

}
