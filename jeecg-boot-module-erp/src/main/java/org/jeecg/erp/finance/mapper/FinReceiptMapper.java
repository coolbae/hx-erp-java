package org.jeecg.erp.finance.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.erp.finance.entity.FinReceipt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 收款单
 * @Author: jeecg-boot
 * @Date:   2020-04-30
 * @Version: V1.0
 */
public interface FinReceiptMapper extends BaseMapper<FinReceipt> {

}
