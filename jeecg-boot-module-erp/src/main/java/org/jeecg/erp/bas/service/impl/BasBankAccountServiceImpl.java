package org.jeecg.erp.bas.service.impl;

import org.jeecg.erp.bas.entity.BasBankAccount;
import org.jeecg.erp.bas.mapper.BasBankAccountMapper;
import org.jeecg.erp.bas.service.IBasBankAccountService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 银行账户
 * @Author: jeecg-boot
 * @Date:   2020-04-14
 * @Version: V1.0
 */
@Service
public class BasBankAccountServiceImpl extends ServiceImpl<BasBankAccountMapper, BasBankAccount> implements IBasBankAccountService {

}
