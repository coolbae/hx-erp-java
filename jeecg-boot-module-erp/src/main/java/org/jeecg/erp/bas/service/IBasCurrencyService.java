package org.jeecg.erp.bas.service;

import org.jeecg.erp.bas.entity.BasCurrency;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 币种
 * @Author: jeecg-boot
 * @Date:   2020-04-14
 * @Version: V1.0
 */
public interface IBasCurrencyService extends IService<BasCurrency> {

}
