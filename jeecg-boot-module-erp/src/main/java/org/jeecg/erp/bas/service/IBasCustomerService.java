package org.jeecg.erp.bas.service;

import org.jeecg.erp.bas.entity.BasCustomer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 客户
 * @Author: jeecg-boot
 * @Date:   2020-04-01
 * @Version: V1.0
 */
public interface IBasCustomerService extends IService<BasCustomer> {

}
