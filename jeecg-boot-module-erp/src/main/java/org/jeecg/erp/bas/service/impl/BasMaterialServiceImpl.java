package org.jeecg.erp.bas.service.impl;

import org.jeecg.erp.bas.entity.BasMaterial;
import org.jeecg.erp.bas.mapper.BasMaterialMapper;
import org.jeecg.erp.bas.service.IBasMaterialService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 物料
 * @Author: jeecg-boot
 * @Date:   2020-05-29
 * @Version: V1.0
 */
@Service
public class BasMaterialServiceImpl extends ServiceImpl<BasMaterialMapper, BasMaterial> implements IBasMaterialService {

}
