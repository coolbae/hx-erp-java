package org.jeecg.erp.bas.service;

import org.jeecg.erp.bas.entity.BasBankAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 银行账户
 * @Author: jeecg-boot
 * @Date:   2020-04-14
 * @Version: V1.0
 */
public interface IBasBankAccountService extends IService<BasBankAccount> {

}
