package org.jeecg.erp.bas.service.impl;

import org.jeecg.erp.bas.entity.BasCurrency;
import org.jeecg.erp.bas.mapper.BasCurrencyMapper;
import org.jeecg.erp.bas.service.IBasCurrencyService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 币种
 * @Author: jeecg-boot
 * @Date:   2020-04-14
 * @Version: V1.0
 */
@Service
public class BasCurrencyServiceImpl extends ServiceImpl<BasCurrencyMapper, BasCurrency> implements IBasCurrencyService {

}
