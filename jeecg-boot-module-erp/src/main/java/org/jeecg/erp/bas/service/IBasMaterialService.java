package org.jeecg.erp.bas.service;

import org.jeecg.erp.bas.entity.BasMaterial;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 物料
 * @Author: jeecg-boot
 * @Date:   2020-05-29
 * @Version: V1.0
 */
public interface IBasMaterialService extends IService<BasMaterial> {

}
