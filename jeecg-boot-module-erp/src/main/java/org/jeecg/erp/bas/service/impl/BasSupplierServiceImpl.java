package org.jeecg.erp.bas.service.impl;

import org.jeecg.erp.bas.entity.BasSupplier;
import org.jeecg.erp.bas.mapper.BasSupplierMapper;
import org.jeecg.erp.bas.service.IBasSupplierService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 供应商
 * @Author: jeecg-boot
 * @Date:   2020-04-01
 * @Version: V1.0
 */
@Service
public class BasSupplierServiceImpl extends ServiceImpl<BasSupplierMapper, BasSupplier> implements IBasSupplierService {

}
