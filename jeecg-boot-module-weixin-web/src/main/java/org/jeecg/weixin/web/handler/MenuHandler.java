package org.jeecg.weixin.web.handler;

import java.util.Map;

import org.springframework.stereotype.Component;

import org.jeecg.weixin.common.api.WxConsts.MenuButtonType;
import org.jeecg.weixin.common.session.WxSessionManager;
import org.jeecg.weixin.cp.api.WxCpService;
import org.jeecg.weixin.cp.bean.message.WxCpXmlMessage;
import org.jeecg.weixin.cp.bean.message.WxCpXmlOutMessage;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
@Component
public class MenuHandler extends AbstractHandler {

	@Override
	public WxCpXmlOutMessage handle(WxCpXmlMessage wxMessage, Map<String, Object> context, WxCpService cpService,
			WxSessionManager sessionManager) {

		String msg = String.format("type:%s, event:%s, key:%s", wxMessage.getMsgType(), wxMessage.getEvent(),
				wxMessage.getEventKey());
		if (MenuButtonType.VIEW.equals(wxMessage.getEvent())) {
			return null;
		}

		return WxCpXmlOutMessage.TEXT().content(msg).fromUser(wxMessage.getToUserName())
				.toUser(wxMessage.getFromUserName()).build();
	}

}
