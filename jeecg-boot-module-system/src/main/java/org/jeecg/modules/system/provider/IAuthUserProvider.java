package org.jeecg.modules.system.provider;

public interface IAuthUserProvider {
	
	public String getUserIdByCode(String code) throws Exception;
	
	public ThirdType getType();

}
