package org.jeecg.modules.system.service.impl;

import org.jeecg.modules.system.entity.SysParameter;
import org.jeecg.modules.system.mapper.SysParameterMapper;
import org.jeecg.modules.system.service.ISysParameterService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class SysParameterServiceImpl extends ServiceImpl<SysParameterMapper, SysParameter> implements ISysParameterService {

}
