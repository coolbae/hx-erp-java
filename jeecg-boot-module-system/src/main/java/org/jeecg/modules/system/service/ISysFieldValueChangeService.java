package org.jeecg.modules.system.service;

import org.jeecg.modules.system.entity.SysFieldValueChange;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 记录字段值的变化
 * @Author: jeecg-boot
 * @Date:   2021-01-06
 * @Version: V1.0
 */
public interface ISysFieldValueChangeService extends IService<SysFieldValueChange> {

}
