package org.jeecg.modules.system.mapper;

import org.jeecg.modules.system.entity.SysParameter;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 系统参数
 * @Author: jeecg-boot
 * @Date:   2020-12-03
 * @Version: V1.0
 */
public interface SysParameterMapper extends BaseMapper<SysParameter> {

}
