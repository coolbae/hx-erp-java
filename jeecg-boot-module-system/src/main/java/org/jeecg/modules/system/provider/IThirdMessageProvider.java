package org.jeecg.modules.system.provider;

import org.jeecg.common.api.dto.message.BusMessageDTO;
import org.jeecg.common.api.dto.message.MessageDTO;

public interface IThirdMessageProvider {

	void send(BusMessageDTO message);

	void sendSimpleMsg(MessageDTO message);

}
