package org.jeecg.modules.system.mapper;

import org.jeecg.modules.system.entity.SysHomePageLayout;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 首页配置
 * @Author: jeecg-boot
 * @Date:   2021-02-03
 * @Version: V1.0
 */
public interface SysHomePageLayoutMapper extends BaseMapper<SysHomePageLayout> {

}
