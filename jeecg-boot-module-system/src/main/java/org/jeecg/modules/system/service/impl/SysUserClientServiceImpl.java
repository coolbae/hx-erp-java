package org.jeecg.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.system.entity.SysUserClient;
import org.jeecg.modules.system.mapper.SysUserClientMapper;
import org.jeecg.modules.system.service.ISysUserClientService;
import org.springframework.stereotype.Service;

/**
 * TODO 移动设备码与用户关系service实现类
 *
 * @author dousw
 * @version 1.0
 * @date 2020/12/7 9:03
 */

@Service
public class SysUserClientServiceImpl extends ServiceImpl<SysUserClientMapper, SysUserClient> implements ISysUserClientService {

    @Override
    public void userBindingDeviceNo(String username, String clientId) {

        SysUserClient sysUserClient = this.getOne(new QueryWrapper<SysUserClient>()
                .eq("user_id", username));

        // 判断是否已绑定
        if (sysUserClient == null) {
            SysUserClient suc = new SysUserClient();
            suc.setClientId(clientId);
            suc.setUserId(username);
            this.save(suc);
        } else {
            this.update(new UpdateWrapper<SysUserClient>().set("client_id", clientId).eq("id", sysUserClient.getId()));
        }
    }
}
