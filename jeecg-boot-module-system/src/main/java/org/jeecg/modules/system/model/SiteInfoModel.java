package org.jeecg.modules.system.model;

import lombok.Data;

@Data
public class SiteInfoModel {
	private String appName;
	private String appAbbr;
	private String appDesc;
	private String appLogo;
	private String appLogoWhite;
	private String helpPath;
	private boolean allowRegister;
	private boolean allowPhoneLogin;
	private boolean allowThirdLogin;
}
