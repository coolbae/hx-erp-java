package org.jeecg.modules.system.service;

import java.util.Map;

import org.jeecg.modules.system.entity.SysHomePageLayout;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 首页配置
 * @Author: jeecg-boot
 * @Date:   2021-02-03
 * @Version: V1.0
 */
public interface ISysHomePageLayoutService extends IService<SysHomePageLayout> {

	/**
	 * 通过当前登录人查询
	 * @param sysUser
	 * @return
	 */
	SysHomePageLayout getSysHomePageLayoutByMap(Map<String, Object> parameter);

}
