package org.jeecg.modules.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/kFormDesign")
@Api(tags="设计器")
public class KFormDesignController {
	
	@RequestMapping("/index")
	public String kFormDesign(){
		log.info("进入设计器");
		return "kFormDesign/index";
	}
	
}
