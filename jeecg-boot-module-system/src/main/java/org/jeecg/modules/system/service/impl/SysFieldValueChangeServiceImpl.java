package org.jeecg.modules.system.service.impl;

import org.jeecg.modules.system.entity.SysFieldValueChange;
import org.jeecg.modules.system.mapper.SysFieldValueChangeMapper;
import org.jeecg.modules.system.service.ISysFieldValueChangeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 记录字段值的变化
 * @Author: jeecg-boot
 * @Date:   2021-01-06
 * @Version: V1.0
 */
@Service
public class SysFieldValueChangeServiceImpl extends ServiceImpl<SysFieldValueChangeMapper, SysFieldValueChange> implements ISysFieldValueChangeService {

}
