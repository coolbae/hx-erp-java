package org.jeecg.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.system.entity.SysUserClient;

public interface ISysUserClientService extends IService<SysUserClient> {

    /**
     * 用户与终端设备绑定
     * @param username
     * @param clientId
     */
    void userBindingDeviceNo(String username, String clientId);
}
