package org.jeecg.modules.system.service;

import org.jeecg.modules.system.entity.SysParameter;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 系统参数
 * @Author: jeecg-boot
 * @Date:   2020-12-03
 * @Version: V1.0
 */
public interface ISysParameterService extends IService<SysParameter> {

}
