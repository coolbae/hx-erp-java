package org.jeecg.modules.system.mapper;

import org.jeecg.modules.system.entity.SysFieldValueChange;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 记录字段值的变化
 * @Author: jeecg-boot
 * @Date:   2021-01-06
 * @Version: V1.0
 */
public interface SysFieldValueChangeMapper extends BaseMapper<SysFieldValueChange> {

}
