package org.jeecg.modules.shiro.authc;
 
import org.apache.shiro.authc.AuthenticationToken;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author Scott
 * @create 2018-07-12 15:19
 * @desc
 **/
public class JwtToken implements AuthenticationToken {
	
	private static final long serialVersionUID = 1L;
	private String token;

	private String loginType;
 
    public JwtToken(String token,String loginType) {
        this.token = token;
        this.loginType = loginType;
    }
 
    @Override
    public Object getPrincipal() {
        Map<String, Object> params = new HashMap<>();
        params.put("token", token);
        params.put("loginType", loginType);
        return params;
    }
 
    @Override
    public Object getCredentials() {
        return token;
    }
}
