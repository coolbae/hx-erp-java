package org.jeecg.modules.online.cgform.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.online.cgform.entity.OnlCgformButton;
import org.jeecg.modules.online.cgform.service.IOnlCgformButtonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("onlCgformButtonController")
@RequestMapping({"/online/cgform/button"})
@Slf4j
public class OnlCgformButtonController {
    @Autowired
    private IOnlCgformButtonService onlCgformButtonService;

    public OnlCgformButtonController() {
    }

    @GetMapping({"/list/{code}"})
    public Result<IPage<OnlCgformButton>> list(OnlCgformButton onlCgformButton, @RequestParam(name = "pageNo",defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize, HttpServletRequest request, @PathVariable("code") String code) {
        Result<IPage<OnlCgformButton>> result = new Result<>();
        onlCgformButton.setCgformHeadId(code);
        QueryWrapper<OnlCgformButton> wrapper = QueryGenerator.initQueryWrapper(onlCgformButton, request.getParameterMap());
        Page<OnlCgformButton> page = new Page<OnlCgformButton>(pageNo, pageSize);
        IPage<OnlCgformButton> ipage = this.onlCgformButtonService.page(page, wrapper);
        result.setSuccess(true);
        result.setResult(ipage);
        return result;
    }

    @PostMapping({"/add"})
    public Result<OnlCgformButton> add(@RequestBody OnlCgformButton var1) {
        Result<OnlCgformButton> result = new Result<>();

        try {
            this.onlCgformButtonService.save(var1);
            result.success("添加成功！");
        } catch (Exception var4) {
            log.error(var4.getMessage(), var4);
            result.error500("操作失败");
        }

        return result;
    }

    @PutMapping({"/edit"})
    public Result<OnlCgformButton> edit(@RequestBody OnlCgformButton entity) {
        Result<OnlCgformButton> result = new Result<>();
        OnlCgformButton onlCgformButton = this.onlCgformButtonService.getById(entity.getId());
        if (onlCgformButton == null) {
        	result.error500("未找到对应实体");
        } else {
            boolean var4 = this.onlCgformButtonService.updateById(entity);
            if (var4) {
            	result.success("修改成功!");
            }
        }

        return result;
    }

    @DeleteMapping({"/delete"})
    public Result<OnlCgformButton> delete(@RequestParam(name = "id",required = true) String id) {
        Result<OnlCgformButton> result = new Result<OnlCgformButton>();
        OnlCgformButton onlCgformButton = this.onlCgformButtonService.getById(id);
        if (onlCgformButton == null) {
        	result.error500("未找到对应实体");
        } else {
            boolean b = this.onlCgformButtonService.removeById(id);
            if (b) {
            	result.success("删除成功!");
            }
        }

        return result;
    }

    @DeleteMapping({"/deleteBatch"})
    public Result<OnlCgformButton> deleteBatch(@RequestParam(name = "ids",required = true) String ids) {
        Result<OnlCgformButton> result = new Result<>();
        if (ids != null && !"".equals(ids.trim())) {
            this.onlCgformButtonService.removeByIds(Arrays.asList(ids.split(",")));
            result.success("删除成功!");
        } else {
        	result.error500("参数不识别！");
        }

        return result;
    }

    @GetMapping({"/queryById"})
    public Result<OnlCgformButton> queryById(@RequestParam(name = "id",required = true) String id) {
        Result<OnlCgformButton> result = new Result<>();
        OnlCgformButton onlCgformButton = this.onlCgformButtonService.getById(id);
        if (onlCgformButton == null) {
        	result.error500("未找到对应实体");
        } else {
        	result.setResult(onlCgformButton);
        	result.setSuccess(true);
        }

        return result;
    }
}
