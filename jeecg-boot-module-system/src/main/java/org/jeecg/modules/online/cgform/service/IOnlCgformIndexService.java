package org.jeecg.modules.online.cgform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.online.cgform.entity.OnlCgformIndex;

import java.util.List;

public interface IOnlCgformIndexService
    extends IService<OnlCgformIndex>
{

    public void createIndex(String code, String databaseType, String tbname);

    public boolean isExistIndex(String countSql);

    public List<OnlCgformIndex> getCgformIndexsByCgformId(String cgformId);
}
