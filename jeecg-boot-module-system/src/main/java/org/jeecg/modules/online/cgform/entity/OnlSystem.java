package org.jeecg.modules.online.cgform.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 子系统
 * @Author: MxpIO
 * @Date:   2020-12-09
 * @Version: V1.0
 */
@Data
@TableName("onl_system")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="onl_system对象", description="子系统")
public class OnlSystem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
    @ApiModelProperty(value = "创建日期")
    private java.lang.String createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
    @ApiModelProperty(value = "更新日期")
    private java.lang.String updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**子系统编号*/
	@Excel(name = "子系统编号", width = 15)
    @ApiModelProperty(value = "子系统编号")
    private java.lang.String sysCode;
	/**子系统名称*/
	@Excel(name = "子系统名称", width = 15)
    @ApiModelProperty(value = "子系统名称")
    private java.lang.String sysName;
	/**管理员*/
	@Excel(name = "管理员", width = 15, dictTable = "sys_user", dicText = "realname", dicCode = "username")
	@Dict(dictTable = "sys_user", dicText = "realname", dicCode = "username")
    @ApiModelProperty(value = "管理员")
    private java.lang.String sysAdmin;
	/**数据源*/
	@Excel(name = "数据源", width = 15, dictTable = "sys_data_source", dicText = "name", dicCode = "code")
	@Dict(dictTable = "sys_data_source", dicText = "name", dicCode = "code")
    @ApiModelProperty(value = "数据源")
    private java.lang.String sysDs;
	/**子系统描述*/
	@Excel(name = "子系统描述", width = 15)
    @ApiModelProperty(value = "子系统描述")
    private java.lang.String sysDesc;
}
