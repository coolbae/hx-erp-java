package org.jeecg.modules.online.config.util;

public class DbType {
    public static final String BIG_DECIMAL = "BigDecimal";
    public static final String BLOB = "Blob";
    public static final String DATE = "Date";
    public static final String DATE_TIME = "datetime";
    public static final String DECIMAL = "decimal";
    public static final String DOUBLE = "double";
    public static final String INT = "int";
    public static final String NUMBER = "number";
    public static final String STRING = "string";
    public static final String TEXT = "Text";
    public static final String TEXT_AREA = "textarea";
    public static final String VARCHAR = "varchar";
}
