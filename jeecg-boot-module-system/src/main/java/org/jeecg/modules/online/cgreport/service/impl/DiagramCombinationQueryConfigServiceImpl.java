package org.jeecg.modules.online.cgreport.service.impl;

import org.jeecg.modules.online.cgreport.entity.DiagramCombinationQueryConfig;
import org.jeecg.modules.online.cgreport.mapper.DiagramCombinationQueryConfigMapper;
import org.jeecg.modules.online.cgreport.service.IDiagramCombinationQueryConfigService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 查询配置
 * @Author: jeecg-boot
 * @Date:   2020-11-13
 * @Version: V1.0
 */
@Service
public class DiagramCombinationQueryConfigServiceImpl extends ServiceImpl<DiagramCombinationQueryConfigMapper, DiagramCombinationQueryConfig> implements IDiagramCombinationQueryConfigService {

}
