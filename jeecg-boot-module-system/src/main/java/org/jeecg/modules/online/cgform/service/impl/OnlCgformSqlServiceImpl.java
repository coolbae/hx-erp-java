package org.jeecg.modules.online.cgform.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.online.cgform.converter.ConverterUtil;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;
import org.jeecg.modules.online.cgform.entity.OnlCgformHead;
import org.jeecg.modules.online.cgform.mapper.OnlCgformFieldMapper;
import org.jeecg.modules.online.cgform.service.IOnlCgformHeadService;
import org.jeecg.modules.online.cgform.service.IOnlCgformSqlService;
import org.jeecg.modules.online.cgform.util.SqlSymbolUtil;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("onlCgformSqlServiceImpl")
public class OnlCgformSqlServiceImpl implements IOnlCgformSqlService {
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;
    @Autowired
    private IOnlCgformHeadService onlCgformHeadService;

    public void saveBatchOnlineTable(OnlCgformHead head, List<OnlCgformField> fieldList, List<Map<String, Object>> dataList) {
        SqlSession var4 = null;

        try {
            ConverterUtil.converter(2, dataList, fieldList);
            var4 = this.sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH, false);
            OnlCgformFieldMapper var5 = (OnlCgformFieldMapper)var4.getMapper(OnlCgformFieldMapper.class);
            short var6 = 1000;
            int var7;
            String var8;
            if (var6 >= dataList.size()) {
                for(var7 = 0; var7 < dataList.size(); ++var7) {
                    var8 = JSON.toJSONString(dataList.get(var7));
                    this.a(var8, head, fieldList, var5);
                }
            } else {
                for(var7 = 0; var7 < dataList.size(); ++var7) {
                    var8 = JSON.toJSONString(dataList.get(var7));
                    this.a(var8, head, fieldList, var5);
                    if (var7 % var6 == 0) {
                        var4.commit();
                        var4.clearCache();
                    }
                }
            }

            var4.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            var4.close();
        }

    }

    public void saveOrUpdateSubData(String subDataJsonStr, OnlCgformHead head, List<OnlCgformField> subFiledList) throws BusinessException {
        OnlCgformFieldMapper var4 = (OnlCgformFieldMapper)SpringContextUtils.getBean(OnlCgformFieldMapper.class);
        this.a(subDataJsonStr, head, subFiledList, var4);
    }

    private void a(String var1, OnlCgformHead var2, List<OnlCgformField> var3, OnlCgformFieldMapper var4) throws BusinessException {
        JSONObject var5 = JSONObject.parseObject(var1);
        int var6 = this.onlCgformHeadService.executeEnhanceJava("import", "start", var2, var5);
        String var7 = var2.getTableName();
        Map<String, Object> var8;
        if (1 == var6) {
            var8 = SqlSymbolUtil.getInsertSql(var7, var3, var5);
            var4.executeInsertSQL(var8);
        } else if (2 == var6) {
            var8 = SqlSymbolUtil.getUpdateSql(var7, var3, var5);
            var4.executeUpdatetSQL(var8);
        } else if (0 == var6) {
        }

    }
}
