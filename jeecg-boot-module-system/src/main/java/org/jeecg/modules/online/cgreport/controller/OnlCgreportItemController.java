package org.jeecg.modules.online.cgreport.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.online.cgreport.entity.OnlCgreportItem;
import org.jeecg.modules.online.cgreport.service.IOnlCgreportItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

@RestController
@RequestMapping({"/online/cgreport/item"})
public class OnlCgreportItemController {
    @Autowired
    private IOnlCgreportItemService onlCgreportItemService;

    @GetMapping({"/listByHeadId"})
    public Result<?> listByHeadId(@RequestParam("headId") String headId) {
        QueryWrapper<OnlCgreportItem> qw = new QueryWrapper<>();
        qw.eq("cgrhead_id", headId);
        qw.orderByAsc("order_num");
        List<OnlCgreportItem> list = this.onlCgreportItemService.list(qw);
        Result<List<OnlCgreportItem>> result = new Result<>();
        result.setSuccess(true);
        result.setResult(list);
        return result;
    }

    @GetMapping({"/list"})
    public Result<IPage<OnlCgreportItem>> list(OnlCgreportItem onlCgreportItem, @RequestParam(name = "pageNo",defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize, HttpServletRequest request) {
        Result<IPage<OnlCgreportItem>> result = new Result<>();
        QueryWrapper<OnlCgreportItem> qw = QueryGenerator.initQueryWrapper(onlCgreportItem, request.getParameterMap());
        Page<OnlCgreportItem> page = new Page<>(pageNo, pageSize);
        IPage<OnlCgreportItem> ipage = this.onlCgreportItemService.page(page, qw);
        result.setSuccess(true);
        result.setResult(ipage);
        return result;
    }

    @PostMapping({"/add"})
    public Result<?> add(@RequestBody OnlCgreportItem onlCgreportItem) {
        this.onlCgreportItemService.save(onlCgreportItem);
        return Result.OK("添加成功!", null);
    }

    @PutMapping({"/edit"})
    public Result<?> edit(@RequestBody OnlCgreportItem onlCgreportItem) {
        this.onlCgreportItemService.updateById(onlCgreportItem);
        return Result.OK("编辑成功!", null);
    }

    @DeleteMapping({"/delete"})
    public Result<?> delete(@RequestParam(name = "id",required = true) String id) {
        this.onlCgreportItemService.removeById(id);
        return Result.OK("删除成功!", null);
    }

    @DeleteMapping({"/deleteBatch"})
    public Result<?> deleteBatch(@RequestParam(name = "ids",required = true) String ids) {
        this.onlCgreportItemService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!", null);
    }

    @GetMapping({"/queryById"})
    public Result<OnlCgreportItem> queryById(@RequestParam(name = "id",required = true) String var1) {
        Result<OnlCgreportItem> result = new Result<>();
        OnlCgreportItem onlCgreportItem = this.onlCgreportItemService.getById(var1);
        result.setResult(onlCgreportItem);
        result.setSuccess(true);
        return result;
    }
}
