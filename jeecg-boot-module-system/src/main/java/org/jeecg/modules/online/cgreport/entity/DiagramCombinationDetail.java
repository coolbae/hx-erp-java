package org.jeecg.modules.online.cgreport.entity;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@TableName("CS_DIAGRAM_COMBINATION_DETAIL")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "图表组合明细", description = "图表组合明细")
public class DiagramCombinationDetail extends JeecgEntity {
	
	private static final long serialVersionUID = 8066142514369533464L;
	
	@Excel(name = "组合报表ID", width = 15)
	@ApiModelProperty(value = "组合报表ID")
	private String graphreportTempletId;
	
	@Excel(name = "图表编码", width = 15)
	@ApiModelProperty(value = "图表编码")
	private String graphreportCode;

	@Excel(name = "图表类型", width = 15)
	@ApiModelProperty(value = "图表类型")
	private String graphreportType;
	
	@Excel(name = "分组编号", width = 15)
	@ApiModelProperty(value = "分组编号")
	private Integer groupNum;
	
	@Excel(name = "分组展示风格", width = 15)
	@ApiModelProperty(value = "分组展示风格")
	private String groupStyle;
	
	@Excel(name = "分组描述", width = 15)
	@ApiModelProperty(value = "分组描述")
	private String groupTxt;
	
	@Excel(name = "分组栅格数", width = 15)
	@ApiModelProperty(value = "分组栅格数")
	private String groupGrid;
	
	@Excel(name = "组合报表栅格数", width = 15)
	@ApiModelProperty(value = "组合报表栅格数")
	private String templetGrid;
	
	@Excel(name = "排序", width = 15)
	@ApiModelProperty(value = "排序")
	private Integer orderNum;
	
	@Excel(name = "是否显示", width = 15)
	@ApiModelProperty(value = "是否显示")
	private String isShow;
	
}
