package org.jeecg.modules.online.cgreport.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.online.cgreport.entity.OnlCgreportHead;
import org.jeecg.modules.online.cgreport.model.OnlCgreportModel;

public interface IOnlCgreportHeadService extends IService<OnlCgreportHead> {
	/**
	 * 修改全部项，包括新增、修改、删除
	 * 
	 * @param values
	 * @return
	 */
	Result<?> editAll(OnlCgreportModel values);

    Result<?> delete(String id);

    Result<?> bathDelete(String[] ids);

    Map<String, Object> executeSelectSql(String sql, String onlCgreportHeadId, Map<String, Object> params) throws SQLException;

    Map<String, Object> executeSelectSqlDynamic(String dbKey, String sql, Map<String, Object> params, String onlCgreportHeadId);

    /**
	 * 动态数据源： 获取SQL解析的字段
	 */
	public List<String> getSqlFields(String sql,String dbKey) throws SQLException;

    List<String> getSqlParams(String var1);

    Map<String, Object> queryCgReportConfig(String var1);

    List<Map<?, ?>> queryByCgReportSql(String var1, Map var2, Map var3, int var4, int var5);
}
