package org.jeecg.modules.online.cgform.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.jeecg.modules.online.cgform.common.CommonEntity;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;
import org.jeecg.modules.online.cgform.entity.OnlCgformHead;
import org.jeecg.modules.online.cgform.model.TreeModel;

public interface IOnlCgformFieldService
    extends IService<OnlCgformField>
{

	/**
     * 查询表单的数据
     * @param tableName
     * @param headId
     * @param params
     * @param needList
     * @return
     */
	public Map<String, Object> queryAutolistPage(String tableName, String headId, Map<String, Object> params, List<String> needList);

    public Map<String, Object> queryAutoTreeNoPage(String tbname, String headId, Map<String, Object> params, List<String> needList, String pidField);

    /**
     * 删除表单
     * @param head
     * @param ids
     */
    public void deleteAutoListMainAndSub(OnlCgformHead head, String ids);
    
    /**
     * 删除表单
     * @param tableName
     * @param ids
     */
    public void deleteAutoListById(String tableName, String ids);

    /**
     * 删除表单
     * @param tableName
     * @param linkField
     * @param linkValue
     */
    public void deleteAutoList(String tableName, String linkField, String linkValue);

    /**
     * 保存表单的数据
     * @param code
     * @param tableName
     * @param json
     * @param isCrazy
     */
    public void saveFormData(String code, String tableName, JSONObject json, boolean isCrazy);

    /**
     * 保存树形
     * @param code
     * @param tableName
     * @param json
     * @param hasChildField
     * @param pidField
     */
    public void saveTreeFormData(String code, String tableName, JSONObject json, String hasChildField, String pidField);

    /**
     * 保存表单的数据
     * @param fieldList
     * @param tableName
     * @param json
     */
    public void saveFormData(List<OnlCgformField> fieldList, String tableName, JSONObject json);

    /**
     * 获取表单的字段
     * @param code
     * @param isForm
     * @return
     */
    public List<OnlCgformField> queryFormFields(String code, boolean isForm);

    /**
     * 通过表名获取字段
     * @param tableName
     * @return
     */
    public List<OnlCgformField> queryFormFieldsByTableName(String tableName);

    public abstract OnlCgformField queryFormFieldByTableNameAndField(String s, String s1);

    /**
     * 编辑树形
     * @param code
     * @param tableName
     * @param json
     * @param hasChildField
     * @param pidField
     */
    public void editTreeFormData(String code, String tableName, JSONObject json, String hasChildField, String pidField);

    /**
     * 编辑表单的数据
     * @param code
     * @param tableName
     * @param json
     * @param isCrazy
     */
    public void editFormData(String code, String tableName, JSONObject json, boolean isCrazy);

    public Map<String, Object> queryFormData(String code, String tbname, String id);

    /**
     * 获取表单的数据
     * @param fieldList
     * @param tableName
     * @param id
     * @return
     */
    public Map<String, Object> queryFormData(List<OnlCgformField> fieldList, String tableName, String id);

    /**
     * 获取表单的数据
     * @param fieldList
     * @param tableName
     * @param linkField
     * @param value
     * @return
     */
    public List<Map<String, Object>> querySubFormData(List<OnlCgformField> fieldList, String tableName, String linkField, String value);

    /**
     * 获取查询信息
     * @param code
     * @return
     */
    public List<Map<String, String>> getAutoListQueryInfo(String code);

    /**
     * 查询隐藏字段
     * @param tableName
     * @return
     */
    public List<String> selectOnlineHideColumns(String tableName);


    public IPage<Map<String, Object>> selectPageBySql(Page<Map<String, Object>> page, String sql);

    /**
     * 查询可用字段
     * @param cgFormId
     * @param tableName
     * @param taskId
     * @param isList
     * @return
     */
    public List<OnlCgformField> queryAvailableFields(String cgFormId, String tableName, String taskId, boolean isList);

    /**
     * 查询被禁用字段
     * @param tableName
     * @return
     */
    public List<String> queryDisabledFields(String tableName);

    /**
     * 查询被禁用字段
     * @param tableName
     * @param taskId
     * @return
     */
    public List<String> queryDisabledFields(String tbname, String taskId);

    /**
     * 查询可用字段
     * @param tableName
     * @param isList
     * @param onlCgformFieldList
     * @param needList
     * @return
     */
    public List<OnlCgformField> queryAvailableFields(String tableName, boolean isList, List<OnlCgformField> onlCgformFieldList, List<String> needList);

    /**
     * 执行插入语句
     * @param params
     */
    public void executeInsertSQL(Map<String, Object> params);

    /**
     * 执行更新语句
     * @param params
     */
    public void executeUpdatetSQL(Map<String, Object> params);

    /**
     * 通过linkDown获取树形数据
     * @param linkDown
     * @return
     */
    public List<TreeModel> queryDataListByLinkDown(CommonEntity a);

    /**
     * 更新无孩子节点
     * @param tableName
     * @param filed
     * @param id
     */
    public void updateTreeNodeNoChild(String tableName, String filed, String id);

    public String queryTreeChildIds(OnlCgformHead onlcgformhead, String s);

	public void editFormBpmStatus(String formDataId, String tableName, String string);
	
	public void editFormData(String formDataId, String tableName, Map<String, Object> data);

	public Map<String, Object> queryBpmData(String tableId, String tableName, String formDataId);
}
