package org.jeecg.modules.online.cgreport.service;

import java.util.List;
import java.util.Map;

import org.jeecg.modules.online.cgreport.entity.DiagramConfiguration;
import org.jeecg.modules.online.cgreport.entity.DiagramFieldConfiguration;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 图表配置
 */
public interface IDiagramConfigurationService extends IService<DiagramConfiguration> {
	
	void add(DiagramConfiguration diagramConfiguration);

	void edit(DiagramConfiguration diagramConfiguration);

	void deleteById(String id);

	void deleteBatch(String ids);
	
	List selectBySql(String sql);

	/**
	 * 封装树形列表数据
	 * @param dataList 原始数据
	 * @param unfoldFieldName 展开列名称
	 * @param pid 父节点ID
	 * @return
	 */
    List<Map<String, Object>> handelTreeTableData(List<Map<String, Object>> dataList, String unfoldFieldName, String pid);

	/**
	 * 封装查询sql
	 * @param jsonObject
	 * @return
	 */
	String handelQuerySql(String cgrSql, JSONObject jsonObject, List<DiagramFieldConfiguration> diagramFieldConfigurations);
}
