package org.jeecg.modules.online.cgform.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.entity.OnlCgformButton;
import org.jeecg.modules.online.cgform.entity.OnlCgformEnhanceJs;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EnhanceJsUtil {
    private static final String actions = "beforeAdd,beforeEdit,afterAdd,afterEdit,beforeDelete,afterDelete,mounted,created,show,loaded";
    private static final String interval = "\\}\\s*\r*\n*\\s*";
    private static final String separator = ",";

    public static void main(String[] args) {
        String var1 = "aa(row){console.log(112);}\nbb(row){console.log(row);}";
        String var2 = "bb";
        System.out.println(getCgJs(var1, var2));
        List<OnlCgformButton> buttons = new ArrayList<>();
        OnlCgformButton button = new OnlCgformButton();
        button.setButtonCode("bb");
        button.setButtonStyle("link");
        buttons.add(button);
        OnlCgformButton button2 = new OnlCgformButton();
        button2.setButtonCode("aa");
        button2.setButtonStyle("link");
        buttons.add(button2);
        System.out.println(getJsFunction(var1, buttons));
    }

    public static String getCgJs(String cgJs, String buttonCode) {
        String var2 = "(" + buttonCode + "\\s*\\(row\\)\\s*\\{)";
        String var3 = buttonCode + ":function(that,row){const getAction=this._getAction,postAction=this._postAction,deleteAction=this._deleteAction;";
        String var4 = getJsFunction(cgJs, interval + var2, "}," + var3);
        if (var4 == null) {
        	cgJs = getRegexCgJs(cgJs, var2, var3);
        } else {
        	cgJs = var4;
        }

        cgJs = getCgJs(cgJs, buttonCode, null);
        return cgJs;
    }

    public static String getCgJs(String cgJs, String buttonCode, String s) {
        String s1  = "(" + oConvertUtils.getString(s) + buttonCode + "\\s*\\(\\)\\s*\\{)";
        String s2 = buttonCode + ":function(that){const getAction=this._getAction,postAction=this._postAction,deleteAction=this._deleteAction;";
        String jsFunction = getJsFunction(cgJs, interval + s1, "}," + s2);
        if (jsFunction == null) {
        	cgJs = getRegexCgJs(cgJs, s1, s2);
        } else {
        	cgJs = jsFunction;
        }

        return cgJs;
    }

    public static String getJsFunction(String cgJs, String regex, String s2) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(cgJs);
        if (matcher.find()) {
        	cgJs = cgJs.replace(matcher.group(0), s2);
            return cgJs;
        } else {
            return null;
        }
    }

    public static String getRegexCgJs(String cgJs, String regex, String s1) {
        String jsFunction = getJsFunction(cgJs, regex, s1);
        return jsFunction != null ? jsFunction : cgJs;
    }

    /*public static String getCgJs(String var0, List<OnlCgformButton> var1) {
        log.info("最终的增强JS", var0);
        return "class OnlineEnhanceJs{constructor(getAction,postAction,deleteAction){this._getAction=getAction;this._postAction=postAction;this._deleteAction=deleteAction;}" + var0 + "}";
    }*/

    public static String getJsFunction(String cgJs, String dbFieldName) {
        String var2 = "(\\s+" + dbFieldName + "\\s*\\(\\)\\s*\\{)";
        String var3 = dbFieldName + ":function(that,event){";
        String jsFunction = getJsFunction(cgJs, interval + var2, "}," + var3);
        if (jsFunction == null) {
        	cgJs = getRegexCgJs(cgJs, var2, var3);
        } else {
        	cgJs = jsFunction;
        }

        return cgJs;
    }

    public static String getCgJs(String cgJs) {
        String s = "function OnlineEnhanceJs(getAction,postAction,deleteAction){return {_getAction:getAction,_postAction:postAction,_deleteAction:deleteAction," + cgJs + "}}";
        log.info("最终的增强JS", s);
        return s;
    }

    public static String getJsFunction(String cgJs, List<OnlCgformButton> onlCgformButtonList) {
    	cgJs = getCgJs(cgJs, onlCgformButtonList);
        String s = "function OnlineEnhanceJs(getAction,postAction,deleteAction){return {_getAction:getAction,_postAction:postAction,_deleteAction:deleteAction," + cgJs + "}}";
        log.info("最终的增强JS", s);
        return s;
    }

    public static String getCgJs(String cgJs, List<OnlCgformButton> buttons) {
        if (buttons != null) {
            Iterator<OnlCgformButton> iterator = buttons.iterator();
            
            while(iterator.hasNext()) {
            	OnlCgformButton button = iterator.next();
                String code = button.getButtonCode();
                if ("link".equals(button.getButtonStyle())) {
                	cgJs = getCgJs(cgJs, code);
                } else if ("button".equals(button.getButtonStyle()) || "form".equals(button.getButtonStyle())) {
                	cgJs = getCgJs(cgJs, code, null);
                }
            }
        }

        String[] actionArr = actions.split(separator);
        int length = actionArr.length;

        for(int i = 0; i < length; ++i) {
            String action = actionArr[i];
            if ("beforeAdd,afterAdd,mounted,created,show,loaded".indexOf(action) >= 0) {
            	cgJs = getCgJs(cgJs, action, null);
            } else {
            	cgJs = getCgJs(cgJs, action);
            }
        }

        return cgJs;
    }

    public static void a(OnlCgformEnhanceJs onlCgformEnhanceJs, String var1, List<OnlCgformField> fields) {
        if (onlCgformEnhanceJs != null && !oConvertUtils.isEmpty(onlCgformEnhanceJs.getCgJs())) {
            String var3 = " " + onlCgformEnhanceJs.getCgJs();
            log.info("one enhanceJs begin==> " + var3);
            Pattern var4 = Pattern.compile("(\\s{1}onlChange\\s*\\(\\)\\s*\\{)");
            Matcher var5 = var4.matcher(var3);
            if (var5.find()) {
            	log.info("---JS 增强转换-main--enhanceJsFunctionName----onlChange");
                var3 = getCgJs(var3, "onlChange", "\\s{1}");

                OnlCgformField var7;
                for(Iterator<OnlCgformField> var6 = fields.iterator(); var6.hasNext(); var3 = getJsFunction(var3, var7.getDbFieldName())) {
                    var7 = var6.next();
                }
            }

            log.info("one enhanceJs end==> " + var3);
            onlCgformEnhanceJs.setCgJs(var3);
        }
    }

    public static void getJsFunction(OnlCgformEnhanceJs onlCgformEnhanceJs, String var1, List<OnlCgformField> fields) {
        if (onlCgformEnhanceJs != null && !oConvertUtils.isEmpty(onlCgformEnhanceJs.getCgJs())) {
        	log.info(" sub enhanceJs begin==> " + onlCgformEnhanceJs);
            String var3 = onlCgformEnhanceJs.getCgJs();
            String var4 = var1 + "_" + "onlChange";
            Pattern var5 = Pattern.compile("(" + var4 + "\\s*\\(\\)\\s*\\{)");
            Matcher var6 = var5.matcher(var3);
            if (var6.find()) {
            	log.info("---JS 增强转换-sub-- enhanceJsFunctionName----" + var4);
                var3 = getCgJs(var3, var4, null);

                OnlCgformField var8;
                for(Iterator<OnlCgformField> var7 = fields.iterator(); var7.hasNext(); var3 = getJsFunction(var3, var8.getDbFieldName())) {
                    var8 = var7.next();
                }
            }

            log.info(" sub enhanceJs end==> " + var3);
            onlCgformEnhanceJs.setCgJs(var3);
        }
    }
}
