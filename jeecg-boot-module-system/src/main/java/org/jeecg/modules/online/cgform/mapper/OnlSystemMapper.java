package org.jeecg.modules.online.cgform.mapper;

import org.jeecg.modules.online.cgform.entity.OnlSystem;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 子系统
 * @Author: MxpIO
 * @Date:   2020-12-09
 * @Version: V1.0
 */
public interface OnlSystemMapper extends BaseMapper<OnlSystem> {

}
