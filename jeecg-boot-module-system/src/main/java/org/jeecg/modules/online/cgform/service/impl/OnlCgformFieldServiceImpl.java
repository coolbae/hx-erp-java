package org.jeecg.modules.online.cgform.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.common.CommonEntity;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;
import org.jeecg.modules.online.cgform.entity.OnlCgformHead;
import org.jeecg.modules.online.cgform.mapper.OnlCgformFieldMapper;
import org.jeecg.modules.online.cgform.mapper.OnlCgformHeadMapper;
import org.jeecg.modules.online.cgform.model.TreeModel;
import org.jeecg.modules.online.cgform.service.IOnlCgformFieldService;
import org.jeecg.modules.online.cgform.util.SqlSymbolUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class OnlCgformFieldServiceImpl extends ServiceImpl<OnlCgformFieldMapper, OnlCgformField> implements IOnlCgformFieldService {
	@Resource
    private OnlCgformFieldMapper onlCgformFieldMapper;

    @Resource
    private OnlCgformHeadMapper cgformHeadMapper;

    public Map<String, Object> queryAutolistPage(String tableName, String headId, Map<String, Object> params, List<String> needList) {
        HashMap<String, Object> result = new HashMap<>();
        LambdaQueryWrapper<OnlCgformField> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OnlCgformField::getCgformHeadId, headId);
        queryWrapper.orderByAsc(OnlCgformField::getOrderNum);
        List<OnlCgformField> onlCgformFields = this.list(queryWrapper);
        List<OnlCgformField> onlCgformFields1 = this.queryAvailableFields(tableName, true, onlCgformFields, needList);
        StringBuffer sbf = new StringBuffer();
        SqlSymbolUtil.getSelect(tableName, onlCgformFields1, sbf);
        String sql = SqlSymbolUtil.getByDataType(onlCgformFields, params, needList);
        String sql1 = SqlSymbolUtil.getByParams(params);
        sbf.append(" where 1=1  " + sql + sql1);
        Object column = params.get("column");
        if (column != null) {
            String columnStr = column.toString();
            String orderStr = params.get("order").toString();
            if (this.a(columnStr, onlCgformFields)) {
                sbf.append(" ORDER BY " + oConvertUtils.camelToUnderline(columnStr));
                if ("asc".equals(orderStr)) {
                    sbf.append(" asc");
                } else {
                    sbf.append(" desc");
                }
            }
        }

        Integer pageSzie = params.get("pageSize") == null ? 10 : Integer.parseInt(params.get("pageSize").toString());
        if (pageSzie == -521) {
            List<Map<String, Object>> onlCgformFieldList = this.onlCgformFieldMapper.queryListBySql(sbf.toString());
            log.info("---Online查询sql 不分页 :>>" + sbf.toString());
            if (onlCgformFieldList != null && onlCgformFieldList.size() != 0) {
                result.put("total", onlCgformFieldList.size());
                result.put("fieldList", onlCgformFields1);
                result.put("records", SqlSymbolUtil.transforRecords(onlCgformFieldList));
            } else {
                result.put("total", 0);
                result.put("fieldList", onlCgformFields1);
            }
        } else {
            Integer pageNo = params.get("pageNo") == null ? 1 : Integer.parseInt(params.get("pageNo").toString());
            Page<Map<String, Object>> page = new Page<>(pageNo, pageSzie);
            log.info("---Online查询sql:>>" + sbf.toString());
            IPage<Map<String, Object>> ipage = this.onlCgformFieldMapper.selectPageBySql(page, sbf.toString());
            result.put("total", ipage.getTotal());
            result.put("records", SqlSymbolUtil.transforRecords(ipage.getRecords()));
        }

        return result;
    }

    public Map<String, Object> queryAutoTreeNoPage(String tbname, String headId, Map<String, Object> params, List<String> needList, String pidField) {
        Map<String, Object> var6 = new HashMap<>();
        LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<>();
        qw.eq(OnlCgformField::getCgformHeadId, headId);
        qw.orderByAsc(OnlCgformField::getOrderNum);
        List<OnlCgformField> fields = this.list(qw);
        List<OnlCgformField> var9 = this.queryAvailableFields(tbname, true, fields, needList);
        StringBuffer var10 = new StringBuffer();
        SqlSymbolUtil.getSelect(tbname, var9, var10);
        String dataTypes = SqlSymbolUtil.getByDataType(fields, params, needList);
        String paramsSql = SqlSymbolUtil.getByParams(params);
        var10.append(" where 1=1  " + dataTypes + paramsSql);
        Object var13 = params.get("column");
        if (var13 != null) {
            String var14 = var13.toString();
            String var15 = params.get("order").toString();
            if (this.a(var14, fields)) {
                var10.append(" ORDER BY " + oConvertUtils.camelToUnderline(var14));
                if ("asc".equals(var15)) {
                    var10.append(" asc");
                } else {
                    var10.append(" desc");
                }
            }
        }

        Integer var21 = params.get("pageSize") == null ? 10 : Integer.parseInt(params.get("pageSize").toString());
        if (var21 == -521) {
        	List<Map<String, Object>> var22 = this.onlCgformFieldMapper.queryListBySql(var10.toString());
            if ("true".equals(params.get("hasQuery"))) {
                List<Map<String, Object>> var16 = new ArrayList<>();
                Iterator<Map<String, Object>> var17 = var22.iterator();

                while(true) {
                    while(var17.hasNext()) {
                    	Map<String, Object> var18 = var17.next();
                        String var19 = var18.get(pidField).toString();
                        if (var19 != null && !"0".equals(var19)) {
                        	Map<String, Object> var20 = this.a(var19, tbname, headId, needList, pidField);
                            if (var20 != null && var20.size() > 0 && !var16.contains(var20)) {
                                var16.add(var20);
                            }
                        } else if (!var16.contains(var18)) {
                            var16.add(var18);
                        }
                    }

                    var22 = var16;
                    break;
                }
            }

            log.info("---Online查询sql 不分页 :>>" + var10.toString());
            if (var22 != null && var22.size() != 0) {
                var6.put("total", var22.size());
                var6.put("fieldList", var9);
                var6.put("records", SqlSymbolUtil.transforRecords(var22));
            } else {
                var6.put("total", 0);
                var6.put("fieldList", var9);
            }
        } else {
            Integer var23 = params.get("pageNo") == null ? 1 : Integer.parseInt(params.get("pageNo").toString());
            Page var24 = new Page(var23, var21);
            log.info("---Online查询sql:>>" + var10.toString());
            IPage var25 = this.onlCgformFieldMapper.selectPageBySql(var24, var10.toString());
            var6.put("total", var25.getTotal());
            var6.put("records", SqlSymbolUtil.transforRecords(var25.getRecords()));
        }

        return var6;
    }

    private Map<String, Object> a(String var1, String var2, String var3, List<String> var4, String var5) {
        HashMap var6 = new HashMap();
        var6.put("id", var1);
        LambdaQueryWrapper<OnlCgformField> var7 = new LambdaQueryWrapper();
        var7.eq(OnlCgformField::getCgformHeadId, var3);
        var7.orderByAsc(OnlCgformField::getOrderNum);
        List var8 = this.list(var7);
        List var9 = this.queryAvailableFields(var2, true, var8, var4);
        StringBuffer var10 = new StringBuffer();
        SqlSymbolUtil.getSelect(var2, var9, var10);
        String var11 = SqlSymbolUtil.getByDataType(var8, var6, var4);
        var10.append(" where 1=1  " + var11 + "and id='" + var1 + "'");
        List var12 = this.onlCgformFieldMapper.queryListBySql(var10.toString());
        if (var12 != null && var12.size() > 0) {
            Map var13 = (Map)var12.get(0);
            return var13 != null && var13.get(var5) != null && !"0".equals(var13.get(var5)) ? this.a(var13.get(var5).toString(), var2, var3, var4, var5) : var13;
        } else {
            return null;
        }
    }

    public void saveFormData(String code, String tbname, JSONObject json, boolean isCrazy) {
        LambdaQueryWrapper<OnlCgformField> var5 = new LambdaQueryWrapper();
        var5.eq(OnlCgformField::getCgformHeadId, code);
        List var6 = this.list(var5);
        if (isCrazy) {
            ((OnlCgformFieldMapper)this.baseMapper).executeInsertSQL(SqlSymbolUtil.c(tbname, var6, json));
        } else {
            ((OnlCgformFieldMapper)this.baseMapper).executeInsertSQL(SqlSymbolUtil.getInsertSql(tbname, var6, json));
        }

    }

    public void saveTreeFormData(String code, String tbname, JSONObject json, String hasChildField, String pidField) {
        LambdaQueryWrapper<OnlCgformField> var6 = new LambdaQueryWrapper();
        var6.eq(OnlCgformField::getCgformHeadId, code);
        List var7 = this.list(var6);
        Iterator var8 = var7.iterator();

        while(true) {
            while(var8.hasNext()) {
                OnlCgformField var9 = (OnlCgformField)var8.next();
                if (hasChildField.equals(var9.getDbFieldName()) && var9.getIsShowForm() != 1) {
                    var9.setIsShowForm(1);
                    json.put(hasChildField, "0");
                } else if (pidField.equals(var9.getDbFieldName()) && oConvertUtils.isEmpty(json.get(pidField))) {
                    var9.setIsShowForm(1);
                    json.put(pidField, "0");
                }
            }

            Map var10 = SqlSymbolUtil.getInsertSql(tbname, var7, json);
            ((OnlCgformFieldMapper)this.baseMapper).executeInsertSQL(var10);
            if (!"0".equals(json.getString(pidField))) {
                ((OnlCgformFieldMapper)this.baseMapper).editFormData("update " + tbname + " set " + hasChildField + " = '1' where id = '" + json.getString(pidField) + "'");
            }

            return;
        }
    }

    public void saveFormData(List fieldList, String tbname, JSONObject json) {
        Map var4 = SqlSymbolUtil.getInsertSql(tbname, fieldList, json);
        ((OnlCgformFieldMapper)this.baseMapper).executeInsertSQL(var4);
    }

    public void editFormData(String code, String tbname, JSONObject json, boolean isCrazy) {
        LambdaQueryWrapper<OnlCgformField> var5 = new LambdaQueryWrapper();
        var5.eq(OnlCgformField::getCgformHeadId, code);
        List var6 = this.list(var5);
        if (isCrazy) {
            this.baseMapper.executeUpdatetSQL(SqlSymbolUtil.d(tbname, var6, json));
        } else {
            this.baseMapper.executeUpdatetSQL(SqlSymbolUtil.getUpdateSql(tbname, var6, json));
        }
    }
    
    public void editFormBpmStatus(String code, String tbname, String bpmStatus){
    	Map<String, Object> hashMap = new HashMap<>();
    	String sql = "update " + SqlSymbolUtil.getSubstring(tbname) + " set bpm_status = '"+ bpmStatus +"' where 1=1  " + " AND " + "id" + "=" + "'" + code + "'";
        log.info("--动态表单编辑sql-->" + sql);
        hashMap.put("execute_sql_string", sql);
    	this.baseMapper.executeUpdatetSQL(hashMap);
    }
    
	@Override
	public void editFormData(String formDataId, String tableName, Map<String, Object> data) {
		StringBuffer sql = new StringBuffer("update " + SqlSymbolUtil.getSubstring(tableName) + " set ");
		for(Entry<String, Object> entry : data.entrySet()){
			if(entry.getValue() instanceof String){
				sql = sql.append(entry.getKey()+" = '" + entry.getValue().toString() + "',");
			}else if(entry.getValue() instanceof Number) {
				sql = sql.append(entry.getKey()+" = " + entry.getValue().toString() + ",");
			}
		}
		sql.setLength(sql.length()-1);
		sql.append(" where 1=1  " + " AND " + "id" + "=" + "'" + formDataId + "'");
		Map<String, Object> hashMap = new HashMap<>();
		log.info("--动态表单编辑sql-->" + sql.toString());
        hashMap.put("execute_sql_string", sql.toString());
    	this.baseMapper.executeUpdatetSQL(hashMap);
	}

    public void editTreeFormData(String code, String tbname, JSONObject json, String hasChildField, String pidField) {
        String var6 = SqlSymbolUtil.getSubstring(tbname);
        String var7 = "select * from " + var6 + " where id = '" + json.getString("id") + "'";
        Map var8 = ((OnlCgformFieldMapper)this.baseMapper).queryFormData(var7);
        Map var9 = SqlSymbolUtil.getValueType(var8);
        String var10 = var9.get(pidField).toString();
        LambdaQueryWrapper<OnlCgformField> var11 = new LambdaQueryWrapper();
        var11.eq(OnlCgformField::getCgformHeadId, code);
        List var12 = this.list(var11);
        Iterator var13 = var12.iterator();

        while(var13.hasNext()) {
            OnlCgformField var14 = (OnlCgformField)var13.next();
            if (pidField.equals(var14.getDbFieldName()) && oConvertUtils.isEmpty(json.get(pidField))) {
                var14.setIsShowForm(1);
                json.put(pidField, "0");
            }
        }

        Map var16 = SqlSymbolUtil.getUpdateSql(tbname, var12, json);
        ((OnlCgformFieldMapper)this.baseMapper).executeUpdatetSQL(var16);
        if (!var10.equals(json.getString(pidField))) {
            if (!"0".equals(var10)) {
                String var17 = "select count(*) from " + var6 + " where " + pidField + " = '" + var10 + "'";
                Integer var15 = ((OnlCgformFieldMapper)this.baseMapper).queryCountBySql(var17);
                if (var15 == null || var15 == 0) {
                    ((OnlCgformFieldMapper)this.baseMapper).editFormData("update " + var6 + " set " + hasChildField + " = '0' where id = '" + var10 + "'");
                }
            }

            if (!"0".equals(json.getString(pidField))) {
                ((OnlCgformFieldMapper)this.baseMapper).editFormData("update " + var6 + " set " + hasChildField + " = '1' where id = '" + json.getString(pidField) + "'");
            }
        }

    }

    public Map<String, Object> queryFormData(String code, String tbname, String id) {
        LambdaQueryWrapper<OnlCgformField> var4 = new LambdaQueryWrapper();
        var4.eq(OnlCgformField::getCgformHeadId, code);
        var4.eq(OnlCgformField::getIsShowForm, 1);
        List var5 = this.list(var4);
        String var6 = SqlSymbolUtil.getSelectSql(tbname, var5, id);
        return this.onlCgformFieldMapper.queryFormData(var6);
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    public void deleteAutoListMainAndSub(OnlCgformHead head, String ids) {
        if (head.getTableType() == 2) {
            String var3 = head.getId();
            String var4 = head.getTableName();
            String var5 = "tableName";
            String var6 = "linkField";
            String var7 = "linkValueStr";
            String var8 = "mainField";
            ArrayList var9 = new ArrayList();
            if (oConvertUtils.isNotEmpty(head.getSubTableStr())) {
                String[] var10 = head.getSubTableStr().split(",");
                int var11 = var10.length;

                for(int var12 = 0; var12 < var11; ++var12) {
                    String var13 = var10[var12];
                    OnlCgformHead var14 = (OnlCgformHead)this.cgformHeadMapper.selectOne((Wrapper)(new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, var13));
                    if (var14 != null) {
                        LambdaQueryWrapper<OnlCgformField> var15 = (LambdaQueryWrapper<OnlCgformField>)((LambdaQueryWrapper<OnlCgformField>)(new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, var14.getId())).eq(OnlCgformField::getMainTable, head.getTableName());
                        List var16 = this.list(var15);
                        if (var16 != null && var16.size() != 0) {
                            OnlCgformField var17 = (OnlCgformField)var16.get(0);
                            HashMap var18 = new HashMap();
                            var18.put(var6, var17.getDbFieldName());
                            var18.put(var8, var17.getMainField());
                            var18.put(var5, var13);
                            var18.put(var7, "");
                            var9.add(var18);
                        }
                    }
                }

                LambdaQueryWrapper<OnlCgformField> var24 = new LambdaQueryWrapper();
                var24.eq(OnlCgformField::getCgformHeadId, var3);
                List var25 = this.list(var24);
                String[] var26 = ids.split(",");
                String[] var27 = var26;
                int var29 = var26.length;
                int var31 = 0;

                label52:
                while(true) {
                    if (var31 >= var29) {
                        Iterator var28 = var9.iterator();

                        while(true) {
                            if (!var28.hasNext()) {
                                break label52;
                            }

                            Map var30 = (Map)var28.next();
                            this.deleteAutoList((String)var30.get(var5), (String)var30.get(var6), (String)var30.get(var7));
                        }
                    }

                    String var32 = var27[var31];
                    String var33 = SqlSymbolUtil.getSelectSql(var4, var25, var32);
                    Map var34 = this.onlCgformFieldMapper.queryFormData(var33);
                    new ArrayList();
                    Iterator var20 = var9.iterator();

                    while(var20.hasNext()) {
                        Map var21 = (Map)var20.next();
                        Object var22 = var34.get(((String)var21.get(var8)).toLowerCase());
                        if (var22 == null) {
                            var22 = var34.get(((String)var21.get(var8)).toUpperCase());
                        }

                        if (var22 != null) {
                            String var23 = (String)var21.get(var7) + String.valueOf(var22) + ",";
                            var21.put(var7, var23);
                        }
                    }

                    ++var31;
                }
            }

            this.deleteAutoListById(head.getTableName(), ids);
        }

    }

    public void deleteAutoListById(String tbname, String ids) {
        this.deleteAutoList(tbname, "id", ids);
    }

    public void deleteAutoList(String tbname, String linkField, String linkValue) {
        if (linkValue != null && !"".equals(linkValue)) {
            String[] var4 = linkValue.split(",");
            StringBuffer var5 = new StringBuffer();
            String[] var6 = var4;
            int var7 = var4.length;

            for(int var8 = 0; var8 < var7; ++var8) {
                String var9 = var6[var8];
                if (var9 != null && !"".equals(var9)) {
                    var5.append("'" + var9 + "',");
                }
            }

            String var10 = var5.toString();
            String var11 = "DELETE FROM " + SqlSymbolUtil.getSubstring(tbname) + " where " + linkField + " in(" + var10.substring(0, var10.length() - 1) + ")";
            log.info("--删除sql-->" + var11);
            this.onlCgformFieldMapper.deleteAutoList(var11);
        }

    }

    public List<Map<String, String>> getAutoListQueryInfo(String code) {
        LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<>();
        qw.eq(OnlCgformField::getCgformHeadId, code);
        qw.eq(OnlCgformField::getIsQuery, 1);
        qw.orderByAsc(OnlCgformField::getOrderNum);
        List<OnlCgformField> fields = this.list(qw);
        List<Map<String, String>> columns = new ArrayList<>();
        int i = 0;

        Map<String, String> column;
        for(Iterator<OnlCgformField> iterator = fields.iterator(); iterator.hasNext(); columns.add(column)) {
            OnlCgformField field = iterator.next();
            column = new HashMap<String, String>();
            column.put("label", field.getDbFieldTxt());
            column.put("field", field.getDbFieldName());
            column.put("mode", field.getQueryMode());
            String[] dictTexts;
            String dictTable;
            if ("1".equals(field.getQueryConfigFlag())) {
            	column.put("config", "1");
            	column.put("view", field.getQueryShowType());
            	column.put("defValue", field.getQueryDefVal());
                if ("cat_tree".equals(field.getFieldShowType())) {
                	column.put("pcode", field.getQueryDictField());
                } else if ("sel_tree".equals(field.getFieldShowType())) {
                	dictTexts = field.getQueryDictText().split(",");
                    dictTable = field.getQueryDictTable() + "," + dictTexts[2] + "," + dictTexts[0];
                    column.put("dict", dictTable);
                    column.put("pidField", dictTexts[1]);
                    column.put("hasChildField", dictTexts[3]);
                    column.put("pidValue", field.getQueryDictField());
                } else {
                	column.put("dictTable", field.getQueryDictTable());
                	column.put("dictCode", field.getQueryDictField());
                	column.put("dictText", field.getQueryDictText());
                }
            } else {
            	column.put("view", field.getFieldShowType());
                if ("cat_tree".equals(field.getFieldShowType())) {
                	column.put("pcode", field.getDictField());
                } else if ("sel_tree".equals(field.getFieldShowType())) {
                	dictTexts = field.getDictText().split(",");
                	dictTable = field.getDictTable() + "," + dictTexts[2] + "," + dictTexts[0];
                    column.put("dict", dictTable);
                    column.put("pidField", dictTexts[1]);
                    column.put("hasChildField", dictTexts[3]);
                    column.put("pidValue", field.getDictField());
                } else if ("popup".equals(field.getFieldShowType())) {
                	column.put("dictTable", field.getDictTable());
                	column.put("dictCode", field.getDictField());
                	column.put("dictText", field.getDictText());
                }

                column.put("mode", field.getQueryMode());
            }

            ++i;
            if (i > 2) {
            	column.put("hidden", "1");
            }
        }

        return columns;
    }

    public List<OnlCgformField> queryFormFields(String code, boolean isform) {
        LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<>();
        qw.eq(OnlCgformField::getCgformHeadId, code);
        if (isform) {
        	qw.eq(OnlCgformField::getIsShowForm, 1);
        }

        return this.list(qw);
    }

    public List<OnlCgformField> queryFormFieldsByTableName(String tableName) {
        OnlCgformHead head = this.cgformHeadMapper.selectOne(new LambdaQueryWrapper<OnlCgformHead>().eq(OnlCgformHead::getTableName, tableName));
        if (head != null) {
            LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<>();
            qw.eq(OnlCgformField::getCgformHeadId, head.getId());
            return this.list(qw);
        } else {
            return null;
        }
    }

    public OnlCgformField queryFormFieldByTableNameAndField(String tableName, String fieldName) {
        OnlCgformHead head = this.cgformHeadMapper.selectOne(new LambdaQueryWrapper<OnlCgformHead>().eq(OnlCgformHead::getTableName, tableName));
        if (head != null) {
            LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<>();
            qw.eq(OnlCgformField::getCgformHeadId, head.getId());
            qw.eq(OnlCgformField::getDbFieldName, fieldName);
            if (this.list(qw) != null && this.list(qw).size() > 0) {
                return this.list(qw).get(0);
            }
        }

        return null;
    }

    public Map<String, Object> queryFormData(List<OnlCgformField> fieldList, String tbname, String id) {
        String sql = SqlSymbolUtil.getSelectSql(tbname, fieldList, id);
        return this.onlCgformFieldMapper.queryFormData(sql);
    }
    
    public Map<String, Object> queryBpmData(String tableId, String tbname, String id){
    	LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<>();
    	qw.eq(OnlCgformField::getCgformHeadId, tableId);
    	List<OnlCgformField> field = this.list(qw);
    	String sql = SqlSymbolUtil.getSelectSql(tbname, field, id);
    	return this.onlCgformFieldMapper.queryFormData(sql);
    }

    public List<Map<String, Object>> querySubFormData(List<OnlCgformField> fieldList, String tbname, String linkField, String value) {
        String sql = SqlSymbolUtil.getSelectSql(tbname, fieldList, linkField, value);
        return this.onlCgformFieldMapper.queryListData(sql);
    }

    public IPage<Map<String, Object>> selectPageBySql(Page page, String sql) {
        return ((OnlCgformFieldMapper)this.baseMapper).selectPageBySql(page, sql);
    }

    public List<String> selectOnlineHideColumns(String tbname) {
        String var2 = "online:" + tbname + ":%";
        LoginUser var3 = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        String var4 = var3.getId();
        List var5 = ((OnlCgformFieldMapper)this.baseMapper).selectOnlineHideColumns(var4, var2);
        return this.a(var5);
    }

    public List<OnlCgformField> queryAvailableFields(String cgFormId, String tbname, String taskId, boolean isList) {
        LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<OnlCgformField>();
        qw.eq(OnlCgformField::getCgformHeadId, cgFormId);
        if (isList) {
        	qw.eq(OnlCgformField::getIsShowList, 1);
        } else {
        	qw.eq(OnlCgformField::getIsShowForm, 1);
        }

        qw.orderByAsc(OnlCgformField::getOrderNum);
        List<OnlCgformField> fields = this.list(qw);
        String var7 = "online:" + tbname + "%";
        LoginUser loginUser = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        String userId = loginUser.getId();
        ArrayList var10 = new ArrayList();
        List var11;
        if (oConvertUtils.isEmpty(taskId)) {
            var11 = ((OnlCgformFieldMapper)this.baseMapper).selectOnlineHideColumns(userId, var7);
            if (var11 != null && var11.size() != 0 && var11.get(0) != null) {
                var10.addAll(var11);
            }
        } else if (oConvertUtils.isNotEmpty(taskId)) {
            var11 = ((OnlCgformFieldMapper)this.baseMapper).selectFlowAuthColumns(tbname, taskId, "1");
            if (var11 != null && var11.size() > 0 && var11.get(0) != null) {
                var10.addAll(var11);
            }
        }

        if (var10.size() == 0) {
            return fields;
        } else {
            List<OnlCgformField> var14 = new ArrayList();

            for(int var12 = 0; var12 < fields.size(); ++var12) {
                OnlCgformField var13 = fields.get(var12);
                if (this.b(var13.getDbFieldName(), var10)) {
                    var14.add(var13);
                }
            }

            return var14;
        }
    }

    public List<String> queryDisabledFields(String tbname) {
        String var2 = "online:" + tbname + "%";
        LoginUser var3 = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        String var4 = var3.getId();
        List var5 = ((OnlCgformFieldMapper)this.baseMapper).selectOnlineDisabledColumns(var4, var2);
        return this.a(var5);
    }

    public List<String> queryDisabledFields(String tbname, String taskId) {
        if (oConvertUtils.isEmpty(taskId)) {
            return null;
        } else {
            List var3 = ((OnlCgformFieldMapper)this.baseMapper).selectFlowAuthColumns(tbname, taskId, "2");
            return this.a(var3);
        }
    }

    private List<String> a(List<String> var1) {
        ArrayList var2 = new ArrayList();
        if (var1 != null && var1.size() != 0 && var1.get(0) != null) {
            Iterator var3 = var1.iterator();

            while(var3.hasNext()) {
                String var4 = (String)var3.next();
                if (!oConvertUtils.isEmpty(var4)) {
                    String var5 = var4.substring(var4.lastIndexOf(":") + 1);
                    if (!oConvertUtils.isEmpty(var5)) {
                        var2.add(var5);
                    }
                }
            }

            return var2;
        } else {
            return var2;
        }
    }

    public List<OnlCgformField> queryAvailableFields(String tbname, boolean isList, List List, List needList) {
        ArrayList var5 = new ArrayList();
        String var6 = "online:" + tbname + "%";
        LoginUser var7 = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        String var8 = var7.getId();
        List var9 = ((OnlCgformFieldMapper)this.baseMapper).selectOnlineHideColumns(var8, var6);
        boolean var10 = true;
        if (var9 == null || var9.size() == 0 || var9.get(0) == null) {
            var10 = false;
        }

        Iterator var11 = List.iterator();

        while(true) {
            while(var11.hasNext()) {
                OnlCgformField var12 = (OnlCgformField)var11.next();
                String var13 = var12.getDbFieldName();
                if (needList != null && needList.contains(var13)) {
                    var12.setIsQuery(1);
                    var5.add(var12);
                } else {
                    if (isList) {
                        if (var12.getIsShowList() != 1) {
                            if (oConvertUtils.isNotEmpty(var12.getMainTable()) && oConvertUtils.isNotEmpty(var12.getMainField())) {
                                var5.add(var12);
                            }
                            continue;
                        }
                    } else if (var12.getIsShowForm() != 1) {
                        continue;
                    }

                    if (var10) {
                        if (this.b(var13, var9)) {
                            var5.add(var12);
                        }
                    } else {
                        var5.add(var12);
                    }
                }
            }

            return var5;
        }
    }

    private boolean b(String var1, List<String> var2) {
        boolean var3 = true;

        for(int var4 = 0; var4 < var2.size(); ++var4) {
            String var5 = (String)var2.get(var4);
            if (!oConvertUtils.isEmpty(var5)) {
                String var6 = var5.substring(var5.lastIndexOf(":") + 1);
                if (!oConvertUtils.isEmpty(var6) && var6.equals(var1)) {
                    var3 = false;
                }
            }
        }

        return var3;
    }

    public boolean a(String var1, List<OnlCgformField> var2) {
        boolean var3 = false;
        Iterator var4 = var2.iterator();

        while(var4.hasNext()) {
            OnlCgformField var5 = (OnlCgformField)var4.next();
            if (oConvertUtils.camelToUnderline(var1).equals(var5.getDbFieldName())) {
                var3 = true;
                break;
            }
        }

        return var3;
    }

    public void executeInsertSQL(Map<String, Object> params) {
        ((OnlCgformFieldMapper)this.baseMapper).executeInsertSQL(params);
    }

    public void executeUpdatetSQL(Map<String, Object> params) {
        ((OnlCgformFieldMapper)this.baseMapper).executeUpdatetSQL(params);
    }

    public List<TreeModel> queryDataListByLinkDown(CommonEntity linkDown) {
        return ((OnlCgformFieldMapper)this.baseMapper).queryDataListByLinkDown(linkDown);
    }

    public void updateTreeNodeNoChild(String tableName, String filed, String id) {
        Map var4 = org.jeecg.modules.online.cgform.util.SqlSymbolUtil.a(tableName, filed, id);
        ((OnlCgformFieldMapper)this.baseMapper).executeUpdatetSQL(var4);
    }

    public String queryTreeChildIds(OnlCgformHead head, String ids) {
        String var3 = head.getTreeParentIdField();
        String var4 = head.getTableName();
        String[] var5 = ids.split(",");
        StringBuffer var6 = new StringBuffer();
        String[] var7 = var5;
        int var8 = var5.length;

        for(int var9 = 0; var9 < var8; ++var9) {
            String var10 = var7[var9];
            if (var10 != null && !var6.toString().contains(var10)) {
                if (var6.toString().length() > 0) {
                    var6.append(",");
                }

                var6.append(var10);
                this.a(var10, var3, var4, var6);
            }
        }

        return var6.toString();
    }

    private StringBuffer a(String var1, String var2, String var3, StringBuffer var4) {
        String var5 = "select * from " + SqlSymbolUtil.getSubstring(var3) + " where " + var2 + "= '" + var1 + "'";
        List var6 = this.onlCgformFieldMapper.queryListBySql(var5);
        Map var8;
        if (var6 != null && var6.size() > 0) {
            for(Iterator var7 = var6.iterator(); var7.hasNext(); this.a(var8.get("id").toString(), var2, var3, var4)) {
                var8 = (Map)var7.next();
                if (!var4.toString().contains(var8.get("id").toString())) {
                    var4.append(",").append(var8.get("id"));
                }
            }
        }

        return var4;
    }

}
