package org.jeecg.modules.online.cgreport.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgreport.entity.OnlCgreportItem;
import org.jeecg.modules.online.cgreport.mapper.OnlCgreportItemMapper;
import org.jeecg.modules.online.cgreport.service.IOnlCgreportItemService;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * TODO
 *
 * @author dousw
 * @version 1.0
 * @date 2020/11/11 14:58
 */
@Service("onlCgreportItemServiceImpl")
public class OnlCgreportItemServiceImpl extends ServiceImpl<OnlCgreportItemMapper, OnlCgreportItem> implements IOnlCgreportItemService {

    public List<Map<String, String>> getAutoListQueryInfo(String cgrheadId) {
        LambdaQueryWrapper<OnlCgreportItem> var2 = new LambdaQueryWrapper<>();
        var2.eq(OnlCgreportItem::getCgrheadId, cgrheadId);
        var2.eq(OnlCgreportItem::getIsSearch, 1);
        List<OnlCgreportItem> var3 = this.list(var2);
        List<Map<String, String>> var4 = new ArrayList<>();
        int var5 = 0;

        Map<String,String> var8;
        for(Iterator<OnlCgreportItem> var6 = var3.iterator(); var6.hasNext(); var4.add(var8)) {
            OnlCgreportItem var7 = (OnlCgreportItem)var6.next();
            var8 = new HashMap<>();
            var8.put("label", var7.getFieldTxt());
            if (oConvertUtils.isNotEmpty(var7.getDictCode())) {
                var8.put("view", "list");
            } else {
                var8.put("view", var7.getFieldType().toLowerCase());
            }

            var8.put("mode", oConvertUtils.isEmpty(var7.getSearchMode()) ? "single" : var7.getSearchMode());
            var8.put("field", var7.getFieldName());
            ++var5;
            if (var5 > 2) {
                var8.put("hidden", "1");
            }
        }

        return var4;
    }
}
