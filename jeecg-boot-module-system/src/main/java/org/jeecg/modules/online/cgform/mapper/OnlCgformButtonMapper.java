package org.jeecg.modules.online.cgform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.online.cgform.entity.OnlCgformButton;

public interface OnlCgformButtonMapper extends BaseMapper<OnlCgformButton> {
}
