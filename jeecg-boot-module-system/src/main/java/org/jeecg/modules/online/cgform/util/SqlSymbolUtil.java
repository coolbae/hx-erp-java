package org.jeecg.modules.online.cgform.util;

import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.autopoi.poi.excel.entity.params.ExcelExportEntity;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.query.MatchTypeEnum;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.query.QueryRuleEnum;
import org.jeecg.common.system.vo.DictModel;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.system.vo.SysPermissionDataRuleModel;
import org.jeecg.common.util.CommonUtils;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.common.util.UUIDGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.common.util.jsonschema.BaseColumn;
import org.jeecg.common.util.jsonschema.CommonProperty;
import org.jeecg.common.util.jsonschema.JsonSchemaDescrip;
import org.jeecg.common.util.jsonschema.JsonschemaUtil;
import org.jeecg.common.util.jsonschema.validate.DictProperty;
import org.jeecg.common.util.jsonschema.validate.HiddenProperty;
import org.jeecg.common.util.jsonschema.validate.LinkDownProperty;
import org.jeecg.common.util.jsonschema.validate.NumberProperty;
import org.jeecg.common.util.jsonschema.validate.PopupProperty;
import org.jeecg.common.util.jsonschema.validate.StringProperty;
import org.jeecg.common.util.jsonschema.validate.SwitchProperty;
import org.jeecg.common.util.jsonschema.validate.TreeSelectProperty;
import org.jeecg.modules.online.cgform.entity.OnlCgformButton;
import org.jeecg.modules.online.cgform.entity.OnlCgformEnhanceJava;
import org.jeecg.modules.online.cgform.entity.OnlCgformEnhanceJs;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;
import org.jeecg.modules.online.cgform.entity.OnlCgformHead;
import org.jeecg.modules.online.cgform.entity.OnlCgformIndex;
import org.jeecg.modules.online.cgform.enums.CgformValidPatternEnum;
import org.jeecg.modules.online.cgform.mapper.OnlCgformHeadMapper;
import org.jeecg.modules.online.cgform.model.FieldModel;
import org.jeecg.modules.online.cgform.service.IOnlCgformFieldService;
import org.jeecg.modules.online.config.exception.DBException;
import org.jeecg.modules.online.config.util.DbType;
import org.jeecg.modules.online.config.util.TableUtil;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SqlSymbolUtil {
	// private static String au;

	public static void getSelect(String tableName, List<OnlCgformField> onlCgformFieldList, StringBuffer sbf) {
		if (onlCgformFieldList != null && onlCgformFieldList.size() != 0) {
			sbf.append("SELECT ");
			int size = onlCgformFieldList.size();
			boolean b = false;

			for (int i = 0; i < size; ++i) {
				OnlCgformField onlCgformField = onlCgformFieldList.get(i);
				if ("id".equals(onlCgformField.getDbFieldName())) {
					b = true;
				}

				if ("cat_tree".equals(onlCgformField.getFieldShowType())
						&& oConvertUtils.isNotEmpty(onlCgformField.getDictText())) {
					sbf.append(onlCgformField.getDictText() + ",");
				}

				if (i == size - 1) {
					sbf.append(onlCgformField.getDbFieldName() + " ");
				} else {
					sbf.append(onlCgformField.getDbFieldName() + ",");
				}
			}

			if (!b) {
				sbf.append(",id");
			}
		} else {
			sbf.append("SELECT id");
		}

		sbf.append(" FROM " + getSubstring(tableName));
	}

	public static String toDateyMdHms(String s) {
		return " to_date('" + s + "','yyyy-MM-dd HH24:mi:ss')";
	}

	public static String toDateyMd(String s) {
		return " to_date('" + s + "','yyyy-MM-dd')";
	}

	public static boolean isListType(String fieldShowType) {
		if ("list".equals(fieldShowType)) {
			return true;
		} else if ("radio".equals(fieldShowType)) {
			return true;
		} else if ("checkbox".equals(fieldShowType)) {
			return true;
		} else {
			return "list_multi".equals(fieldShowType);
		}
	}

	public static String getByDataType(List<OnlCgformField> onlCgformField, Map<String, Object> params,
			List<String> needList) {
		StringBuffer sbf = new StringBuffer();
		String databaseType = "";

		try {
			databaseType = org.jeecg.modules.online.config.util.TableUtil.getDatabaseType();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (DBException e) {
			e.printStackTrace();
		}

		Map<String, SysPermissionDataRuleModel> ruleMap = QueryGenerator.getRuleMap();
		Iterator<String> iterator1 = ruleMap.keySet().iterator();

		while (iterator1.hasNext()) {
			String rule = (String) iterator1.next();
			if (oConvertUtils.isNotEmpty(rule) && rule.startsWith("SQL_RULES_COLUMN")) {
				sbf.append(" AND (" + QueryGenerator.getSqlRuleValue((ruleMap.get(rule)).getRuleValue()) + ")");
			}
		}

		Iterator<OnlCgformField> iterator2 = onlCgformField.iterator();

		while (true) {
			while (true) {
				String dbFieldName;
				String dbType;
				Object queryValue;
				do {
					while (true) {
						OnlCgformField onlCgF;
						do {
							if (!iterator2.hasNext()) {
								return sbf.toString();
							}

							onlCgF = (OnlCgformField) iterator2.next();
							dbFieldName = onlCgF.getDbFieldName();
							dbType = onlCgF.getDbType();
							if (ruleMap.containsKey(dbFieldName)) {
								rule(databaseType, ruleMap.get(dbFieldName), dbFieldName, dbType, sbf);
							}

							if (ruleMap.containsKey(oConvertUtils.camelNames(dbFieldName))) {
								rule(databaseType, ruleMap.get(dbFieldName), dbFieldName, dbType, sbf);
							}

							if (needList != null && needList.contains(dbFieldName)) {
								onlCgF.setIsQuery(1);
								onlCgF.setQueryMode("single");
							}

							if (oConvertUtils.isNotEmpty(onlCgF.getMainField())
									&& oConvertUtils.isNotEmpty(onlCgF.getMainTable())) {
								onlCgF.setIsQuery(1);
								onlCgF.setQueryMode("single");
							}
						} while (1 != onlCgF.getIsQuery());

						if ("single".equals(onlCgF.getQueryMode())) {
							queryValue = params.get(dbFieldName);
							break;
						}

						queryValue = params.get(dbFieldName + "_begin");
						if (queryValue != null) {
							sbf.append(" AND " + dbFieldName + ">=");
							if (DataTypeUtil.isNumberType(dbType)) {
								sbf.append(queryValue.toString());
							} else if ("ORACLE".equals(databaseType) && dbType.toLowerCase().indexOf("date") >= 0) {
								sbf.append(toDateyMdHms(queryValue.toString()));
							} else {
								sbf.append("'" + queryValue.toString() + "'");
							}
						}

						Object object = params.get(dbFieldName + "_end");
						if (object != null) {
							sbf.append(" AND " + dbFieldName + "<=");
							if (DataTypeUtil.isNumberType(dbType)) {
								sbf.append(object.toString());
							} else if ("ORACLE".equals(databaseType) && dbType.toLowerCase().indexOf("date") >= 0) {
								sbf.append(toDateyMdHms(object.toString()));
							} else {
								sbf.append("'" + object.toString() + "'");
							}
						}
					}
				} while (queryValue == null);

				if ("ORACLE".equals(databaseType) && dbType.toLowerCase().indexOf("date") >= 0) {
					sbf.append(" AND " + dbFieldName + "=" + toDateyMdHms(queryValue.toString()));
				} else {
					boolean b = !DataTypeUtil.isNumberType(dbType);
					String singleQueryConditionSql = QueryGenerator.getSingleQueryConditionSql(dbFieldName, "",
							queryValue, b);
					sbf.append(" AND " + singleQueryConditionSql);
				}
			}
		}
	}

	public static String getByParams(Map<String, Object> params) {
		Object object = params.get("superQueryParams");
		if (object != null && !StringUtils.isBlank(object.toString())) {
			IOnlCgformFieldService onlCgformFieldService = SpringContextUtils.getBean(IOnlCgformFieldService.class);
			String s = null;

			try {
				s = URLDecoder.decode(object.toString(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				return "";
			}

			JSONArray jsonArray = JSONArray.parseArray(s);
			Object object1 = params.get("superQueryMatchType");
			MatchTypeEnum matchTypeEnum = MatchTypeEnum.getByValue(object1);
			if (matchTypeEnum == null) {
				matchTypeEnum = MatchTypeEnum.AND;
			}

			Map<String, JSONObject> hashMap = new HashMap<>();
			StringBuilder stringBuilder = (new StringBuilder(" AND ")).append("(");

			for (int i = 0; i < jsonArray.size(); ++i) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				String field = jsonObject.getString("field");
				String[] fields = field.split(",");
				if (fields.length == 1) {
					append(stringBuilder, field, jsonObject, matchTypeEnum, (JSONObject) null, i == 0);
				} else if (fields.length == 2) {
					String field1 = fields[0];
					String field2 = fields[1];
					JSONObject jsonObject1 = hashMap.get(field1);
					if (jsonObject1 == null) {
						List<OnlCgformField> cgformFields = onlCgformFieldService.queryFormFieldsByTableName(field1);
						jsonObject1 = new JSONObject(3);
						Iterator<OnlCgformField> iterator = cgformFields.iterator();

						while (iterator.hasNext()) {
							OnlCgformField onlCgformField = iterator.next();
							if (StringUtils.isNotBlank(onlCgformField.getMainTable())) {
								jsonObject1.put("subTableName", field1);
								jsonObject1.put("subField", onlCgformField.getDbFieldName());
								jsonObject1.put("mainTable", onlCgformField.getMainTable());
								jsonObject1.put("mainField", onlCgformField.getMainField());
							}
						}

						hashMap.put(field1, jsonObject1);
					}

					append(stringBuilder, field2, jsonObject, matchTypeEnum, jsonObject1, i == 0);
				}
			}

			return stringBuilder.append(")").toString();
		} else {
			return "";
		}
	}

	private static void append(StringBuilder stringBuilder, String s, JSONObject jsonObject,
			MatchTypeEnum matchTypeEnum, JSONObject jsonObject1, boolean b) {
		if (!b) {
			stringBuilder.append(" ").append(matchTypeEnum.getValue()).append(" ");
		}

		String type = jsonObject.getString("type");
		String val = jsonObject.getString("val");
		String sql = appendSpace(type, val);
		QueryRuleEnum queryRuleEnum = QueryRuleEnum.getByValue(jsonObject.getString("rule"));
		if (queryRuleEnum == null) {
			queryRuleEnum = QueryRuleEnum.EQ;
		}

		if (jsonObject1 != null) {
			String subTableName = jsonObject1.getString("subTableName");
			String subField = jsonObject1.getString("subField");
			// String mainTable = jsonObject1.getString("mainTable");
			String mainField = jsonObject1.getString("mainField");
			stringBuilder.append("(").append(mainField).append(" IN (SELECT ").append(subField).append(" FROM ")
					.append(subTableName).append(" WHERE ").append(s);
			appendSymbol(stringBuilder, queryRuleEnum, val, sql, type);
			stringBuilder.append("))");
		} else {
			stringBuilder.append(s);
			appendSymbol(stringBuilder, queryRuleEnum, val, sql, type);
		}

	}

	private static void appendSymbol(StringBuilder stringBuilder, QueryRuleEnum queryRuleEnum, String value, String sql,
			String dataType) {
		if ("date".equals(dataType) && "ORACLE".equalsIgnoreCase(CommonUtils.getDatabaseType())) {
			sql = sql.replace("'", "");
			if (sql.length() == 10) {
				sql = toDateyMd(sql);
			} else {
				sql = toDateyMdHms(sql);
			}
		}

		switch (queryRuleEnum) {
		case GT:
			stringBuilder.append(">").append(sql);
			break;
		case GE:
			stringBuilder.append(">=").append(sql);
			break;
		case LT:
			stringBuilder.append("<").append(sql);
			break;
		case LE:
			stringBuilder.append("<=").append(sql);
			break;
		case NE:
			stringBuilder.append("!=").append(sql);
			break;
		case IN:
			stringBuilder.append(" IN (");
			String[] values = value.split(",");

			for (int i = 0; i < values.length; ++i) {
				String value1 = values[i];
				if (StringUtils.isNotBlank(value1)) {
					String s = appendSpace(dataType, value1);
					stringBuilder.append(s);
					if (i < values.length - 1) {
						stringBuilder.append(",");
					}
				}
			}

			stringBuilder.append(")");
			break;
		case LIKE:
			stringBuilder.append(" like ").append("N").append("'").append("%").append(value).append("%").append("'");
			break;
		case LEFT_LIKE:
			stringBuilder.append(" like ").append("N").append("'").append("%").append(value).append("'");
			break;
		case RIGHT_LIKE:
			stringBuilder.append(" like ").append("N").append("'").append(value).append("%").append("'");
			break;
		case EQ:
		default:
			stringBuilder.append("=").append(sql);
		}

	}

	private static String appendSpace(String dataType, String value) {
		if (!"int".equals(dataType) && !"number".equals(dataType)) {
			if ("date".equals(dataType)) {
				return "'" + value + "'";
			} else {
				return "SQLSERVER".equals(CommonUtils.getDatabaseType()) ? "N'" + value + "'" : "'" + value + "'";
			}
		} else {
			return value;
		}
	}

	public static Map<String, Object> getParameterMap(HttpServletRequest request) {
		Map<String, String[]> map = request.getParameterMap();
		Map<String, Object> hashMap = new HashMap<>();
		Iterator<Entry<String, String[]>> entryIterator = map.entrySet().iterator();
		String key = "";
		String value = "";

		for (Object object = null; entryIterator.hasNext(); hashMap.put(key, value)) {
			Entry<String, String[]> entry = entryIterator.next();
			key = (String) entry.getKey();
			object = entry.getValue();
			if (!"_t".equals(key) && null != object) {
				if (!(object instanceof String[])) {
					value = object.toString();
				} else {
					String[] var8 = (String[]) (object);

					for (int var9 = 0; var9 < var8.length; ++var9) {
						value = var8[var9] + ",";
					}

					value = value.substring(0, value.length() - 1);
				}
			} else {
				value = "";
			}
		}

		return hashMap;
	}

	public static boolean isExistField(String onlCgformField, List<OnlCgformField> onlCgformFieldList) {
		Iterator<OnlCgformField> iterator = onlCgformFieldList.iterator();

		OnlCgformField onlCgformField1;
		do {
			if (!iterator.hasNext()) {
				return false;
			}

			onlCgformField1 = iterator.next();
		} while (!onlCgformField.equals(onlCgformField1.getDbFieldName()));

		return true;
	}

	public static JSONObject getFiledJson(List<OnlCgformField> onlCgformFieldList, List<String> stringList,
			FieldModel fieldModel) {
		List<String> notNullFieldList = new ArrayList<>();
		List<CommonProperty> arrayList1 = new ArrayList<>();
		ISysBaseAPI sysBaseAPI = SpringContextUtils.getBean(ISysBaseAPI.class);
		OnlCgformHeadMapper onlCgformHeadMapper = SpringContextUtils.getBean(OnlCgformHeadMapper.class);
		List<String> arrayList2 = new ArrayList<>();
		Iterator<OnlCgformField> iterator = onlCgformFieldList.iterator();

		while (true) {
			OnlCgformField onlCgformField;
			String fieldName;
			do {
				do {
					if (!iterator.hasNext()) {
						JSONObject jsonObject;
						JsonSchemaDescrip jsonSchemaDescrip;
						if (notNullFieldList.size() > 0) {
							jsonSchemaDescrip = new JsonSchemaDescrip(notNullFieldList);
							jsonObject = JsonschemaUtil.getJsonSchema(jsonSchemaDescrip, arrayList1);
						} else {
							jsonSchemaDescrip = new JsonSchemaDescrip();
							jsonObject = JsonschemaUtil.getJsonSchema(jsonSchemaDescrip, arrayList1);
						}

						return jsonObject;
					}

					onlCgformField = iterator.next();
					fieldName = onlCgformField.getDbFieldName();
				} while ("id".equals(fieldName));
			} while (arrayList2.contains(fieldName));

			String dbFieldTxt = onlCgformField.getDbFieldTxt();
			if ("1".equals(onlCgformField.getFieldMustInput())) {
				notNullFieldList.add(fieldName);
			}

			String fieldShowType = onlCgformField.getFieldShowType();
			CommonProperty property = null;
			if ("switch".equals(fieldShowType)) {
				property = new SwitchProperty(fieldName, dbFieldTxt, onlCgformField.getFieldExtendJson());
			} else if (isListType(fieldShowType)) {
				List<DictModel> arrayList3 = new ArrayList<>();
				if (oConvertUtils.isNotEmpty(onlCgformField.getDictTable())) {
					arrayList3 = sysBaseAPI.queryTableDictItemsByCode(onlCgformField.getDictTable(),
							onlCgformField.getDictText(), onlCgformField.getDictField());
				} else if (oConvertUtils.isNotEmpty(onlCgformField.getDictField())) {
					arrayList3 = sysBaseAPI.queryDictItemsByCode(onlCgformField.getDictField());
				}

				property = new StringProperty(fieldName, dbFieldTxt, fieldShowType, onlCgformField.getDbLength(),
						arrayList3);
				if (DataTypeUtil.isNumberType(onlCgformField.getDbType())) {
					property.setType("number");
				}
			} else if (DataTypeUtil.isNumberType(onlCgformField.getDbType())) {
				NumberProperty numberProperty = new NumberProperty(fieldName, dbFieldTxt, "number");
				/*
				 * if (CgformValidPatternEnum.INTEGER.getType().equals(
				 * onlCgformField.getFieldValidType())) {
				 * numberProperty.setPattern(CgformValidPatternEnum.INTEGER.
				 * getPattern()); } else {
				 * numberProperty.setPattern(onlCgformField.getFieldValidType())
				 * ; numberProperty.setErrorInfo("输入的值不合法"); }
				 */

				if (oConvertUtils.isNotEmpty(onlCgformField.getFieldValidType())) {
					CgformValidPatternEnum cgformValidPatternEnum = CgformValidPatternEnum
							.getPatternInfoByType(onlCgformField.getFieldValidType());
					if (cgformValidPatternEnum != null) {
						if (CgformValidPatternEnum.NOTNULL == cgformValidPatternEnum) {
							notNullFieldList.add(fieldName);
						} else {
							numberProperty.setPattern(cgformValidPatternEnum.getPattern());
							numberProperty.setErrorInfo(cgformValidPatternEnum.getMsg());
						}
					} else {
						numberProperty.setPattern(onlCgformField.getFieldValidType());
						numberProperty.setErrorInfo("输入的值不合法");
					}
				}

				property = numberProperty;
			} else {
				String dictField;
				if (!"popup".equals(fieldShowType)) {
					if ("sel_search".equals(fieldShowType)) {
						property = new DictProperty(fieldName, dbFieldTxt, onlCgformField.getDictTable(),
								onlCgformField.getDictField(), onlCgformField.getDictText());
					} else if ("link_down".equals(fieldShowType)) {
						LinkDownProperty linkDownProperty = new LinkDownProperty(fieldName, dbFieldTxt,
								onlCgformField.getDictTable());
						a(linkDownProperty, onlCgformFieldList, arrayList2);
						property = linkDownProperty;
					} else {
						String dictText;
						String dictTable;
						if ("sel_tree".equals(fieldShowType)) {
							dictText = onlCgformField.getDictText();
							String[] dictTexts = dictText.split(",");
							dictTable = onlCgformField.getDictTable() + "," + dictTexts[2] + "," + dictTexts[0];
							TreeSelectProperty treeSelectProperty = new TreeSelectProperty(fieldName, dbFieldTxt,
									dictTable, dictTexts[1], onlCgformField.getDictField());
							if (dictTexts.length > 3) {
								treeSelectProperty.setHasChildField(dictTexts[3]);
							}

							property = treeSelectProperty;
						} else if ("cat_tree".equals(fieldShowType)) {
							dictText = onlCgformField.getDictText();
							dictField = onlCgformField.getDictField();
							dictTable = "0";
							if (oConvertUtils.isNotEmpty(dictField) && !"0".equals(dictField)) {
								dictTable = onlCgformHeadMapper.queryCategoryIdByCode(dictField);
							}

							if (oConvertUtils.isEmpty(dictText)) {
								property = new TreeSelectProperty(fieldName, dbFieldTxt, dictTable);
							} else {
								property = new TreeSelectProperty(fieldName, dbFieldTxt, dictTable, dictText);
								HiddenProperty hiddenProperty = new HiddenProperty(dictText, dictText);
								arrayList1.add(hiddenProperty);
							}
						} else if (fieldModel != null && fieldName.equals(fieldModel.getFieldName())) {
							dictText = fieldModel.getTableName() + "," + fieldModel.getTextField() + ","
									+ fieldModel.getCodeField();
							TreeSelectProperty treeSelectProperty = new TreeSelectProperty(fieldName, dbFieldTxt,
									dictText, fieldModel.getPidField(), fieldModel.getPidValue());
							treeSelectProperty.setHasChildField(fieldModel.getHsaChildField());
							treeSelectProperty.setPidComponent(1);
							property = treeSelectProperty;
						} else {
							StringProperty stringProperty = new StringProperty(fieldName, dbFieldTxt, fieldShowType,
									onlCgformField.getDbLength());
							if (oConvertUtils.isNotEmpty(onlCgformField.getFieldValidType())) {
								CgformValidPatternEnum cgformValidPatternEnum = CgformValidPatternEnum
										.getPatternInfoByType(onlCgformField.getFieldValidType());
								if (cgformValidPatternEnum != null) {
									if (CgformValidPatternEnum.NOTNULL == cgformValidPatternEnum) {
										notNullFieldList.add(fieldName);
									} else {
										stringProperty.setPattern(cgformValidPatternEnum.getPattern());
										stringProperty.setErrorInfo(cgformValidPatternEnum.getMsg());
									}
								} else {
									stringProperty.setPattern(onlCgformField.getFieldValidType());
									stringProperty.setErrorInfo("输入的值不合法");
								}
							}

							property = stringProperty;
						}
					}
				} else {
					PopupProperty popupProperty = new PopupProperty(fieldName, dbFieldTxt,
							onlCgformField.getDictTable(), onlCgformField.getDictText(), onlCgformField.getDictField(),
							onlCgformField.getFieldExtendJson());
					dictField = onlCgformField.getDictText();
					if (dictField != null && !dictField.equals("")) {
						String[] dictFields = dictField.split(",");
						for (String dictField1 : dictFields) {
							if (!isExistField(dictField1, onlCgformFieldList)) {
								HiddenProperty hiddenProperty = new HiddenProperty(dictField1, dictField1);
								hiddenProperty.setOrder(onlCgformField.getOrderNum());
								arrayList1.add(hiddenProperty);
							}
						}
					}

					property = popupProperty;
				}
			}

			if (onlCgformField.getIsReadOnly() == 1 || (stringList != null && stringList.indexOf(fieldName) >= 0)) {
				property.setDisabled(true);
			}

			property.setOrder(onlCgformField.getOrderNum());
			property.setDefVal(onlCgformField.getFieldDefaultValue());
			arrayList1.add(property);
		}
	}

	public static Set<String> getByShowType(List<OnlCgformField> onlCgformFields) {
		Set<String> hashSet = new HashSet<>();
		Iterator<OnlCgformField> iterator = onlCgformFields.iterator();

		OnlCgformField onlCgformField;
		String s;
		while (iterator.hasNext()) {
			onlCgformField = iterator.next();
			if ("popup".equals(onlCgformField.getFieldShowType())) {
				s = onlCgformField.getDictText();
				if (s != null && !s.equals("")) {
					hashSet.addAll(Arrays.stream(s.split(",")).collect(Collectors.toSet()));
				}
			}

			if ("cat_tree".equals(onlCgformField.getFieldShowType())) {
				s = onlCgformField.getDictText();
				if (oConvertUtils.isNotEmpty(s)) {
					hashSet.add(s);
				}
			}
		}

		iterator = onlCgformFields.iterator();

		while (iterator.hasNext()) {
			onlCgformField = iterator.next();
			s = onlCgformField.getDbFieldName();
			if (onlCgformField.getIsShowForm() == 1 && hashSet.contains(s)) {
				hashSet.remove(s);
			}
		}

		return hashSet;
	}

	public static Map<String, Object> getInsertSql(String tbname, List<OnlCgformField> fieldList,
			JSONObject jsonObject) {
		StringBuffer stringBuilder = new StringBuffer();
		StringBuffer stringBuilder1 = new StringBuffer();
		String s = "";

		try {
			s = TableUtil.getDatabaseType();
		} catch (SQLException | DBException e) {
			e.printStackTrace();
		}

		Map<String, Object> hashMap = new HashMap<>();
		boolean b = false;
		String s1 = null;
		LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		if (user == null) {
			throw new JeecgBootException("online保存表单数据异常:系统未找到当前登陆用户信息");
		} else {
			Set<String> set = getByShowType(fieldList);
			Iterator<OnlCgformField> iterator = fieldList.iterator();

			while (true) {
				while (iterator.hasNext()) {
					OnlCgformField onlCgformField = iterator.next();
					String dbFieldName = onlCgformField.getDbFieldName();
					if (null == dbFieldName) {
						log.info("--------online保存表单数据遇见空名称的字段------->>" + onlCgformField.getId());
					} else if ("id".equals(dbFieldName.toLowerCase())) {
						b = true;
						s1 = jsonObject.getString(dbFieldName);
					} else {
						appendUserData(onlCgformField, user, jsonObject, "CREATE_BY", "CREATE_TIME", "SYS_ORG_CODE");
						if ("bpm_status".equals(dbFieldName.toLowerCase())) {
							stringBuilder.append("," + dbFieldName);
							stringBuilder1.append(",'0'");
						} else {
							String s2;
							if (set.contains(dbFieldName)) {
								stringBuilder.append("," + dbFieldName);
								s2 = DataTypeUtil.getSql(s, onlCgformField, jsonObject, hashMap);
								stringBuilder1.append("," + s2);
							} else if (onlCgformField.getIsShowForm() == 1
									|| !oConvertUtils.isEmpty(onlCgformField.getMainField())
									|| !oConvertUtils.isEmpty(onlCgformField.getDbDefaultVal())) {
								if (jsonObject.get(dbFieldName) == null) {
									if (oConvertUtils.isEmpty(onlCgformField.getDbDefaultVal())) {
										continue;
									}

									jsonObject.put(dbFieldName, onlCgformField.getDbDefaultVal());
								}

								if ("".equals(jsonObject.get(dbFieldName))) {
									s2 = onlCgformField.getDbType();
									if (DataTypeUtil.isNumberType(s2) || DataTypeUtil.isDateType(s2)) {
										continue;
									}
								}

								stringBuilder.append("," + dbFieldName);
								s2 = DataTypeUtil.getSql(s, onlCgformField, jsonObject, hashMap);
								stringBuilder1.append("," + s2);
							}
						}
					}
				}

				if (b) {
					if (oConvertUtils.isEmpty(s1)) {
						s1 = getIdWorkerId();
					}
				} else {
					s1 = getIdWorkerId();
				}

				String sql = "insert into " + getSubstring(tbname) + "(" + "id" + stringBuilder.toString() + ") values("
						+ "'" + s1 + "'" + stringBuilder1.toString() + ")";
				hashMap.put("execute_sql_string", sql);
				log.info("--动态表单保存sql-->" + sql);
				return hashMap;
			}
		}
	}

	public static Map<String, Object> getUpdateSql(String tableName, List<OnlCgformField> onlCgformFields,
			JSONObject jsonObject) {
		StringBuffer stringBuilder = new StringBuffer();
		Map<String, Object> hashMap = new HashMap<>();
		String databaseType = "";

		try {
			databaseType = TableUtil.getDatabaseType();
		} catch (SQLException | DBException e) {
			e.printStackTrace();
		}

		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		if (sysUser == null) {
			throw new JeecgBootException("online修改表单数据异常:系统未找到当前登陆用户信息");
		} else {
			Set<String> showType = getByShowType(onlCgformFields);
			Iterator<OnlCgformField> iterator = onlCgformFields.iterator();

			while (iterator.hasNext()) {
				OnlCgformField onlCgformField = iterator.next();
				String dbFieldName = onlCgformField.getDbFieldName();
				if (null == dbFieldName) {
					log.info("--------online修改表单数据遇见空名称的字段------->>" + onlCgformField.getId());
				} else {
					appendUserData(onlCgformField, sysUser, jsonObject, "UPDATE_BY", "UPDATE_TIME", "SYS_ORG_CODE");
					String s2;
					if (showType.contains(dbFieldName) && jsonObject.get(dbFieldName) != null
							&& !"".equals(jsonObject.getString(dbFieldName))) {
						s2 = DataTypeUtil.getSql(databaseType, onlCgformField, jsonObject, hashMap);
						stringBuilder.append(dbFieldName + "=" + s2 + ",");
					} else if (onlCgformField.getIsShowForm() == 1 && !"id".equals(dbFieldName)) {
						if ("".equals(jsonObject.get(dbFieldName))) {
							s2 = onlCgformField.getDbType();
							if (DataTypeUtil.isNumberType(s2) || DataTypeUtil.isDateType(s2)) {
								continue;
							}
						}

						if (!oConvertUtils.isNotEmpty(onlCgformField.getMainTable())
								|| !oConvertUtils.isNotEmpty(onlCgformField.getMainField())) {
							s2 = DataTypeUtil.getSql(databaseType, onlCgformField, jsonObject, hashMap);
							stringBuilder.append(dbFieldName + "=" + s2 + ",");
						}
					}
				}
			}

			String s3 = stringBuilder.toString();
			if (s3.endsWith(",")) {
				s3 = s3.substring(0, s3.length() - 1);
			}

			String sql = "update " + getSubstring(tableName) + " set " + s3 + " where 1=1  " + " AND " + "id" + "="
					+ "'" + jsonObject.getString("id") + "'";
			log.info("--动态表单编辑sql-->" + sql);
			hashMap.put("execute_sql_string", sql);
			return hashMap;
		}
	}

	public static String getSelectSql(String tbname, List<OnlCgformField> onlCgformFields, String id) {
		return getSelectSql(tbname, onlCgformFields, "id", id);
	}

	public static String getSelectSql(String tbname, List<OnlCgformField> onlCgformFields, String field, String id) {
		StringBuffer stringBuilder = new StringBuffer();
		stringBuilder.append("SELECT ");
		int size = onlCgformFields.size();
		boolean b = false;

		for (int i = 0; i < size; ++i) {
			String dbFieldName = onlCgformFields.get(i).getDbFieldName();
			if ("id".equals(dbFieldName)) {
				b = true;
			}

			stringBuilder.append(dbFieldName);
			if (size > i + 1) {
				stringBuilder.append(",");
			}
		}

		if (!b) {
			stringBuilder.append(",id");
		}

		stringBuilder.append(" FROM " + getSubstring(tbname) + " where 1=1  " + " AND " + field + "=" + "'" + id + "'");
		return stringBuilder.toString();
	}

	public static void appendUserData(OnlCgformField onlCgformField, LoginUser loginUser, JSONObject jsonObject,
			String... strings) {
		String dbFieldName = onlCgformField.getDbFieldName();
		boolean b = false;
		int length = strings.length;

		for (int i = 0; i < length; ++i) {
			String s1 = strings[i];
			if (dbFieldName.toUpperCase().equals(s1)) {
				if (onlCgformField.getIsShowForm() == 1) {
					if (jsonObject.get(dbFieldName) == null) {
						b = true;
					}
				} else {
					onlCgformField.setIsShowForm(1);
					b = true;
				}

				if (b) {
					byte bt = -1;
					switch (s1) {
					case "CREATE_BY":
						bt = 0;
						break;
					case "SYS_ORG_CODE":
						bt = 4;
						break;
					case "UPDATE_BY":
						bt = 2;
						break;
					case "UPDATE_TIME":
						bt = 3;
						break;
					case "CREATE_TIME":
						bt = 1;
					}

					switch (bt) {
					case 0:
						jsonObject.put(dbFieldName, loginUser.getUsername());
						return;
					case 1:
						onlCgformField.setFieldShowType("datetime");
						jsonObject.put(dbFieldName, DateUtils.formatDateTime());
						return;
					case 2:
						jsonObject.put(dbFieldName, loginUser.getUsername());
						return;
					case 3:
						onlCgformField.setFieldShowType("datetime");
						jsonObject.put(dbFieldName, DateUtils.formatDateTime());
						return;
					case 4:
						jsonObject.put(dbFieldName, loginUser.getOrgCode());
					}
				}
				break;
			}
		}

	}

	public static boolean equals(Object object1, Object object2) {
		if (oConvertUtils.isEmpty(object1) && oConvertUtils.isEmpty(object2)) {
			return true;
		} else {
			return oConvertUtils.isNotEmpty(object1) && object1.equals(object2);
		}
	}

	public static boolean fieldEquals(OnlCgformField field, OnlCgformField newField) {
		return !equals(field.getDbFieldName(), newField.getDbFieldName())
				|| !equals(field.getDbFieldTxt(), newField.getDbFieldTxt())
				|| !equals(field.getDbLength(), newField.getDbLength())
				|| !equals(field.getDbPointLength(), newField.getDbPointLength())
				|| !equals(field.getDbType(), newField.getDbType())
				|| !equals(field.getDbIsNull(), newField.getDbIsNull())
				|| !equals(field.getDbIsKey(), newField.getDbIsKey())
				|| !equals(field.getDbDefaultVal(), newField.getDbDefaultVal());
	}

	public static boolean indexEquals(OnlCgformIndex index, OnlCgformIndex newIndex) {
		return !equals(index.getIndexName(), newIndex.getIndexName())
				|| !equals(index.getIndexField(), newIndex.getIndexField())
				|| !equals(index.getIndexType(), newIndex.getIndexType());
	}

	public static boolean onlCgformHeadEquals(OnlCgformHead onlCgformHead, OnlCgformHead newOnlCgformHead) {
		return !equals(onlCgformHead.getTableName(), newOnlCgformHead.getTableName())
				|| !equals(onlCgformHead.getTableTxt(), newOnlCgformHead.getTableTxt());
	}

	public static String a(String tableName, List<OnlCgformField> onlCgformFields, Map<String, Object> map) {
		StringBuffer stringBuilder = new StringBuffer();
		StringBuffer stringBuilder1 = new StringBuffer();

		for (OnlCgformField onlCgformField : onlCgformFields) {
			String dbFieldName = onlCgformField.getDbFieldName();
			String dbType = onlCgformField.getDbType();
			if (onlCgformField.getIsShowList() == 1) {
				stringBuilder1.append("," + dbFieldName);
			}

			boolean b;
			String s1;
			if (oConvertUtils.isNotEmpty(onlCgformField.getMainField())) {
				b = !DataTypeUtil.isNumberType(dbType);
				s1 = QueryGenerator.getSingleQueryConditionSql(dbFieldName, "", map.get(dbFieldName), b);
				if (!"".equals(s1)) {
					stringBuilder.append(" AND " + s1);
				}
			}

			if (onlCgformField.getIsQuery() == 1) {
				if ("single".equals(onlCgformField.getQueryMode())) {
					if (map.get(dbFieldName) != null) {
						b = !DataTypeUtil.isNumberType(dbType);
						s1 = QueryGenerator.getSingleQueryConditionSql(dbFieldName, "", map.get(dbFieldName), b);
						if (!"".equals(s1)) {
							stringBuilder.append(" AND " + s1);
						}
					}
				} else {
					Object object = map.get(dbFieldName + "_begin");
					if (object != null) {
						stringBuilder.append(" AND " + dbFieldName + ">=");
						if (DataTypeUtil.isNumberType(dbType)) {
							stringBuilder.append(object.toString());
						} else {
							stringBuilder.append("'" + object.toString() + "'");
						}
					}

					Object object1 = map.get(dbFieldName + "_end");
					if (object1 != null) {
						stringBuilder.append(" AND " + dbFieldName + "<=");
						if (DataTypeUtil.isNumberType(dbType)) {
							stringBuilder.append(object1.toString());
						} else {
							stringBuilder.append("'" + object1.toString() + "'");
						}
					}
				}
			}
		}

		return "SELECT id" + stringBuilder1.toString() + " FROM " + getSubstring(tableName) + " where 1=1  "
				+ stringBuilder.toString();
	}

	public static List<ExcelExportEntity> getExcelExportEntities(List<OnlCgformField> fields, String fieldName) {
		List<ExcelExportEntity> exportEntities = new ArrayList<>();

		for (int i = 0; i < fields.size(); ++i) {
			if ((null == fieldName || !fieldName.equals(fields.get(i).getDbFieldName()))
					&& fields.get(i).getIsShowList() == 1) {
				ExcelExportEntity exportEntity = new ExcelExportEntity((fields.get(i)).getDbFieldTxt(),
						fields.get(i).getDbFieldName());
				int length = fields.get(i).getDbLength() == 0 ? 12
						: (fields.get(i).getDbLength() > 30 ? 30 : fields.get(i).getDbLength());
				if (((OnlCgformField) fields.get(i)).getFieldShowType().equals("date")) {
					exportEntity.setFormat("yyyy-MM-dd");
				} else if (fields.get(i).getFieldShowType().equals("datetime")) {
					exportEntity.setFormat("yyyy-MM-dd HH:mm:ss");
				}

				if (length < 10) {
					length = 10;
				}

				exportEntity.setWidth((double) length);
				exportEntities.add(exportEntity);
			}
		}

		return exportEntities;
	}

	public static boolean isExistJava(OnlCgformEnhanceJava onlCgformEnhanceJava) {
		String javaType = onlCgformEnhanceJava.getCgJavaType();
		String javaValue = onlCgformEnhanceJava.getCgJavaValue();
		if (oConvertUtils.isNotEmpty(javaValue)) {
			try {
				if ("class".equals(javaType)) {
					Class clazz = Class.forName(javaValue);
					if (clazz == null || clazz.newInstance() == null) {
						return false;
					}
				}

				if ("spring".equals(javaType)) {
					Object bean = SpringContextUtils.getBean(javaValue);
					if (bean == null) {
						return false;
					}
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				return false;
			}
		}

		return true;
	}

	public static void sortByStr(List<String> list) {
		Collections.sort(list, new Comparator<String>() {
			public int compare(String str1, String str2) {
				if (str1 != null && str2 != null) {
					if (str1.compareTo(str2) > 0) {
						return 1;
					} else if (str1.compareTo(str2) < 0) {
						return -1;
					} else {
						return str1.compareTo(str2) == 0 ? 0 : 0;
					}
				} else {
					return -1;
				}
			}
		});
	}

	public static void sortByLength(List<String> list) {
		Collections.sort(list, new Comparator<String>() {
			public int compare(String str1, String str2) {
				if (str1 != null && str2 != null) {
					if (str1.length() > str2.length()) {
						return 1;
					} else if (str1.length() < str2.length()) {
						return -1;
					} else if (str1.compareTo(str2) > 0) {
						return 1;
					} else if (str1.compareTo(str2) < 0) {
						return -1;
					} else {
						return str1.compareTo(str2) == 0 ? 0 : 0;
					}
				} else {
					return -1;
				}
			}
		});
	}

	private static String a(String var0, boolean var1) {
		return var1 ? "'" + QueryGenerator.converRuleValue(var0) + "'" : QueryGenerator.converRuleValue(var0);
	}

	private static void rule(String var0, SysPermissionDataRuleModel var1, String var2, String var3,
			StringBuffer var4) {
		QueryRuleEnum var5 = QueryRuleEnum.getByValue(var1.getRuleConditions());
		boolean var6 = !DataTypeUtil.isNumberType(var3);
		String var7 = a(var1.getRuleValue(), var6);
		if (var7 != null && var5 != null) {
			if ("ORACLE".equalsIgnoreCase(var0) && "Date".equals(var3)) {
				var7 = var7.replace("'", "");
				if (var7.length() == 10) {
					var7 = toDateyMd(var7);
				} else {
					var7 = toDateyMdHms(var7);
				}
			}

			switch (var5) {
			case GT:
				var4.append(" AND " + var2 + ">" + var7);
				break;
			case GE:
				var4.append(" AND " + var2 + ">=" + var7);
				break;
			case LT:
				var4.append(" AND " + var2 + "<" + var7);
				break;
			case LE:
				var4.append(" AND " + var2 + "<=" + var7);
				break;
			case NE:
				var4.append(" AND " + var2 + " <> " + var7);
				break;
			case IN:
				var4.append(" AND " + var2 + " IN " + var7);
				break;
			case LIKE:
				var4.append(" AND " + var2 + " LIKE '%" + QueryGenerator.trimSingleQuote(var7) + "%'");
				break;
			case LEFT_LIKE:
				var4.append(" AND " + var2 + " LIKE '%" + QueryGenerator.trimSingleQuote(var7) + "'");
				break;
			case RIGHT_LIKE:
				var4.append(" AND " + var2 + " LIKE '" + QueryGenerator.trimSingleQuote(var7) + "%'");
				break;
			case EQ:
				var4.append(" AND " + var2 + "=" + var7);
				break;
			default:
				log.info("--查询规则未匹配到---");
			}

		}
	}

	public static String a(String var0, JSONObject var1) {
		if (var1 == null) {
			return var0;
		} else {
			var0 = var0.replace("#{UUID}", UUIDGenerator.generate());
			Set var2 = QueryGenerator.getSqlRuleParams(var0);
			Iterator var3 = var2.iterator();

			while (true) {
				while (var3.hasNext()) {
					String var4 = (String) var3.next();
					String var5;
					if (var1.get(var4.toUpperCase()) == null && var1.get(var4.toLowerCase()) == null) {
						var5 = QueryGenerator.converRuleValue(var4);
						var0 = var0.replace("#{" + var4 + "}", var5);
					} else {
						var5 = null;
						if (var1.containsKey(var4.toLowerCase())) {
							var5 = var1.getString(var4.toLowerCase());
						} else if (var1.containsKey(var4.toUpperCase())) {
							var5 = var1.getString(var4.toUpperCase());
						}

						var0 = var0.replace("#{" + var4 + "}", var5);
					}
				}

				return var0;
			}
		}
	}

	public static String c(String var0, List<OnlCgformButton> var1) {
		var0 = d(var0, var1);
		String[] var2 = "beforeAdd,beforeEdit,afterAdd,afterEdit,beforeDelete,afterDelete,mounted,created".split(",");
		int var3 = var2.length;

		for (int var4 = 0; var4 < var3; ++var4) {
			String var5 = var2[var4];
			Pattern var6;
			Matcher var7;
			if ("beforeAdd,afterAdd,mounted,created".indexOf(var5) >= 0) {
				var6 = Pattern.compile("(" + var5 + "\\s*\\(\\)\\s*\\{)");
				var7 = var6.matcher(var0);
				if (var7.find()) {
					var0 = var0.replace(var7.group(0), var5
							+ "(that){const getAction=this._getAction,postAction=this._postAction,deleteAction=this._deleteAction;");
				}
			} else {
				var6 = Pattern.compile("(" + var5 + "\\s*\\(row\\)\\s*\\{)");
				var7 = var6.matcher(var0);
				if (var7.find()) {
					var0 = var0.replace(var7.group(0), var5
							+ "(that,row){const getAction=this._getAction,postAction=this._postAction,deleteAction=this._deleteAction;");
				} else {
					Pattern var8 = Pattern.compile("(" + var5 + "\\s*\\(\\)\\s*\\{)");
					Matcher var9 = var8.matcher(var0);
					if (var9.find()) {
						var0 = var0.replace(var9.group(0), var5
								+ "(that){const getAction=this._getAction,postAction=this._postAction,deleteAction=this._deleteAction;");
					}
				}
			}
		}

		return d(var0);
	}

	public static void a(OnlCgformEnhanceJs var0, String var1, List<OnlCgformField> var2) {
		if (var0 != null && !oConvertUtils.isEmpty(var0.getCgJs())) {
			String var3 = var0.getCgJs();
			String var4 = "onlChange";
			Pattern var5 = Pattern.compile("(" + var1 + "_" + var4 + "\\s*\\(\\)\\s*\\{)");
			Matcher var6 = var5.matcher(var3);
			if (var6.find()) {
				var3 = var3.replace(var6.group(0), var1 + "_" + var4
						+ "(){const getAction=this._getAction,postAction=this._postAction,deleteAction=this._deleteAction;");
				Iterator var7 = var2.iterator();

				while (var7.hasNext()) {
					OnlCgformField var8 = (OnlCgformField) var7.next();
					Pattern var9 = Pattern.compile("(" + var8.getDbFieldName() + "\\s*\\(\\))");
					Matcher var10 = var9.matcher(var3);
					if (var10.find()) {
						var3 = var3.replace(var10.group(0), var8.getDbFieldName() + "(that,event)");
					}
				}
			}

			var0.setCgJs(var3);
		}
	}

	public static void a(OnlCgformEnhanceJs var0, String var1, List<OnlCgformField> var2, boolean var3) {
		if (var0 != null && !oConvertUtils.isEmpty(var0.getCgJs())) {
			String var4 = var0.getCgJs();
			String var5 = "onlChange";
			Pattern var6 = Pattern.compile("([^_]" + var5 + "\\s*\\(\\)\\s*\\{)");
			Matcher var7 = var6.matcher(var4);
			if (var7.find()) {
				var4 = var4.replace(var7.group(0), var5
						+ "(){const getAction=this._getAction,postAction=this._postAction,deleteAction=this._deleteAction;");
				Iterator var8 = var2.iterator();

				while (var8.hasNext()) {
					OnlCgformField var9 = (OnlCgformField) var8.next();
					Pattern var10 = Pattern.compile("(" + var9.getDbFieldName() + "\\s*\\(\\))");
					Matcher var11 = var10.matcher(var4);
					if (var11.find()) {
						var4 = var4.replace(var11.group(0), var9.getDbFieldName() + "(that,event)");
					}
				}
			}

			var0.setCgJs(var4);
			a(var0);
			a(var0, var1, var2);
		}
	}

	public static void a(OnlCgformEnhanceJs var0) {
		String var1 = var0.getCgJs();
		String var2 = "show";
		Pattern var3 = Pattern.compile("(" + var2 + "\\s*\\(\\)\\s*\\{)");
		Matcher var4 = var3.matcher(var1);
		if (var4.find()) {
			var1 = var1.replace(var4.group(0), var2
					+ "(that){const getAction=this._getAction,postAction=this._postAction,deleteAction=this._deleteAction;");
		}

		var0.setCgJs(var1);
	}

	public static String d(String var0) {
		log.info("最终的增强JS", var0);
		return "class OnlineEnhanceJs{constructor(getAction,postAction,deleteAction){this._getAction=getAction;this._postAction=postAction;this._deleteAction=deleteAction;}"
				+ var0 + "}";
	}

	public static String d(String var0, List<OnlCgformButton> var1) {
		if (var1 != null) {
			Iterator var2 = var1.iterator();

			while (true) {
				while (var2.hasNext()) {
					OnlCgformButton var3 = (OnlCgformButton) var2.next();
					String var4 = var3.getButtonCode();
					Pattern var5;
					Matcher var6;
					if ("link".equals(var3.getButtonStyle())) {
						var5 = Pattern.compile("(" + var4 + "\\s*\\(row\\)\\s*\\{)");
						var6 = var5.matcher(var0);
						if (var6.find()) {
							var0 = var0.replace(var6.group(0), var4
									+ "(that,row){const getAction=this._getAction,postAction=this._postAction,deleteAction=this._deleteAction;");
						} else {
							Pattern var7 = Pattern.compile("(" + var4 + "\\s*\\(\\)\\s*\\{)");
							Matcher var8 = var7.matcher(var0);
							if (var8.find()) {
								var0 = var0.replace(var8.group(0), var4
										+ "(that){const getAction=this._getAction,postAction=this._postAction,deleteAction=this._deleteAction;");
							}
						}
					} else if ("button".equals(var3.getButtonStyle()) || "form".equals(var3.getButtonStyle())) {
						var5 = Pattern.compile("(" + var4 + "\\s*\\(\\)\\s*\\{)");
						var6 = var5.matcher(var0);
						if (var6.find()) {
							var0 = var0.replace(var6.group(0), var4
									+ "(that){const getAction=this._getAction,postAction=this._postAction,deleteAction=this._deleteAction;");
						}
					}
				}

				return var0;
			}
		} else {
			return var0;
		}
	}

	public static JSONArray getColumns(List<OnlCgformField> fields, List<String> hiddenFields) {
		JSONArray jsonArray = new JSONArray();
		ISysBaseAPI sysBaseAPI = SpringContextUtils.getBean(ISysBaseAPI.class);
		Iterator<OnlCgformField> iterator = fields.iterator();

		while (true) {
			OnlCgformField field;
			String dbFieldName;
			do {
				if (!iterator.hasNext()) {
					return jsonArray;
				}

				field = iterator.next();
				dbFieldName = field.getDbFieldName();
			} while ("id".equals(dbFieldName));

			JSONObject jsonObject = new JSONObject();
			if (hiddenFields.indexOf(dbFieldName) >= 0 || field.getIsReadOnly() == 1) {
				jsonObject.put("disabled", true);
			}

			jsonObject.put("title", field.getDbFieldTxt());
			jsonObject.put("key", dbFieldName);
			jsonObject.put("width", "186px");
			String type = getType(field);
			jsonObject.put("type", type);
			if (type.equals("file") || type.equals("image")) {
				jsonObject.put("responseName", "message");
				jsonObject.put("token", true);
			}

			if (type.equals("switch")) {
				jsonObject.put("type", "checkbox");
				JSONArray yn = new JSONArray();
				if (oConvertUtils.isEmpty(field.getFieldExtendJson())) {
					yn.add("Y");
					yn.add("N");
				} else {
					yn = JSONArray.parseArray(field.getFieldExtendJson());
				}

				jsonObject.put("customValue", yn);
			}

			if (type.equals("popup")) {
				jsonObject.put("popupCode", field.getDictTable());
				jsonObject.put("orgFields", field.getDictField());
				jsonObject.put("destFields", field.getDictText());
				jsonObject.put("extendJson", field.getFieldExtendJson());
				String dictText = field.getDictText();
				if (dictText != null && !dictText.equals("")) {
					List<String> var10 = new ArrayList<>();
					String[] var11 = dictText.split(",");
					String[] var12 = var11;
					int var13 = var11.length;

					for (int var14 = 0; var14 < var13; ++var14) {
						String var15 = var12[var14];
						if (!isExistField(var15, fields)) {
							var10.add(var15);
							JSONObject var16 = new JSONObject();
							var16.put("title", var15);
							var16.put("key", var15);
							var16.put("type", "hidden");
							jsonArray.add(var16);
						}
					}
				}
			}

			jsonObject.put("defaultValue", field.getDbDefaultVal());
			jsonObject.put("fieldDefaultValue", field.getFieldDefaultValue());
			jsonObject.put("placeholder", "请输入" + field.getDbFieldTxt());
			jsonObject.put("validateRules", b(field));
			if ("list".equals(field.getFieldShowType()) || "radio".equals(field.getFieldShowType())
					|| "checkbox_meta".equals(field.getFieldShowType()) || "list_multi".equals(field.getFieldShowType())
					|| "sel_search".equals(field.getFieldShowType())) {
				List<DictModel> dictItems = new ArrayList<DictModel>();
				if (oConvertUtils.isNotEmpty(field.getDictTable())) {
					dictItems = sysBaseAPI.queryTableDictItemsByCode(field.getDictTable(), field.getDictText(),
							field.getDictField());
				} else if (oConvertUtils.isNotEmpty(field.getDictField())) {
					dictItems = sysBaseAPI.queryDictItemsByCode(field.getDictField());
				}

				jsonObject.put("options", dictItems);
				if ("list_multi".equals(field.getFieldShowType())) {
					jsonObject.put("width", "230px");
				}
			}

			jsonArray.add(jsonObject);
		}
	}

	private static JSONArray b(OnlCgformField var0) {
		JSONArray var1 = new JSONArray();
		JSONObject var2;
		if (var0.getDbIsNull() == 0 || "1".equals(var0.getFieldMustInput())) {
			var2 = new JSONObject();
			var2.put("required", true);
			var2.put("message", var0.getDbFieldTxt() + "不能为空!");
			var1.add(var2);
		}

		if (oConvertUtils.isNotEmpty(var0.getFieldValidType())) {
			var2 = new JSONObject();
			if ("only".equals(var0.getFieldValidType())) {
				var2.put("pattern", (Object) null);
			} else {
				var2.put("pattern", var0.getFieldValidType());
			}

			var2.put("message", var0.getDbFieldTxt() + "格式不正确");
			var1.add(var2);
		}

		return var1;
	}

	public static Map<String, Object> getValueType(Map<String, Object> var0) {
		HashMap var1 = new HashMap();
		if (var0 != null && !var0.isEmpty()) {
			Set var2 = var0.keySet();
			Iterator var3 = var2.iterator();

			while (var3.hasNext()) {
				String var4 = (String) var3.next();
				Object var5 = var0.get(var4);
				if (var5 instanceof Clob) {
					var5 = a((Clob) var5);
				} else if (var5 instanceof byte[]) {
					var5 = new String((byte[]) ((byte[]) var5));
				} else if (var5 instanceof Blob) {
					try {
						if (var5 != null) {
							Blob var6 = (Blob) var5;
							var5 = new String(var6.getBytes(1L, (int) var6.length()), "UTF-8");
						}
					} catch (Exception var7) {
						var7.printStackTrace();
					}
				}

				String var8 = var4.toLowerCase();
				var1.put(var8, var5);
			}

			return var1;
		} else {
			return var1;
		}
	}

	public static JSONObject a(JSONObject var0) {
		if (!TableUtil.isOracle()) {
			return var0;
		} else {
			JSONObject var1 = new JSONObject();
			if (var0 != null && !var0.isEmpty()) {
				Set var2 = var0.keySet();
				Iterator var3 = var2.iterator();

				while (var3.hasNext()) {
					String var4 = (String) var3.next();
					String var5 = var4.toLowerCase();
					var1.put(var5, var0.get(var4));
				}

				return var1;
			} else {
				return var1;
			}
		}
	}

	public static List<Map<String, Object>> transforRecords(List<Map<String, Object>> onlCgformFieldList) {
		List<Map<String, Object>> records = new ArrayList<>();
		Iterator<Map<String, Object>> iterator = onlCgformFieldList.iterator();

		while (iterator.hasNext()) {
			Map<String, Object> originalRecord = iterator.next();
			Map<String, Object> record = new HashMap<>();
			Set<String> fieldNames = originalRecord.keySet();
			Iterator<String> fieldNamesIterator = fieldNames.iterator();

			while (fieldNamesIterator.hasNext()) {
				String fieldName = fieldNamesIterator.next();
				Object fieldValue = originalRecord.get(fieldName);
				if (fieldValue instanceof Clob) {
					fieldValue = a((Clob) fieldValue);
				} else if (fieldValue instanceof byte[]) {
					fieldValue = new String((byte[]) ((byte[]) fieldValue));
				} else if (fieldValue instanceof Blob) {
					try {
						if (fieldValue != null) {
							Blob temp = (Blob) fieldValue;
							fieldValue = new String(temp.getBytes(1L, (int) temp.length()), "UTF-8");
						}
					} catch (Exception var10) {
						var10.printStackTrace();
					}
				}

				String lowerFieldName = fieldName.toLowerCase();
				record.put(lowerFieldName, fieldValue);
			}

			records.add(record);
		}

		return records;
	}

	public static String a(Clob var0) {
		String var1 = "";

		try {
			Reader var2 = var0.getCharacterStream();
			char[] var3 = new char[(int) var0.length()];
			var2.read(var3);
			var1 = new String(var3);
			var2.close();
		} catch (IOException var4) {
			var4.printStackTrace();
		} catch (SQLException var5) {
			var5.printStackTrace();
		}

		return var1;
	}

	public static Map<String, Object> c(String var0, List<OnlCgformField> var1, JSONObject var2) {
		StringBuffer var3 = new StringBuffer();
		StringBuffer var4 = new StringBuffer();
		String var5 = "";

		try {
			var5 = org.jeecg.modules.online.config.util.TableUtil.getDatabaseType();
		} catch (SQLException var14) {
			var14.printStackTrace();
		} catch (DBException var15) {
			var15.printStackTrace();
		}

		HashMap var6 = new HashMap();
		boolean var7 = false;
		String var8 = null;
		LoginUser var9 = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		if (var9 == null) {
			throw new JeecgBootException("online保存表单数据异常:系统未找到当前登陆用户信息");
		} else {
			Iterator var10 = var1.iterator();

			while (true) {
				while (var10.hasNext()) {
					OnlCgformField var11 = (OnlCgformField) var10.next();
					String var12 = var11.getDbFieldName();
					if (null == var12) {
						log.info("--------online保存表单数据遇见空名称的字段------->>" + var11.getId());
					} else if (var2.get(var12) != null || "CREATE_BY".equalsIgnoreCase(var12)
							|| "CREATE_TIME".equalsIgnoreCase(var12) || "SYS_ORG_CODE".equalsIgnoreCase(var12)) {
						appendUserData(var11, var9, var2, "CREATE_BY", "CREATE_TIME", "SYS_ORG_CODE");
						String var13;
						if ("".equals(var2.get(var12))) {
							var13 = var11.getDbType();
							if (DataTypeUtil.isNumberType(var13) || DataTypeUtil.isDateType(var13)) {
								continue;
							}
						}

						if ("id".equals(var12.toLowerCase())) {
							var7 = true;
							var8 = var2.getString(var12);
						} else {
							var3.append("," + var12);
							var13 = DataTypeUtil.getSql(var5, var11, var2, var6);
							var4.append("," + var13);
						}
					}
				}

				if (!var7 || oConvertUtils.isEmpty(var8)) {
					var8 = getIdWorkerId();
				}

				String var16 = "insert into " + getSubstring(var0) + "(" + "id" + var3.toString() + ") values(" + "'"
						+ var8 + "'" + var4.toString() + ")";
				var6.put("execute_sql_string", var16);
				log.info("--表单设计器表单保存sql-->" + var16);
				return var6;
			}
		}
	}

	public static Map<String, Object> d(String tableName, List<OnlCgformField> fields, JSONObject json) {
		StringBuffer var3 = new StringBuffer();
		HashMap var4 = new HashMap();
		String databaseType = "";

		try {
			databaseType = org.jeecg.modules.online.config.util.TableUtil.getDatabaseType();
		} catch (SQLException var11) {
			var11.printStackTrace();
		} catch (DBException var12) {
			var12.printStackTrace();
		}

		LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		if (user == null) {
			throw new JeecgBootException("online保存表单数据异常:系统未找到当前登陆用户信息");
		} else {
			Iterator<OnlCgformField> iterator = fields.iterator();
			while (iterator.hasNext()) {
				OnlCgformField field = iterator.next();
				String dbFieldName = field.getDbFieldName();
				if (null == dbFieldName) {
					log.info("--------online修改表单数据遇见空名称的字段------->>" + field.getId());
				} else if (!"id".equals(dbFieldName) && (json.get(dbFieldName) != null
						|| "UPDATE_BY".equalsIgnoreCase(dbFieldName) || "UPDATE_TIME".equalsIgnoreCase(dbFieldName)
						|| "SYS_ORG_CODE".equalsIgnoreCase(dbFieldName))) {
					appendUserData(field, user, json, "UPDATE_BY", "UPDATE_TIME", "SYS_ORG_CODE");
					String dbType;
					if ("".equals(json.get(dbFieldName))) {
						dbType = field.getDbType();
						if (DataTypeUtil.isNumberType(dbType) || DataTypeUtil.isDateType(dbType)) {
							continue;
						}
					}

					dbType = DataTypeUtil.getSql(databaseType, field, json, var4);
					var3.append(dbFieldName + "=" + dbType + ",");
				}
			}

			String var13 = var3.toString();
			if (var13.endsWith(",")) {
				var13 = var13.substring(0, var13.length() - 1);
			}

			String sql = "update " + getSubstring(tableName) + " set " + var13 + " where 1=1  " + " AND " + "id" + "="
					+ "'" + json.getString("id") + "'";
			log.info("--表单设计器表单编辑sql-->" + sql);
			var4.put("execute_sql_string", sql);
			return var4;
		}
	}

	public static Map<String, Object> a(String var0, String var1, String var2) {
		HashMap var3 = new HashMap();
		String var4 = "update " + getSubstring(var0) + " set " + var1 + "=" + "'" + 0 + "'" + " where 1=1  " + " AND "
				+ "id" + "=" + "'" + var2 + "'";
		log.info("--修改树节点状态：为无子节点sql-->" + var4);
		var3.put("execute_sql_string", var4);
		return var3;
	}

	public static String getFilterSql(String dictField) {
		return dictField != null && !"".equals(dictField) && !"0".equals(dictField)
				? "CODE like '" + dictField + "%" + "'" : "";
	}

	public static String getSubstring(String s) {
		return Pattern.matches("^[a-zA-z].*\\$\\d+$", s) ? s.substring(0, s.lastIndexOf("$")) : s;
	}

	public static void a(LinkDownProperty linkDownProperty, List<OnlCgformField> fields, List<String> var2) {
		String dictTable = linkDownProperty.getDictTable();
		JSONObject dictTableJson = JSONObject.parseObject(dictTable);
		String linkFields = dictTableJson.getString("linkField");
		ArrayList<BaseColumn> oherColumns = new ArrayList<>();
		if (oConvertUtils.isNotEmpty(linkFields)) {
			String[] linkFieldsArr = linkFields.split(",");
			Iterator<OnlCgformField> iterator = fields.iterator();

			while (true) {
				if (!iterator.hasNext()) {
					break;
				}

				OnlCgformField field = iterator.next();
				String fieldName = field.getDbFieldName();
				String[] var11 = linkFieldsArr;
				int length = linkFieldsArr.length;

				for (int i = 0; i < length; ++i) {
					String var14 = var11[i];
					if (var14.equals(fieldName)) {
						var2.add(fieldName);
						oherColumns.add(new BaseColumn(field.getDbFieldTxt(), fieldName));
						break;
					}
				}
			}
		}

		linkDownProperty.setOtherColumns(oherColumns);
	}

	public static String a(byte[] var0, String var1, String var2, String var3) {
		return CommonUtils.uploadOnlineImage(var0, var1, var2, var3);
	}

	public static List<String> e(List<OnlCgformField> var0) {
		ArrayList var1 = new ArrayList();
		Iterator var2 = var0.iterator();

		while (var2.hasNext()) {
			OnlCgformField var3 = (OnlCgformField) var2.next();
			if ("image".equals(var3.getFieldShowType())) {
				var1.add(var3.getDbFieldTxt());
			}
		}

		return var1;
	}

	public static List<String> b(List<OnlCgformField> var0, String var1) {
		ArrayList var2 = new ArrayList();
		Iterator var3 = var0.iterator();

		while (var3.hasNext()) {
			OnlCgformField var4 = (OnlCgformField) var3.next();
			if ("image".equals(var4.getFieldShowType())) {
				var2.add(var1 + "_" + var4.getDbFieldTxt());
			}
		}

		return var2;
	}

	public static String getIdWorkerId() {
		long var0 = IdWorker.getId();
		return String.valueOf(var0);
	}

	public static String getMessage(Exception e) {
		String var1 = e.getCause() != null ? e.getCause().getMessage() : e.getMessage();
		if (var1.indexOf("ORA-01452") != -1) {
			var1 = "ORA-01452: 无法 CREATE UNIQUE INDEX; 找到重复的关键字";
		} else if (var1.indexOf("duplicate key") != -1) {
			var1 = "无法 CREATE UNIQUE INDEX; 找到重复的关键字";
		}

		return var1;
	}

	public static List<DictModel> getYNDict(OnlCgformField onlCgformField) {
		ArrayList<DictModel> dictModels = new ArrayList<>();
		String fieldExtendJson = onlCgformField.getFieldExtendJson();
		String y = "是";
		String n = "否";
		JSONArray var5 = JSONArray.parseArray("[\"Y\",\"N\"]");
		if (oConvertUtils.isNotEmpty(fieldExtendJson)) {
			var5 = JSONArray.parseArray(fieldExtendJson);
		}

		DictModel dictModelY = new DictModel(var5.getString(0), y);
		DictModel dictModelN = new DictModel(var5.getString(1), n);
		dictModels.add(dictModelY);
		dictModels.add(dictModelN);
		return dictModels;
	}

	private static String getType(OnlCgformField field) {
		if ("checkbox".equals(field.getFieldShowType())) {
			return "checkbox";
		} else if ("list".equals(field.getFieldShowType())) {
			return "select";
		} else if ("switch".equals(field.getFieldShowType())) {
			return "switch";
		} else if (!"image".equals(field.getFieldShowType()) && !"file".equals(field.getFieldShowType())
				&& !"radio".equals(field.getFieldShowType()) && !"popup".equals(field.getFieldShowType())
				&& !"list_multi".equals(field.getFieldShowType()) && !"sel_search".equals(field.getFieldShowType())) {
			if ("datetime".equals(field.getFieldShowType())) {
				return "datetime";
			} else if ("date".equals(field.getFieldShowType())) {
				return "date";
			} else if (DbType.INT.equals(field.getDbType())) {
				return "inputNumber";
			} else {
				return !DbType.DOUBLE.equals(field.getDbType()) && !DbType.BIG_DECIMAL.equals(field.getDbType())
						? "input" : "inputNumber";
			}
		} else {
			return field.getFieldShowType();
		}
	}
}
