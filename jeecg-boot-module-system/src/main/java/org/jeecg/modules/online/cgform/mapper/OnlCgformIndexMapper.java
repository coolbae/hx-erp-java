package org.jeecg.modules.online.cgform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.online.cgform.entity.OnlCgformIndex;

public interface OnlCgformIndexMapper extends BaseMapper<OnlCgformIndex> {
    int queryIndexCount(@Param("sqlStr") String var1);
}
