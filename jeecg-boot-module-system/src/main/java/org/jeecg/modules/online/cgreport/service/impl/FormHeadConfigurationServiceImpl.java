package org.jeecg.modules.online.cgreport.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.online.cgreport.entity.FormHeadConfiguration;
import org.jeecg.modules.online.cgreport.mapper.FormHeadConfigurationMapper;
import org.jeecg.modules.online.cgreport.service.IFormHeadConfigurationService;
import org.springframework.stereotype.Service;

@Service
public class FormHeadConfigurationServiceImpl extends ServiceImpl<FormHeadConfigurationMapper, FormHeadConfiguration> implements IFormHeadConfigurationService {
}
