package org.jeecg.modules.online.cgreport.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.online.cgreport.entity.OnlCgreportParam;
import org.jeecg.modules.online.cgreport.service.IOnlCgreportParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

@RestController
@RequestMapping({"/online/cgreport/param"})
public class OnlCgreportParamController {
    @Autowired
    private IOnlCgreportParamService onlCgreportParamService;

    @GetMapping({"/listByHeadId"})
    public Result<?> listByHeadId(@RequestParam("headId") String headId) {
        QueryWrapper<OnlCgreportParam> qw = new QueryWrapper<>();
        qw.eq("cgrhead_id", headId);
        qw.orderByAsc("order_num");
        List<OnlCgreportParam> list = this.onlCgreportParamService.list(qw);
        Result<List<OnlCgreportParam>> result = new Result<>();
        result.setSuccess(true);
        result.setResult(list);
        return result;
    }

    @GetMapping({"/list"})
    public Result<IPage<OnlCgreportParam>> list(OnlCgreportParam cgreportParam, @RequestParam(name = "pageNo",defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize, HttpServletRequest request) {
        Result<IPage<OnlCgreportParam>> result = new Result<>();
        QueryWrapper<OnlCgreportParam> qw = QueryGenerator.initQueryWrapper(cgreportParam, request.getParameterMap());
        Page<OnlCgreportParam> page = new Page<>(pageNo, pageSize);
        IPage<OnlCgreportParam> ipage = this.onlCgreportParamService.page(page, qw);
        result.setSuccess(true);
        result.setResult(ipage);
        return result;
    }

    @PostMapping({"/add"})
    public Result<?> add(@RequestBody OnlCgreportParam onlCgreportParam) {
        this.onlCgreportParamService.save(onlCgreportParam);
        return Result.OK("添加成功!", null);
    }

    @PutMapping({"/edit"})
    public Result<?> edit(@RequestBody OnlCgreportParam onlCgreportParam) {
        this.onlCgreportParamService.updateById(onlCgreportParam);
        return Result.OK("编辑成功!", null);
    }

    @DeleteMapping({"/delete"})
    public Result<?> delete(@RequestParam(name = "id",required = true) String id) {
        this.onlCgreportParamService.removeById(id);
        return Result.OK("删除成功!", null);
    }

    @DeleteMapping({"/deleteBatch"})
    public Result<?> deleteBatch(@RequestParam(name = "ids",required = true) String ids) {
        this.onlCgreportParamService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!", null);
    }

    @GetMapping({"/queryById"})
    public Result<OnlCgreportParam> queryById(@RequestParam(name = "id",required = true) String id) {
        Result<OnlCgreportParam> result = new Result<>();
        OnlCgreportParam onlCgreportParam = this.onlCgreportParamService.getById(id);
        result.setResult(onlCgreportParam);
        return result;
    }
}
