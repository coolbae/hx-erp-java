package org.jeecg.modules.online.cgform.converter.impl;

import org.jeecg.modules.online.cgform.converter.field.TableFieldCommentConverter;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;

public class SelTreeFieldCommentConverter extends TableFieldCommentConverter {

	public SelTreeFieldCommentConverter(OnlCgformField onlCgformField) {

		String dictText = onlCgformField.getDictText();
		String[] strings = dictText.split(",");
		this.setTable(onlCgformField.getDictTable());
		this.setCode(strings[0]);
		this.setText(strings[2]);
	}
}
