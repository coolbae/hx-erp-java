package org.jeecg.modules.online.cgreport.mapper;

import org.jeecg.modules.online.cgreport.entity.DiagramCombinationQueryConfig;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 查询配置
 * @Author: jeecg-boot
 * @Date:   2020-11-13
 * @Version: V1.0
 */
public interface DiagramCombinationQueryConfigMapper extends BaseMapper<DiagramCombinationQueryConfig> {

}
