package org.jeecg.modules.online.cgreport.service;

import org.jeecg.modules.online.cgreport.entity.DiagramCombinationQueryConfig;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 查询配置
 * @Author: jeecg-boot
 * @Date:   2020-11-13
 * @Version: V1.0
 */
public interface IDiagramCombinationQueryConfigService extends IService<DiagramCombinationQueryConfig> {

}
