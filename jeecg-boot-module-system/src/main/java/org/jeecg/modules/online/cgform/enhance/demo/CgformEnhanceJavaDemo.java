package org.jeecg.modules.online.cgform.enhance.demo;

import com.alibaba.fastjson.JSONObject;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import org.jeecg.modules.online.cgform.enhance.CgformEnhanceJavaInter;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.springframework.stereotype.Component;

@Slf4j
@Component("cgformEnhanceJavaDemo")
public class CgformEnhanceJavaDemo implements CgformEnhanceJavaInter {

    public int execute(String tableName, JSONObject json) throws BusinessException {
        log.info("----------我是自定义java增强测试demo类-----------");
        log.info("--------当前tableName=>" + tableName);
        log.info("--------当前JSON数据=>" + json.toJSONString());
        return 1;
    }

    public int execute(String tableName, Map map) throws BusinessException {
        return 1;
    }
}
