package org.jeecg.modules.online.cgreport.entity;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@TableName("CS_DIAGRAM_FIELD_CONFIG")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "图表字段配置对象", description = "图表字段配置")
public class DiagramFieldConfiguration extends JeecgEntity {

	private static final long serialVersionUID = -5009925794758375152L;

	@Excel(name = "图表code", width = 15)
	@ApiModelProperty(value = "图表code")
	@TableField(value = "DIAGRAM_CODE")
	private String diagramCode;// "27accd782f98c41eae5fad1b0514238d"

	@Excel(name = "字段名", width = 15)
	@ApiModelProperty(value = "字段名")
	private String fieldName;// "id"

	@Excel(name = "字段文本", width = 15)
	@ApiModelProperty(value = "字段文本")
	private String fieldTxt;// "id"

	@Excel(name = "排序", width = 15)
	@ApiModelProperty(value = "排序")
	private Integer orderNum;// 1

	@Excel(name = "字段类型", width = 15)
	@ApiModelProperty(value = "字段类型")
	private String fieldType;// "String"

	@Excel(name = "是否显示", width = 15)
	@ApiModelProperty(value = "是否显示")
	private Integer isShow;// 0：否；1：是

	@Excel(name = "计算总计", width = 15)
	@ApiModelProperty(value = "计算总计")
	private Integer isTotal;// 0：否；1：是

	@Excel(name = "是否查询", width = 15)
	@ApiModelProperty(value = "是否查询")
	private Integer searchFlag;// 0：否；1：是

	@Excel(name = "查询模式", width = 15)
	@ApiModelProperty(value = "查询模式")
	private String searchMode;// "group"

	@Excel(name = "字典Code", width = 15)
	@ApiModelProperty(value = "字典Code")
	private String dictCode;// ""

	@Excel(name = "字段链接", width = 15)
	@ApiModelProperty(value = "字段链接")
	private String fieldHref;// null

	@Excel(name = "替换值", width = 15)
	@ApiModelProperty(value = "替换值")
	private String replaceVal;// null
	
	@Excel(name = "函数", width = 15)
	@ApiModelProperty(value = "函数")
	private String func;// null
	
	@Excel(name = "函数参数", width = 15)
	@ApiModelProperty(value = "函数参数")
	private String funcParam;// null
	
	@Excel(name = "聚合方式", width = 15)
	@ApiModelProperty(value = "聚合方式")
	private String aggregateType;

	@Excel(name = "JS增强", width = 15)
	@ApiModelProperty(value = "JS增强")
	private String jsEnhance;

	@Excel(name = "字段列表宽度", width = 15)
	@ApiModelProperty(value = "字段列表宽度")
	private int fieldWidth;
}
