package org.jeecg.modules.online.cgform.service;

import org.jeecg.modules.online.cgform.entity.OnlSystem;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 子系统
 * @Author: MxpIO
 * @Date:   2020-12-09
 * @Version: V1.0
 */
public interface IOnlSystemService extends IService<OnlSystem> {

}
