package org.jeecg.modules.online.cgreport.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.util.SqlSymbolUtil;
import org.jeecg.modules.online.cgreport.def.CgReportConstant;
import org.jeecg.modules.online.cgreport.entity.OnlCgreportItem;
import org.jeecg.modules.online.config.exception.DBException;
import org.jeecg.modules.online.config.util.TableUtil;

public class CgReportQueryParamUtil {
	
	/**
	 * 组装查询参数
	 * @param request 请求(查询值从此处取)
	 * @param item 动态报表字段配置
	 * @param pageSearchFields 页面参数查询字段（占位符的条件语句）
	 * @param paramData 占位符对应的数据
	 */
	public static void loadQueryParams(HttpServletRequest request, Map<String,Object> item, Map<String,Object> pageSearchFields,Map<String,Object> paramData) {
		String filedName = (String) item.get(CgReportConstant.ITEM_FIELDNAME);
		String queryMode = (String) item.get(CgReportConstant.ITEM_QUERYMODE);
		String filedType = (String) item.get(CgReportConstant.ITEM_FIELDTYPE);
		if("single".equals(queryMode)){
			//单条件组装方式
			String value =request.getParameter(filedName.toLowerCase());
			try {
				if(oConvertUtils.isEmpty(value)){
					return;
				}
				String uri = request.getQueryString();
				if(uri.contains(filedName+"=")){
					String contiansChinesevalue = new String(value.getBytes("ISO-8859-1"), "UTF-8");
					value = contiansChinesevalue;
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				return;
			} 
			if(oConvertUtils.isNotEmpty(value)){
				if(value.contains("*")){
					//模糊查询
					value = value.replaceAll("\\*", "%");
					pageSearchFields.put(filedName, CgReportConstant.OP_LIKE+":"+filedName);
				}else{
					pageSearchFields.put(filedName, CgReportConstant.OP_EQ+":"+filedName);
				}
			}
			paramData.put(filedName, covertData(filedType,value,true));
		}else if("group".equals(queryMode)){
			//范围查询组装
			String begin = request.getParameter(filedName.toLowerCase()+"_begin");
			String end = request.getParameter(filedName.toLowerCase()+"_end");
			if(oConvertUtils.isNotEmpty(begin)){
				String re = CgReportConstant.OP_RQ+":"+filedName+"_begin";
				pageSearchFields.put(filedName, re);
				paramData.put(filedName+"_begin", covertData(filedType,begin,true));
			} 
			if(oConvertUtils.isNotEmpty(end)){
				String re = CgReportConstant.OP_LQ+":"+filedName+"_end";
				pageSearchFields.put(new String(filedName), re);
				paramData.put(filedName+"_end", covertData(filedType,end,false));
			}
		}
	}

	private static Object covertData(String fieldType,String value,boolean isBegin){
		Object obj = null;
		if(oConvertUtils.isNotEmpty(value)){
			if(CgReportConstant.TYPE_STRING.equalsIgnoreCase(fieldType)){
				obj = value;
			}else if(CgReportConstant.TYPE_DATE.equalsIgnoreCase(fieldType)){
				if (value.length() == 19) {
				} else if (value.length() == 10) {
					if(isBegin){
						value +=" 00:00:00";
					}else{
						value +=" 23:59:59";
					}
				}
				SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				obj = DateUtils.str2Date(value, datetimeFormat);
			}else if(CgReportConstant.TYPE_DOUBLE.equalsIgnoreCase(fieldType)){
				obj = value;
			}else if(CgReportConstant.TYPE_INTEGER.equalsIgnoreCase(fieldType)){
				obj = value;
			}else{
				obj = value;
			}
		}
		return obj;
	}

	/**
	 * 将结果集转化为列表json格式
	 * @param result 结果集
	 * @param size 总大小
	 * @return 处理好的json格式
	 */
	public static String getJson(List<Map<String, Object>> result,Long size){
		JSONObject main = new JSONObject();
		JSONArray rows = new JSONArray();
		main.put("total",size );
		if(result!=null){
			for(Map m:result){
				JSONObject item = new JSONObject();
				Iterator  it =m.keySet().iterator();
				while(it.hasNext()){
					String key = (String) it.next();
					String value =String.valueOf(m.get(key));
					key = key.toLowerCase();
					if(key.contains("time")||key.contains("date")){
						value = datatimeFormat(value);
					}
					item.put(key,value );
				}
				rows.add(item);
			}
		}
		main.put("rows", rows);
		return main.toString();
	}

	/**
	 * 将结果集转化为列表json格式(不含分页信息)
	 * @param result 结果集
	 * @param size 总大小
	 * @return 处理好的json格式
	 */
	public static String getJson(List<Map<String, Object>> result){
		JSONArray rows = new JSONArray();
		for(Map m:result){
			JSONObject item = new JSONObject();
			Iterator  it =m.keySet().iterator();
			while(it.hasNext()){
				String key = (String) it.next();
				String value =String.valueOf(m.get(key));
				key = key.toLowerCase();
				if(key.contains("time")||key.contains("date")){
					value = datatimeFormat(value);
				}
				item.put(key,value );
			}
			rows.add(item);
		}
		return rows.toString();
	}

	/**
	 * 将毫秒数去掉
	 * @param datetime
	 * @return
	 */
	public static String datatimeFormat(String datetime){
		SimpleDateFormat  dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat  dateFormatTo = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d  = null;
		try{
			d = dateFormat.parse(datetime);
			return dateFormatTo.format(d);
		}catch (Exception e) {
			return datetime;
		}
	}

    public static String a(List<OnlCgreportItem> var0, Map<String, Object> var1, String var2) {
        StringBuffer var3 = new StringBuffer();
        String var4 = "";

        try {
            var4 = TableUtil.getDatabaseType();
        } catch (SQLException var11) {
            var11.printStackTrace();
        } catch (DBException var12) {
            var12.printStackTrace();
        }

        Iterator var5 = var0.iterator();

        while(true) {
            while(true) {
                String var7;
                String var8;
                Object var13;
                do {
                    while(true) {
                        OnlCgreportItem var6;
                        do {
                            if (!var5.hasNext()) {
                                return var3.toString();
                            }

                            var6 = (OnlCgreportItem)var5.next();
                            var7 = var6.getFieldName();
                            var8 = var6.getFieldType();
                        } while(1 != var6.getIsSearch());

                        Object var9;
                        if ("group".equals(var6.getSearchMode())) {
                            var9 = var1.get(var7 + "_begin");
                            if (var9 != null) {
                                var3.append(" and " + var2 + var7 + " >= ");
                                if (!"Long".equals(var8) && !"Integer".equals(var8)) {
                                    if ("ORACLE".equals(var4)) {
                                        if (var8.toLowerCase().equals("datetime")) {
                                            var3.append(SqlSymbolUtil.toDateyMdHms(var9.toString()));
                                        } else if (var8.toLowerCase().equals("date")) {
                                            var3.append(SqlSymbolUtil.toDateyMd(var9.toString()));
                                        }
                                    } else {
                                        var3.append("'" + var9.toString() + "'");
                                    }
                                } else {
                                    var3.append(var9.toString());
                                }
                            }

                            var13 = var1.get(var7 + "_end");
                            break;
                        }

                        var9 = var1.get(var7);
                        if (var9 != null) {
                            String var10 = QueryGenerator.getSingleQueryConditionSql(var7, var2, var9, !"Long".equals(var8) && !"Integer".equals(var8));
                            var3.append(" and " + var10);
                        }
                    }
                } while(var13 == null);

                var3.append(" and " + var2 + var7 + " <= ");
                if (!"Long".equals(var8) && !"Integer".equals(var8)) {
                    if ("ORACLE".equals(var4)) {
                        if (var8.toLowerCase().equals("datetime")) {
                            var3.append(SqlSymbolUtil.toDateyMdHms(var13.toString()));
                        } else if (var8.toLowerCase().equals("date")) {
                            var3.append(SqlSymbolUtil.toDateyMd(var13.toString()));
                        }
                    } else {
                        var3.append("'" + var13.toString() + "'");
                    }
                } else {
                    var3.append(var13.toString());
                }
            }
        }
    }
}
