package org.jeecg.modules.config;

import java.util.HashMap;
import java.util.Map;

import org.jeecg.modules.system.model.SiteInfoModel;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class InitSiteDataConfig implements InitializingBean {
	
	@Value("${jeecg.site.appName}")
	private String appName;
	@Value("${jeecg.site.appDesc}")
	private String appDesc;
	@Value("${jeecg.site.appLogo}")
	private String appLogo;
	@Value("${jeecg.site.appLogoWhite}")
	private String appLogoWhite;
	@Value("${jeecg.site.allowRegister}")
	private boolean allowRegister;
	@Value("${jeecg.site.allowPhoneLogin}")
	private boolean allowPhoneLogin;
	@Value("${jeecg.site.allowThirdLogin}")
	private boolean allowThirdLogin;
	@Value("${jeecg.site.helpPath}")
	private String helpPath;
	@Value("${jeecg.site.appAbbr}")
	private String appAbbr;
	

	public static Map<String, Object> SITE_PARAM = new HashMap<>();
	
	@Override
	public void afterPropertiesSet() throws Exception {
		SiteInfoModel siteInfo = new SiteInfoModel();
		siteInfo.setAppName(appName);
		siteInfo.setAppAbbr(appAbbr);
		siteInfo.setAppDesc(appDesc);
		siteInfo.setAppLogo(appLogo);
		siteInfo.setAppLogoWhite(appLogoWhite);
		siteInfo.setHelpPath(helpPath);
		siteInfo.setAllowPhoneLogin(allowPhoneLogin);
		siteInfo.setAllowRegister(allowRegister);
		siteInfo.setAllowThirdLogin(allowThirdLogin);
		
		SITE_PARAM.put("siteInfo", siteInfo);
	}

}
