<!DOCTYPE html>
<html lang="">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <link rel="icon" href="/favicon.ico">
  <title>minder-editor</title>
  <link href="/kFormDesign/css/app.6d6fe5c1.css" rel="preload" as="style">
  <link href="/kFormDesign/css/chunk-vendors.6992b7b1.css" rel="preload" as="style">
  <link href="/kFormDesign/js/app.f30ff171.js" rel="preload" as="script">
  <link href="/kFormDesign/js/chunk-vendors.5dddd128.js" rel="preload" as="script">
  <link href="/kFormDesign/css/chunk-vendors.6992b7b1.css" rel="stylesheet">
  <link href="/kFormDesign/css/app.6d6fe5c1.css" rel="stylesheet">
</head>

<body><noscript><strong>We're sorry but minder-editor doesn't work properly without JavaScript enabled. Please enable it
      to continue.</strong></noscript>
  <div id="app"></div>
  <script src="/kFormDesign/js/chunk-vendors.5dddd128.js"></script>
  <script src="/kFormDesign/js/app.f30ff171.js"></script>
</body>

</html>