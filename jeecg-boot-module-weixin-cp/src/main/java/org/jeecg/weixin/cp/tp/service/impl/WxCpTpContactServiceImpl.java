package org.jeecg.weixin.cp.tp.service.impl;

import lombok.RequiredArgsConstructor;
import org.jeecg.weixin.common.error.WxErrorException;
import org.jeecg.weixin.cp.bean.WxCpTpContactSearch;
import org.jeecg.weixin.cp.bean.WxCpTpContactSearchResp;
import org.jeecg.weixin.cp.tp.service.WxCpTpContactService;
import org.jeecg.weixin.cp.tp.service.WxCpTpService;

import static org.jeecg.weixin.cp.constant.WxCpApiPathConsts.Tp.CONTACT_SEARCH;

/**
 * @author uianz
 * @description
 * @since 2020/12/23 下午 02:39
 */
@RequiredArgsConstructor
public class WxCpTpContactServiceImpl implements WxCpTpContactService {

	private final WxCpTpService mainService;

	@Override
	public WxCpTpContactSearchResp contactSearch(WxCpTpContactSearch wxCpTpContactSearch) throws WxErrorException {
		String responseText = mainService.post(mainService.getWxCpTpConfigStorage().getApiUrl(CONTACT_SEARCH)
				+ "?provider_access_token=" + mainService.getWxCpTpConfigStorage().getCorpSecret(),
				wxCpTpContactSearch.toJson());
		return WxCpTpContactSearchResp.fromJson(responseText);
	}

}
