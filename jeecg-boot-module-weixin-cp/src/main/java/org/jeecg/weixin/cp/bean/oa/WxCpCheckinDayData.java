package org.jeecg.weixin.cp.bean.oa;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * 企业微信日报数据.
 *
 * @author Element
 * @date 2019-04-06 11:01
 */
@Data
public class WxCpCheckinDayData implements Serializable {
	private static final long serialVersionUID = -7503743162686574379L;
	
	@SerializedName("base_info")
	private BaseInfo baseInfo;
	
	@SerializedName("summary_info")
	private SummaryInfo summaryInfo;
	
	@SerializedName("holiday_infos")
	private List<HolidayInfo> holidayInfos;
	
	@SerializedName("exception_infos")
	private List<ExceptionInfo> exceptionInfos;
	
	@SerializedName("ot_info")
	private OtInfo otInfo;
	
	@SerializedName("sp_items")
	private List<SpItem> spItems;
	
	@Data
	public static class BaseInfo implements Serializable {

		private static final long serialVersionUID = -6960415412691461261L;

		@SerializedName("date")
		private Long date;

		@SerializedName("recordType")
		private Long record_type;

		@SerializedName("name")
		private String name;

		@SerializedName("name_ex")
		private String nameEx;
		
		@SerializedName("departs_name")
		private String departsName;

		@SerializedName("acctid")
		private String acctid;
	}
	
	@Data
	public static class SummaryInfo implements Serializable {

		private static final long serialVersionUID = 804448085133910927L;

		@SerializedName("checkin_count")
		private Integer checkinCount;

		@SerializedName("regular_work_sec")
		private Integer regularWorkSec;

		@SerializedName("standard_work_sec")
		private Integer standardWorkSec;

		@SerializedName("earliest_time")
		private Integer earliestTime;
		
		@SerializedName("lastest_time")
		private Integer lastestTime;

	}
	
	@Data
	public static class HolidayInfo implements Serializable {

		private static final long serialVersionUID = -5233286327235549946L;

		@SerializedName("sp_number")
		private String spNumber;

		@SerializedName("sp_title")
		private SpTitle spTitle;

		@SerializedName("sp_description")
		private SpDescription spDescription;

	}
	
	@Data
	public static class SpTitle implements Serializable {

		private static final long serialVersionUID = 7710804587140632635L;
		@SerializedName("data")
		private List<SpTextData> data;

	}
	
	@Data
	public static class SpTextData implements Serializable {

		private static final long serialVersionUID = 6514521541897818105L;

		@SerializedName("text")
		private String text;
		
		@SerializedName("lang")
		private String lang;

	}
	
	@Data
	public static class SpDescription implements Serializable {

		private static final long serialVersionUID = -7099072383062904514L;
		
		@SerializedName("data")
		private List<SpTextData> data;
	}
	
	@Data
	public static class ExceptionInfo implements Serializable {

		private static final long serialVersionUID = -6626375036262047874L;

		@SerializedName("exception")
		private Long exception;
		
		@SerializedName("count")
		private Integer count;
		
		@SerializedName("duration")
		private Integer duration;
	}
	
	@Data
	public static class OtInfo implements Serializable {

		private static final long serialVersionUID = -4230867371871978827L;

		@SerializedName("ot_status")
		private Long otStatus;
		
		@SerializedName("ot_duration")
		private Long otDuration;
		
		@SerializedName("exception_duration")
		private Long[] exceptionDuration;
	}
	
	@Data
	public static class SpItem implements Serializable {

		private static final long serialVersionUID = -6403887472076333538L;

		@SerializedName("type")
		private Long type;
		
		@SerializedName("vacation_id")
		private Long vacationId;
		
		@SerializedName("count")
		private Long count;
		
		@SerializedName("duration")
		private Long duration;
		
		@SerializedName("time_type")
		private Long timeType;
		
		@SerializedName("name")
		private String name;
	}

	public String getToText() {
		StringBuffer text = new StringBuffer("");
		List<SpItem> items = this.getSpItems();
		if(!CollectionUtils.isEmpty(items)){
			for(SpItem item : items){
				if(item.getType() == 1L){
					switch (item.getName()) {
					case "事假":
						if(item.getTimeType() == 0L && item.getDuration() < 86400L && item.getDuration() > 0L){
							text.append("/0.5事");
						}else if(item.getTimeType() == 0L && item.getDuration() > 0L){
							text.append("/事");
						}else if(item.getTimeType() == 1L && item.getDuration() > 0L){
							text.append("/"+ item.getDuration()/3600L +"h事");
						}
						break;
					case "调休":
						if(item.getTimeType() == 0L && item.getDuration() < 86400L && item.getDuration() > 0L){
							text.append("/〒");
						}else if(item.getTimeType() == 0L && item.getDuration() > 0L){
							text.append("/T");
						}else if(item.getTimeType() == 1L && item.getDuration() > 0L){
							text.append("/"+ item.getDuration()/3600L +"h〒");
						}
						break;
					case "病假":
						if(item.getTimeType() == 0L && item.getDuration() < 86400L && item.getDuration() > 0L){
							text.append("/0.5病");
						}else if(item.getTimeType() == 0L && item.getDuration() > 0L){
							text.append("/病");
						}else if(item.getTimeType() == 1L && item.getDuration() > 0L){
							text.append("/"+ item.getDuration()/3600L +"h病");
						}
						break;
					default:
						if(item.getTimeType() == 0L && item.getDuration() < 86400L && item.getDuration() > 0L){
							text.append("/0.5假");
						}else if(item.getTimeType() == 0L && item.getDuration() > 0L){
							text.append("/假");
						}else if(item.getTimeType() == 1L && item.getDuration() > 0L){
							text.append("/"+ item.getDuration()/3600L +"h假");
						}
						break;
					}
				}
			}
		}
		List<ExceptionInfo> exps = this.getExceptionInfos();
		if(CollectionUtils.isEmpty(exps)){
			for(ExceptionInfo exp : exps){
				if(exp.getException() == 1L || exp.getException() == 2L && exp.getDuration() > 0){
					text.append("/" + exp.getDuration()/60L);
				}else if(exp.getException() == 3L && exp.getDuration() > 0){
					text.append("/缺");
				}else if(exp.getException() == 4L && exp.getDuration() > 0){
					text.append("/旷");
				}
			}
		}
		SummaryInfo summaryInfo = this.getSummaryInfo();
		if(text.length() == 0 && summaryInfo.getCheckinCount() == 2){
			text.append("√");
		}
		
		return text.toString();
	}

}
