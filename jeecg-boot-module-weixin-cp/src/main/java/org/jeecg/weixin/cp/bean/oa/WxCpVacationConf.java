package org.jeecg.weixin.cp.bean.oa;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class WxCpVacationConf implements Serializable {

	private static final long serialVersionUID = -4846377231621628051L;

	@SerializedName("id")
	private Long id;
	
	@SerializedName("name")
	private String name;
	
	//假期时间刻度：0-按天请假；1-按小时请假
	@SerializedName("time_attr")
	private Long timeAttr;
	
	//时长计算类型：0-自然日；1-工作日
	@SerializedName("duration_type")
	private Long durationType;
	
	@SerializedName("perday_duration")
	private Long perdayDuration;
	
}
