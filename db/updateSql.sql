-- 绩效计算  20210328

DROP TABLE IF EXISTS `hr_assessment_task_result_summary`;
CREATE TABLE `hr_assessment_task_result_summary` (
`id`  varchar(64) NOT NULL ,
`create_by`  varchar(64) NULL DEFAULT NULL ,
`create_time`  datetime NULL DEFAULT NULL ,
`update_by`  varchar(64) NULL DEFAULT NULL ,
`update_time`  datetime NULL DEFAULT NULL ,
`instance_code`  varchar(64) NULL DEFAULT NULL ,
`username`  varchar(64) NULL DEFAULT NULL ,
`score`  decimal(20,6) NULL DEFAULT NULL ,
`final_score`  decimal(20,6) NULL DEFAULT NULL ,
`assessment_score`  decimal(20,6) NULL DEFAULT NULL ,
`level_desc`  varchar(255) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

ALTER TABLE `hr_assessment_rule` ADD COLUMN `score_super`  decimal(20,6) NULL DEFAULT NULL AFTER `plan_code`;
ALTER TABLE `hr_assessment_rule` ADD COLUMN `score_lower`  decimal(20,6) NULL DEFAULT NULL AFTER `score_super`;
ALTER TABLE `hr_assessment_rule` DROP COLUMN `score`;
ALTER TABLE `hr_assessment_instance` ADD COLUMN `plan_code` varchar(64) NULL DEFAULT NULL AFTER `update_time`;

DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant` (
`id`  int(5) NOT NULL COMMENT '租户编码' ,
`name`  varchar(100) NULL DEFAULT NULL COMMENT '租户名称' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`create_by`  varchar(100) NULL DEFAULT NULL COMMENT '创建人' ,
`begin_date`  datetime NULL DEFAULT NULL COMMENT '开始时间' ,
`end_date`  datetime NULL DEFAULT NULL COMMENT '结束时间' ,
`status`  int(1) NULL DEFAULT NULL COMMENT '状态 1正常 0冻结' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

-- 绩效升级SQL 20210321

DROP TABLE IF EXISTS `hr_assessment_index`;
CREATE TABLE `hr_assessment_index` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `plan_code` varchar(64) DEFAULT NULL,
  `index_name` varchar(255) DEFAULT NULL,
  `index_desc` varchar(4000) DEFAULT NULL,
  `weight` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_instance`;
CREATE TABLE `hr_assessment_instance` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `instance_code` varchar(64) DEFAULT NULL,
  `instance_name` varchar(255) DEFAULT NULL,
  `instance_status` varchar(64) DEFAULT NULL,
  `remark` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_plan`;
CREATE TABLE `hr_assessment_plan` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `plan_code` varchar(64) DEFAULT NULL,
  `plan_name` varchar(255) DEFAULT NULL,
  `plan_type` varchar(64) DEFAULT NULL,
  `account_cycle` varchar(64) DEFAULT NULL,
  `self_able` tinyint(1) DEFAULT NULL,
  `remark` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_proc`;
CREATE TABLE `hr_assessment_proc` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `plan_code` varchar(64) DEFAULT NULL,
  `proc_name` varchar(255) DEFAULT NULL,
  `assignee` varchar(255) DEFAULT NULL,
  `weight` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_rule`;
CREATE TABLE `hr_assessment_rule` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `plan_code` varchar(64) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `level_desc` varchar(255) DEFAULT NULL,
  `assessment_score` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_task`;
CREATE TABLE `hr_assessment_task` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `instance_code` varchar(64) DEFAULT NULL,
  `task_code` varchar(64) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  `leader` varchar(64) DEFAULT NULL,
  `weight` decimal(20,6) DEFAULT NULL,
  `task_status` varchar(64) DEFAULT NULL,
  `task_complete_time` datetime DEFAULT NULL,
  `scale` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_task_result`;
CREATE TABLE `hr_assessment_task_result` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `task_code` varchar(64) DEFAULT NULL,
  `index_name` varchar(255) DEFAULT NULL,
  `index_desc` varchar(4000) DEFAULT NULL,
  `weight` decimal(20,6) DEFAULT NULL,
  `score` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_user`;
CREATE TABLE `hr_assessment_user` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `plan_code` varchar(64) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 菜单
INSERT INTO `sys_permission` (`id`, `parent_id`, `name`, `url`, `component`, `component_name`, `redirect`, `menu_type`, `perms`, `perms_type`, `sort_no`, `always_show`, `icon`, `is_route`, `is_leaf`, `keep_alive`, `hidden`, `description`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `rule_flag`, `status`, `internal_or_external`) VALUES ('1373606643710836738', '1373511790884855809', '绩效任务管理', '/assessment/AssessmentInstance', 'personnel/assessment/AssessmentInstance', NULL, NULL, '1', NULL, '1', '2.00', '0', NULL, '1', '1', '0', '0', NULL, 'admin', '2021-03-21 20:05:19', 'admin', '2021-03-21 20:06:36', '0', '0', '1', '0');
INSERT INTO `sys_permission` (`id`, `parent_id`, `name`, `url`, `component`, `component_name`, `redirect`, `menu_type`, `perms`, `perms_type`, `sort_no`, `always_show`, `icon`, `is_route`, `is_leaf`, `keep_alive`, `hidden`, `description`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `rule_flag`, `status`, `internal_or_external`) VALUES ('1373595260726292481', '1373594259348795394', '绩效考核审批权限', NULL, NULL, NULL, NULL, '2', 'AssessmentInstance:check', '1', '1.00', '0', NULL, '1', '1', '0', '0', NULL, 'admin', '2021-03-21 19:20:05', 'admin', '2021-03-21 20:07:02', '0', '0', '1', '0');
INSERT INTO `sys_permission` (`id`, `parent_id`, `name`, `url`, `component`, `component_name`, `redirect`, `menu_type`, `perms`, `perms_type`, `sort_no`, `always_show`, `icon`, `is_route`, `is_leaf`, `keep_alive`, `hidden`, `description`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `rule_flag`, `status`, `internal_or_external`) VALUES ('1373594259348795394', '1373511790884855809', '绩效考核执行', '/assessment/AssessmentTask', 'personnel/assessment/AssessmentTask', NULL, NULL, '1', NULL, '1', '3.00', '0', NULL, '1', '0', '0', '0', NULL, 'admin', '2021-03-21 19:16:06', 'admin', '2021-03-21 20:06:53', '0', '0', '1', '0');
INSERT INTO `sys_permission` (`id`, `parent_id`, `name`, `url`, `component`, `component_name`, `redirect`, `menu_type`, `perms`, `perms_type`, `sort_no`, `always_show`, `icon`, `is_route`, `is_leaf`, `keep_alive`, `hidden`, `description`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `rule_flag`, `status`, `internal_or_external`) VALUES ('1373511990877659138', '1373511790884855809', '考核计划', '/assessment/AssessmentPlan', 'personnel/assessment/AssessmentPlan', NULL, NULL, '1', NULL, '1', '1.00', '0', NULL, '1', '1', '0', '0', NULL, 'admin', '2021-03-21 13:49:12', NULL, NULL, '0', '0', '1', '0');
INSERT INTO `sys_permission` (`id`, `parent_id`, `name`, `url`, `component`, `component_name`, `redirect`, `menu_type`, `perms`, `perms_type`, `sort_no`, `always_show`, `icon`, `is_route`, `is_leaf`, `keep_alive`, `hidden`, `description`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `rule_flag`, `status`, `internal_or_external`) VALUES ('1373511790884855809', NULL, '绩效考核', '/assessment', 'layouts/RouteView', NULL, NULL, '0', NULL, '1', '5.00', '0', 'reconciliation', '1', '0', '0', '0', NULL, 'admin', '2021-03-21 13:48:24', NULL, NULL, '0', '0', '1', '0');

-- 字典
INSERT INTO `sys_dict` (`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('1373612603313508353', '绩效节点执行状态', 'hr_task_status', '0-未执行；1-已执行', '0', 'admin', '2021-03-21 20:29:00', 'admin', '2021-03-21 20:29:25', '0');
INSERT INTO `sys_dict` (`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('1373600748805304321', '绩效考核状态', 'hr_instance_status', '0-已立项;1-执行中；2-待审批；3-已审批', '0', 'admin', '2021-03-21 19:41:53', NULL, NULL, '0');
INSERT INTO `sys_dict` (`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('1373555975977476097', '绩效考核得分级别', 'hr_attendance_level', '1-优；2-良；3-中；4-差；', '0', 'admin', '2021-03-21 16:43:59', 'admin', '2021-03-21 16:44:52', '0');
INSERT INTO `sys_dict` (`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('1373514054953377794', '绩效考核方式', 'hr_assessment_type', '1-按得分；2-按排名', '0', 'admin', '2021-03-21 13:57:24', NULL, NULL, '0');

-- 字典项
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373514094895734786', '1373514054953377794', '按得分', '1', '', '1', '1', 'admin', '2021-03-21 13:57:34', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373514126084579329', '1373514054953377794', '按排名', '2', '', '1', '1', 'admin', '2021-03-21 13:57:41', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373556031480700929', '1373555975977476097', '优', '1', '', '1', '1', 'admin', '2021-03-21 16:44:12', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373556077416718337', '1373555975977476097', '良', '2', '', '1', '1', 'admin', '2021-03-21 16:44:23', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373556116281139202', '1373555975977476097', '中', '3', '', '1', '1', 'admin', '2021-03-21 16:44:32', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373556149290311681', '1373555975977476097', '差', '4', '', '1', '1', 'admin', '2021-03-21 16:44:40', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373600799426359297', '1373600748805304321', '已立项', '0', '', '1', '1', 'admin', '2021-03-21 19:42:06', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373600834121641985', '1373600748805304321', '执行中', '1', '', '1', '1', 'admin', '2021-03-21 19:42:14', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373600868959531010', '1373600748805304321', '待审批', '2', '', '1', '1', 'admin', '2021-03-21 19:42:22', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373600900651692034', '1373600748805304321', '已审批', '3', '', '1', '1', 'admin', '2021-03-21 19:42:30', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373612646187683842', '1373612603313508353', '未执行', '0', '', '1', '1', 'admin', '2021-03-21 20:29:10', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373612674721533953', '1373612603313508353', '已执行', '1', '', '1', '1', 'admin', '2021-03-21 20:29:17', NULL, NULL);
