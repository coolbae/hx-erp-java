package org.jeecg.community.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import org.jeecg.autopoi.poi.excel.annotation.Excel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 业主工商信息
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@ApiModel(value="xy_park_owner对象", description="业主信息表")
@Data
@TableName("xy_park_ownerbusiness")
public class XyParkOwnerbusiness implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "主键")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**业主编码*/
	@ApiModelProperty(value = "业主编码")
	private java.lang.String ownerCode;
	/**法人代表*/
	@ApiModelProperty(value = "法人代表")
	private java.lang.String legalPerson;
	/**经营范围*/
	@ApiModelProperty(value = "身份证号")
	private java.lang.String idCard;
	/**统一社会信用代码*/
	@ApiModelProperty(value = "统一社会信用代码")
	private java.lang.String socialCreditcode;
	/**注册资本(万元)*/
	@ApiModelProperty(value = "注册资本(万元)")
	private java.math.BigDecimal registeredCapital;
	/**注册地址*/
	@ApiModelProperty(value = "注册地址")
	private java.lang.String registeredAddress;
	/**注册日期*/
	@Excel(name = "注册日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "注册日期")
	private java.util.Date registeredDate;
	/**经营范围*/
	@ApiModelProperty(value = "经营范围")
	private java.lang.String businessScope;
}
