package org.jeecg.community.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import org.jeecg.autopoi.poi.excel.annotation.Excel;

import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 客户信息表
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@ApiModel(value="xy_park_customer对象", description="客户信息表")
@Data
@TableName("xy_park_customer")
public class XyParkCustomer implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**客户编码*/
	@Excel(name = "客户编码", width = 15)
    @ApiModelProperty(value = "客户编码")
    private java.lang.String customerCode;
	/**客户名称*/
	@Excel(name = "客户名称", width = 15)
    @ApiModelProperty(value = "客户名称")
    private java.lang.String customerName;
	/**所属行业*/
	@Excel(name = "所属行业", width = 15, dicCode = "park_customer_trade")
    @Dict(dicCode = "park_customer_trade")
    @ApiModelProperty(value = "所属行业")
    private java.lang.String customerTrade;
	/**跟进人*/
	@Excel(name = "跟进人", width = 15, dictTable = "sys_user", dicText = "realname", dicCode = "username")
    @Dict(dictTable = "sys_user", dicText = "realname", dicCode = "username")
    @ApiModelProperty(value = "跟进人")
    private java.lang.String followUp;
	/**跟进状态*/
	@Excel(name = "跟进状态", width = 15)
    @ApiModelProperty(value = "跟进状态")
    private java.lang.String customerStatus;
	/**来访项目*/
	@Excel(name = "来访项目", width = 15, dictTable = "xy_park_project", dicText = "project_name", dicCode = "project_code")
    @Dict(dictTable = "xy_park_project", dicText = "project_name", dicCode = "project_code")
    @ApiModelProperty(value = "来访项目")
    private java.lang.String project;
	/**首次来访日期*/
	@Excel(name = "首次来访日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "首次来访日期")
    private java.util.Date visitDate;
	/**客户来源*/
	@Excel(name = "客户来源", width = 15, dicCode = "park_customer_source")
    @Dict(dicCode = "park_customer_source")
    @ApiModelProperty(value = "客户来源")
    private java.lang.String customerSource;
	/**意向等级*/
	@Excel(name = "意向等级", width = 15, dicCode = "park_intention_level")
    @Dict(dicCode = "park_intention_level")
    @ApiModelProperty(value = "意向等级")
    private java.lang.String intentionLevel;
	/**成交几率*/
	@Excel(name = "成交几率", width = 15)
    @ApiModelProperty(value = "成交几率")
    private java.math.BigDecimal probability;
	/**客户需求*/
	@Excel(name = "客户需求", width = 15, dicCode = "park_customer_demand")
    @Dict(dicCode = "park_customer_demand")
    @ApiModelProperty(value = "客户需求")
    private java.lang.String customerDemand;
	/**装修需求*/
	@Excel(name = "装修需求", width = 15, dicCode = "park_renovation")
    @Dict(dicCode = "park_renovation")
    @ApiModelProperty(value = "装修需求")
    private java.lang.String renovation;
	/**需求面积自*/
	@Excel(name = "需求面积自", width = 15)
    @ApiModelProperty(value = "需求面积自")
    private java.math.BigDecimal demandAreafrom;
	/**需求面积到*/
	@Excel(name = "需求面积到", width = 15)
    @ApiModelProperty(value = "需求面积到")
    private java.math.BigDecimal demandAreato;
	/**客户预算从*/
	@Excel(name = "客户预算从", width = 15)
    @ApiModelProperty(value = "客户预算从")
    private java.math.BigDecimal budgetFrom;
	/**客户预算到*/
	@Excel(name = "客户预算到", width = 15)
    @ApiModelProperty(value = "客户预算到")
    private java.math.BigDecimal budgetTo;
}
