package org.jeecg.community.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 房源配属资产
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@ApiModel(value="xy_park_house对象", description="房源信息")
@Data
@TableName("xy_park_housecapital")
public class XyParkHousecapital implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "主键")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**房源号*/
	@ApiModelProperty(value = "房源号")
	private java.lang.String houseCode;
	/**资产名称*/
	@ApiModelProperty(value = "资产名称")
	private java.lang.String capitalName;
	/**资产厂家*/
	@ApiModelProperty(value = "资产厂家")
	private java.lang.String capitalSupply;
	/**资产型号*/
	@ApiModelProperty(value = "资产型号")
	private java.lang.String capitalModel;
	/**数量*/
	@ApiModelProperty(value = "数量")
	private java.lang.Integer capitalQty;
	/**单位*/
	@ApiModelProperty(value = "单位")
	private java.lang.String unit;
	/**单价*/
	@ApiModelProperty(value = "单价")
	private java.math.BigDecimal capitalPrice;
}
