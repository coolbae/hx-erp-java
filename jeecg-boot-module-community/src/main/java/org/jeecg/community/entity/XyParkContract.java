package org.jeecg.community.entity;

import java.util.Date;
import java.util.List;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;
import org.jeecg.modules.system.entity.SysUser;
import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xy_park_contract")
public class XyParkContract extends JeecgEntity {

	private static final long serialVersionUID = 1L;
	
	/**合同编号*/
	@Excel(name = "合同编号", width = 15)
    @ApiModelProperty(value = "合同编号")
	private String contractCode;
	
	/**合同类别*/
	@Excel(name = "合同类别", width = 15)
    @ApiModelProperty(value = "合同类别")
	private String contractType;
	
	/**账户*/
	@Excel(name = "账户", width = 15)
    @ApiModelProperty(value = "账户")
	private String account;
	
	/**合同日期*/
	@Excel(name = "合同日期", width = 15)
    @ApiModelProperty(value = "合同日期")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	private Date contractDate;
	
	/**跟进人*/
	@Excel(name = "跟进人", width = 15)
    @ApiModelProperty(value = "跟进人")
	private String bizUser;
	
	/**业主编码*/
	@Excel(name = "业主编码", width = 15)
    @ApiModelProperty(value = "业主编码")
	private String ownerCode;
	
	@Excel(name = "合同开始日期", width = 15)
    @ApiModelProperty(value = "合同开始日期")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	private Date startDate;
	
	@Excel(name = "合同结束日期", width = 15)
    @ApiModelProperty(value = "合同结束日期")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	private Date endDate;
	
	@Excel(name = "合同审批状态", width = 15)
    @ApiModelProperty(value = "合同审批状态")
	private String contractStatus;
	
	@TableField(exist = false)
	private List<XyParkContractFee> xyParkContractFees;
	
	@TableField(exist = false)
	private List<XyParkContractHouse> xyParkContractHouses;
	
	@TableField(exist = false)
	private XyParkOwner xyParkOwner;
	
	@TableField(exist = false)
	private SysUser bizUserEntity;
	
	@TableField(exist = false)
	private XyParkAccount xyParkAccount;
	
}
