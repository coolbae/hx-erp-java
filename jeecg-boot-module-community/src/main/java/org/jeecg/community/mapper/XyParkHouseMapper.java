package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkHouse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 房源信息
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Mapper
public interface XyParkHouseMapper extends BaseMapper<XyParkHouse> {

}
