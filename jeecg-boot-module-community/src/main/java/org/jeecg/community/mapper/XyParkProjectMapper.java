package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 园区项目信息
 * @Author: MxpIO
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Mapper
public interface XyParkProjectMapper extends BaseMapper<XyParkProject> {

}
