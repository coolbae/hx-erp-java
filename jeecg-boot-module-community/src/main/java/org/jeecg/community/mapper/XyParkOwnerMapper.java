package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkOwner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 业主信息表
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Mapper
public interface XyParkOwnerMapper extends BaseMapper<XyParkOwner> {

}
