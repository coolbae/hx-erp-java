package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkContract;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface XyParkContractMapper extends BaseMapper<XyParkContract> {

}
