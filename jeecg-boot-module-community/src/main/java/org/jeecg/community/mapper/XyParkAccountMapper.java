package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 账户管理
 * @Author: MxpIO
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Mapper
public interface XyParkAccountMapper extends BaseMapper<XyParkAccount> {

}
