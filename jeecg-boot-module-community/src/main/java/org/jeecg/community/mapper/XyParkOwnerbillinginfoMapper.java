package org.jeecg.community.mapper;

import java.util.List;
import org.jeecg.community.entity.XyParkOwnerbillinginfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 业主开票信息表
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Mapper
public interface XyParkOwnerbillinginfoMapper extends BaseMapper<XyParkOwnerbillinginfo> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<XyParkOwnerbillinginfo> selectByMainId(@Param("mainId") String mainId);
}
