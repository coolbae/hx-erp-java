package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkBuildling;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 楼宇信息
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Mapper
public interface XyParkBuildlingMapper extends BaseMapper<XyParkBuildling> {

}
