package org.jeecg.community.mapper;

import java.util.List;
import org.jeecg.community.entity.XyParkHousestay;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 房源住宿信息
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Mapper
public interface XyParkHousestayMapper extends BaseMapper<XyParkHousestay> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<XyParkHousestay> selectByMainId(@Param("mainId") String mainId);
}
