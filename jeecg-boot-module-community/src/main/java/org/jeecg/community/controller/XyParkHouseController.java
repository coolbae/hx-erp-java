package org.jeecg.community.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.autopoi.poi.excel.ExcelImportUtil;
import org.jeecg.autopoi.poi.excel.def.NormalExcelConstants;
import org.jeecg.autopoi.poi.excel.entity.ExportParams;
import org.jeecg.autopoi.poi.excel.entity.ImportParams;
import org.jeecg.autopoi.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.community.entity.XyParkHousecapital;
import org.jeecg.community.entity.XyParkHousestay;
import org.jeecg.community.entity.XyParkHouse;
import org.jeecg.community.vo.XyParkHousePage;
import org.jeecg.community.service.IXyParkHouseService;
import org.jeecg.community.service.IXyParkHousecapitalService;
import org.jeecg.community.service.IXyParkHousestayService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 房源信息
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Api(tags="园区物业-房源信息")
@RestController
@RequestMapping("/community/xyParkHouse")
@Slf4j
public class XyParkHouseController {
	@Autowired
	private IXyParkHouseService xyParkHouseService;
	@Autowired
	private IXyParkHousecapitalService xyParkHousecapitalService;
	@Autowired
	private IXyParkHousestayService xyParkHousestayService;
	
	/**
	 * 分页列表查询
	 *
	 * @param xyParkHouse
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "房源信息-分页列表查询")
	@ApiOperation(value="房源信息-分页列表查询", notes="房源信息-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(XyParkHouse xyParkHouse,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<XyParkHouse> queryWrapper = QueryGenerator.initQueryWrapper(xyParkHouse, req.getParameterMap());
		Page<XyParkHouse> page = new Page<XyParkHouse>(pageNo, pageSize);
		IPage<XyParkHouse> pageList = xyParkHouseService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param xyParkHousePage
	 * @return
	 */
	@AutoLog(value = "房源信息-添加")
	@ApiOperation(value="房源信息-添加", notes="房源信息-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody XyParkHousePage xyParkHousePage) {
		XyParkHouse xyParkHouse = new XyParkHouse();
		BeanUtils.copyProperties(xyParkHousePage, xyParkHouse);
		xyParkHouseService.saveMain(xyParkHouse, xyParkHousePage.getXyParkHousecapitalList(),xyParkHousePage.getXyParkHousestayList());
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param xyParkHousePage
	 * @return
	 */
	@AutoLog(value = "房源信息-编辑")
	@ApiOperation(value="房源信息-编辑", notes="房源信息-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody XyParkHousePage xyParkHousePage) {
		XyParkHouse xyParkHouse = new XyParkHouse();
		BeanUtils.copyProperties(xyParkHousePage, xyParkHouse);
		XyParkHouse xyParkHouseEntity = xyParkHouseService.getById(xyParkHouse.getId());
		if(xyParkHouseEntity==null) {
			return Result.error("未找到对应数据");
		}
		xyParkHouseService.updateMain(xyParkHouse, xyParkHousePage.getXyParkHousecapitalList(),xyParkHousePage.getXyParkHousestayList());
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "房源信息-通过id删除")
	@ApiOperation(value="房源信息-通过id删除", notes="房源信息-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		xyParkHouseService.delMain(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "房源信息-批量删除")
	@ApiOperation(value="房源信息-批量删除", notes="房源信息-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.xyParkHouseService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "房源信息-通过id查询")
	@ApiOperation(value="房源信息-通过id查询", notes="房源信息-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		XyParkHouse xyParkHouse = xyParkHouseService.getById(id);
		if(xyParkHouse==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(xyParkHouse);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "房源配属资产通过主表ID查询")
	@ApiOperation(value="房源配属资产主表ID查询", notes="房源配属资产-通主表ID查询")
	@GetMapping(value = "/queryXyParkHousecapitalByMainId")
	public Result<?> queryXyParkHousecapitalListByMainId(@RequestParam(name="id",required=true) String id) {
		List<XyParkHousecapital> xyParkHousecapitalList = xyParkHousecapitalService.selectByMainId(id);
		return Result.OK(xyParkHousecapitalList);
	}
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "房源住宿信息通过主表ID查询")
	@ApiOperation(value="房源住宿信息主表ID查询", notes="房源住宿信息-通主表ID查询")
	@GetMapping(value = "/queryXyParkHousestayByMainId")
	public Result<?> queryXyParkHousestayListByMainId(@RequestParam(name="id",required=true) String id) {
		List<XyParkHousestay> xyParkHousestayList = xyParkHousestayService.selectByMainId(id);
		return Result.OK(xyParkHousestayList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param xyParkHouse
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, XyParkHouse xyParkHouse) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<XyParkHouse> queryWrapper = QueryGenerator.initQueryWrapper(xyParkHouse, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<XyParkHouse> queryList = xyParkHouseService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<XyParkHouse> xyParkHouseList = new ArrayList<XyParkHouse>();
      if(oConvertUtils.isEmpty(selections)) {
          xyParkHouseList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          xyParkHouseList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<XyParkHousePage> pageList = new ArrayList<XyParkHousePage>();
      for (XyParkHouse main : xyParkHouseList) {
          XyParkHousePage vo = new XyParkHousePage();
          BeanUtils.copyProperties(main, vo);
          List<XyParkHousecapital> xyParkHousecapitalList = xyParkHousecapitalService.selectByMainId(main.getId());
          vo.setXyParkHousecapitalList(xyParkHousecapitalList);
          List<XyParkHousestay> xyParkHousestayList = xyParkHousestayService.selectByMainId(main.getId());
          vo.setXyParkHousestayList(xyParkHousestayList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "房源信息列表");
      mv.addObject(NormalExcelConstants.CLASS, XyParkHousePage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("房源信息数据", "导出人:"+sysUser.getRealname(), "房源信息"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<XyParkHousePage> list = ExcelImportUtil.importExcel(file.getInputStream(), XyParkHousePage.class, params);
              for (XyParkHousePage page : list) {
                  XyParkHouse po = new XyParkHouse();
                  BeanUtils.copyProperties(page, po);
                  xyParkHouseService.saveMain(po, page.getXyParkHousecapitalList(),page.getXyParkHousestayList());
              }
              return Result.OK("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.OK("文件导入失败！");
    }

}
