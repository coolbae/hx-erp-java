package org.jeecg.community.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.community.entity.XyParkAccount;
import org.jeecg.community.service.IXyParkAccountService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 账户管理
 * @Author: MxpIO
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Api(tags="园区物业-账户管理")
@RestController
@RequestMapping("/community/xyParkAccount")
public class XyParkAccountController extends JeecgController<XyParkAccount, IXyParkAccountService> {
	@Autowired
	private IXyParkAccountService xyParkAccountService;
	
	/**
	 * 分页列表查询
	 *
	 * @param xyParkAccount
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "账户管理-分页列表查询")
	@ApiOperation(value="账户管理-分页列表查询", notes="账户管理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(XyParkAccount xyParkAccount,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<XyParkAccount> queryWrapper = QueryGenerator.initQueryWrapper(xyParkAccount, req.getParameterMap());
		Page<XyParkAccount> page = new Page<XyParkAccount>(pageNo, pageSize);
		IPage<XyParkAccount> pageList = xyParkAccountService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param xyParkAccount
	 * @return
	 */
	@AutoLog(value = "账户管理-添加")
	@ApiOperation(value="账户管理-添加", notes="账户管理-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody XyParkAccount xyParkAccount) {
		xyParkAccountService.save(xyParkAccount);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param xyParkAccount
	 * @return
	 */
	@AutoLog(value = "账户管理-编辑")
	@ApiOperation(value="账户管理-编辑", notes="账户管理-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody XyParkAccount xyParkAccount) {
		xyParkAccountService.updateById(xyParkAccount);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "账户管理-通过id删除")
	@ApiOperation(value="账户管理-通过id删除", notes="账户管理-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		xyParkAccountService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "账户管理-批量删除")
	@ApiOperation(value="账户管理-批量删除", notes="账户管理-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.xyParkAccountService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "账户管理-通过id查询")
	@ApiOperation(value="账户管理-通过id查询", notes="账户管理-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		XyParkAccount xyParkAccount = xyParkAccountService.getById(id);
		if(xyParkAccount==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(xyParkAccount);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param xyParkAccount
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, XyParkAccount xyParkAccount) {
        return super.exportXls(request, xyParkAccount, XyParkAccount.class, "账户管理");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, XyParkAccount.class);
    }

}
