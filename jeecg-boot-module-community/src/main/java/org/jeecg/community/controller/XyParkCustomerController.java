package org.jeecg.community.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.autopoi.poi.excel.ExcelImportUtil;
import org.jeecg.autopoi.poi.excel.def.NormalExcelConstants;
import org.jeecg.autopoi.poi.excel.entity.ExportParams;
import org.jeecg.autopoi.poi.excel.entity.ImportParams;
import org.jeecg.autopoi.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.community.entity.XyParkCustomercontact;
import org.jeecg.community.entity.XyParkIntentionhouse;
import org.jeecg.community.entity.XyParkCustomer;
import org.jeecg.community.vo.XyParkCustomerPage;
import org.jeecg.community.service.IXyParkCustomerService;
import org.jeecg.community.service.IXyParkCustomercontactService;
import org.jeecg.community.service.IXyParkIntentionhouseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 客户信息表
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Api(tags="园区物业-客户信息")
@RestController
@RequestMapping("/community/xyParkCustomer")
@Slf4j
public class XyParkCustomerController {
	@Autowired
	private IXyParkCustomerService xyParkCustomerService;
	@Autowired
	private IXyParkCustomercontactService xyParkCustomercontactService;
	@Autowired
	private IXyParkIntentionhouseService xyParkIntentionhouseService;
	
	/**
	 * 分页列表查询
	 *
	 * @param xyParkCustomer
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "客户信息表-分页列表查询")
	@ApiOperation(value="客户信息表-分页列表查询", notes="客户信息表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(XyParkCustomer xyParkCustomer,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<XyParkCustomer> queryWrapper = QueryGenerator.initQueryWrapper(xyParkCustomer, req.getParameterMap());
		Page<XyParkCustomer> page = new Page<XyParkCustomer>(pageNo, pageSize);
		IPage<XyParkCustomer> pageList = xyParkCustomerService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param xyParkCustomerPage
	 * @return
	 */
	@AutoLog(value = "客户信息表-添加")
	@ApiOperation(value="客户信息表-添加", notes="客户信息表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody XyParkCustomerPage xyParkCustomerPage) {
		XyParkCustomer xyParkCustomer = new XyParkCustomer();
		BeanUtils.copyProperties(xyParkCustomerPage, xyParkCustomer);
		xyParkCustomerService.saveMain(xyParkCustomer, xyParkCustomerPage.getXyParkCustomercontactList(),xyParkCustomerPage.getXyParkIntentionhouseList());
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param xyParkCustomerPage
	 * @return
	 */
	@AutoLog(value = "客户信息表-编辑")
	@ApiOperation(value="客户信息表-编辑", notes="客户信息表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody XyParkCustomerPage xyParkCustomerPage) {
		XyParkCustomer xyParkCustomer = new XyParkCustomer();
		BeanUtils.copyProperties(xyParkCustomerPage, xyParkCustomer);
		XyParkCustomer xyParkCustomerEntity = xyParkCustomerService.getById(xyParkCustomer.getId());
		if(xyParkCustomerEntity==null) {
			return Result.error("未找到对应数据");
		}
		xyParkCustomerService.updateMain(xyParkCustomer, xyParkCustomerPage.getXyParkCustomercontactList(),xyParkCustomerPage.getXyParkIntentionhouseList());
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "客户信息表-通过id删除")
	@ApiOperation(value="客户信息表-通过id删除", notes="客户信息表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		xyParkCustomerService.delMain(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "客户信息表-批量删除")
	@ApiOperation(value="客户信息表-批量删除", notes="客户信息表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.xyParkCustomerService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "客户信息表-通过id查询")
	@ApiOperation(value="客户信息表-通过id查询", notes="客户信息表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		XyParkCustomer xyParkCustomer = xyParkCustomerService.getById(id);
		if(xyParkCustomer==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(xyParkCustomer);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "客户联系人通过主表ID查询")
	@ApiOperation(value="客户联系人主表ID查询", notes="客户联系人-通主表ID查询")
	@GetMapping(value = "/queryXyParkCustomercontactByMainId")
	public Result<?> queryXyParkCustomercontactListByMainId(@RequestParam(name="id",required=true) String id) {
		List<XyParkCustomercontact> xyParkCustomercontactList = xyParkCustomercontactService.selectByMainId(id);
		return Result.OK(xyParkCustomercontactList);
	}
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "客户意向房源通过主表ID查询")
	@ApiOperation(value="客户意向房源主表ID查询", notes="客户意向房源-通主表ID查询")
	@GetMapping(value = "/queryXyParkIntentionhouseByMainId")
	public Result<?> queryXyParkIntentionhouseListByMainId(@RequestParam(name="id",required=true) String id) {
		List<XyParkIntentionhouse> xyParkIntentionhouseList = xyParkIntentionhouseService.selectByMainId(id);
		return Result.OK(xyParkIntentionhouseList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param xyParkCustomer
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, XyParkCustomer xyParkCustomer) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<XyParkCustomer> queryWrapper = QueryGenerator.initQueryWrapper(xyParkCustomer, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<XyParkCustomer> queryList = xyParkCustomerService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<XyParkCustomer> xyParkCustomerList = new ArrayList<XyParkCustomer>();
      if(oConvertUtils.isEmpty(selections)) {
          xyParkCustomerList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          xyParkCustomerList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<XyParkCustomerPage> pageList = new ArrayList<XyParkCustomerPage>();
      for (XyParkCustomer main : xyParkCustomerList) {
          XyParkCustomerPage vo = new XyParkCustomerPage();
          BeanUtils.copyProperties(main, vo);
          List<XyParkCustomercontact> xyParkCustomercontactList = xyParkCustomercontactService.selectByMainId(main.getId());
          vo.setXyParkCustomercontactList(xyParkCustomercontactList);
          List<XyParkIntentionhouse> xyParkIntentionhouseList = xyParkIntentionhouseService.selectByMainId(main.getId());
          vo.setXyParkIntentionhouseList(xyParkIntentionhouseList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "客户信息表列表");
      mv.addObject(NormalExcelConstants.CLASS, XyParkCustomerPage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("客户信息表数据", "导出人:"+sysUser.getRealname(), "客户信息表"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<XyParkCustomerPage> list = ExcelImportUtil.importExcel(file.getInputStream(), XyParkCustomerPage.class, params);
              for (XyParkCustomerPage page : list) {
                  XyParkCustomer po = new XyParkCustomer();
                  BeanUtils.copyProperties(page, po);
                  xyParkCustomerService.saveMain(po, page.getXyParkCustomercontactList(),page.getXyParkIntentionhouseList());
              }
              return Result.OK("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.OK("文件导入失败！");
    }

}
