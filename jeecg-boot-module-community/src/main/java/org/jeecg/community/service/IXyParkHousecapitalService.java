package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkHousecapital;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 房源配属资产
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
public interface IXyParkHousecapitalService extends IService<XyParkHousecapital> {

	public List<XyParkHousecapital> selectByMainId(String mainId);
}
