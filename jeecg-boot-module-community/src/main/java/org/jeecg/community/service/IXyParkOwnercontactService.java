package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkOwnercontact;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 业主联系人信息
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
public interface IXyParkOwnercontactService extends IService<XyParkOwnercontact> {

	public List<XyParkOwnercontact> selectByMainId(String mainId);
}
