package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkOwnerbillinginfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 业主开票信息表
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
public interface IXyParkOwnerbillinginfoService extends IService<XyParkOwnerbillinginfo> {

	public List<XyParkOwnerbillinginfo> selectByMainId(String mainId);
}
