package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkOwnercontact;
import org.jeecg.community.entity.XyParkOwnerbusiness;
import org.jeecg.community.entity.XyParkOwnerbillinginfo;
import org.jeecg.community.entity.XyParkOwner;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 业主信息表
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
public interface IXyParkOwnerService extends IService<XyParkOwner> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(XyParkOwner xyParkOwner,List<XyParkOwnercontact> xyParkOwnercontactList,List<XyParkOwnerbusiness> xyParkOwnerbusinessList,List<XyParkOwnerbillinginfo> xyParkOwnerbillinginfoList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(XyParkOwner xyParkOwner,List<XyParkOwnercontact> xyParkOwnercontactList,List<XyParkOwnerbusiness> xyParkOwnerbusinessList,List<XyParkOwnerbillinginfo> xyParkOwnerbillinginfoList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
