package org.jeecg.community.service;

import java.util.List;

import org.jeecg.community.entity.XyParkContract;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

public interface IXyParkContractService extends IService<XyParkContract> {

	void handleDetails(IPage<XyParkContract> pageList);

	void approvalByIds(List<String> idList, String status);

}
