package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkHousestay;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 房源住宿信息
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
public interface IXyParkHousestayService extends IService<XyParkHousestay> {

	public List<XyParkHousestay> selectByMainId(String mainId);
}
