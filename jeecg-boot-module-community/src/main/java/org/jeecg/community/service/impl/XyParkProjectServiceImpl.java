package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkProject;
import org.jeecg.community.mapper.XyParkProjectMapper;
import org.jeecg.community.service.IXyParkProjectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 园区项目信息
 * @Author: MxpIO
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Service
public class XyParkProjectServiceImpl extends ServiceImpl<XyParkProjectMapper, XyParkProject> implements IXyParkProjectService {

}
