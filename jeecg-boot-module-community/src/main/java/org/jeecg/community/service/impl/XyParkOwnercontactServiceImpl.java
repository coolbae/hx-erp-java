package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkOwnercontact;
import org.jeecg.community.mapper.XyParkOwnercontactMapper;
import org.jeecg.community.service.IXyParkOwnercontactService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 业主联系人信息
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Service
public class XyParkOwnercontactServiceImpl extends ServiceImpl<XyParkOwnercontactMapper, XyParkOwnercontact> implements IXyParkOwnercontactService {
	
	@Autowired
	private XyParkOwnercontactMapper xyParkOwnercontactMapper;
	
	@Override
	public List<XyParkOwnercontact> selectByMainId(String mainId) {
		return xyParkOwnercontactMapper.selectByMainId(mainId);
	}
}
