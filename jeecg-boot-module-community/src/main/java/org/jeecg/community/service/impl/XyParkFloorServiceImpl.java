package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkFloor;
import org.jeecg.community.mapper.XyParkFloorMapper;
import org.jeecg.community.service.IXyParkFloorService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 楼层信息
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Service
public class XyParkFloorServiceImpl extends ServiceImpl<XyParkFloorMapper, XyParkFloor> implements IXyParkFloorService {
	
	@Autowired
	private XyParkFloorMapper xyParkFloorMapper;
	
	@Override
	public List<XyParkFloor> selectByMainId(String mainId) {
		return xyParkFloorMapper.selectByMainId(mainId);
	}
}
