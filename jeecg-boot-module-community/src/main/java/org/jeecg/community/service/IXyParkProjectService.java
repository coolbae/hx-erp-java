package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkProject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 园区项目信息
 * @Author: MxpIO
 * @Date:   2021-06-14
 * @Version: V1.0
 */
public interface IXyParkProjectService extends IService<XyParkProject> {

}
