package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkIntentionhouse;
import org.jeecg.community.mapper.XyParkIntentionhouseMapper;
import org.jeecg.community.service.IXyParkIntentionhouseService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 客户意向房源
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Service
public class XyParkIntentionhouseServiceImpl extends ServiceImpl<XyParkIntentionhouseMapper, XyParkIntentionhouse> implements IXyParkIntentionhouseService {
	
	@Autowired
	private XyParkIntentionhouseMapper xyParkIntentionhouseMapper;
	
	@Override
	public List<XyParkIntentionhouse> selectByMainId(String mainId) {
		return xyParkIntentionhouseMapper.selectByMainId(mainId);
	}
}
