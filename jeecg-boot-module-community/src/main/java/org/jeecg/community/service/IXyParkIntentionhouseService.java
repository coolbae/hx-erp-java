package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkIntentionhouse;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 客户意向房源
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
public interface IXyParkIntentionhouseService extends IService<XyParkIntentionhouse> {

	public List<XyParkIntentionhouse> selectByMainId(String mainId);
}
