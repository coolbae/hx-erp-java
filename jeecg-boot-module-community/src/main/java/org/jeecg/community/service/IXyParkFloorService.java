package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkFloor;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 楼层信息
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
public interface IXyParkFloorService extends IService<XyParkFloor> {

	public List<XyParkFloor> selectByMainId(String mainId);
}
