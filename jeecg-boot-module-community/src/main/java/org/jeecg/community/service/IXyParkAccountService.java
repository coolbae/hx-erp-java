package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 账户管理
 * @Author: MxpIO
 * @Date:   2021-06-14
 * @Version: V1.0
 */
public interface IXyParkAccountService extends IService<XyParkAccount> {

}
