package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkOwnerbusiness;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 业主工商信息
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
public interface IXyParkOwnerbusinessService extends IService<XyParkOwnerbusiness> {

	public List<XyParkOwnerbusiness> selectByMainId(String mainId);
}
