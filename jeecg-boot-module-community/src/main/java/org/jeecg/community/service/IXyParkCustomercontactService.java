package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkCustomercontact;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 客户联系人
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
public interface IXyParkCustomercontactService extends IService<XyParkCustomercontact> {

	public List<XyParkCustomercontact> selectByMainId(String mainId);
}
