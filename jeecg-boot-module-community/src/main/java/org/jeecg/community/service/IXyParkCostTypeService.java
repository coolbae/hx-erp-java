package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkCostType;

import com.baomidou.mybatisplus.extension.service.IService;

public interface IXyParkCostTypeService extends IService<XyParkCostType> {

}
