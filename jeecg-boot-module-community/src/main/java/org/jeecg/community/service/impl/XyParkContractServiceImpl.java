package org.jeecg.community.service.impl;

import java.util.List;

import org.jeecg.community.entity.XyParkAccount;
import org.jeecg.community.entity.XyParkContract;
import org.jeecg.community.entity.XyParkContractFee;
import org.jeecg.community.entity.XyParkContractHouse;
import org.jeecg.community.entity.XyParkOwner;
import org.jeecg.community.mapper.XyParkAccountMapper;
import org.jeecg.community.mapper.XyParkContractFeeMapper;
import org.jeecg.community.mapper.XyParkContractHouseMapper;
import org.jeecg.community.mapper.XyParkContractMapper;
import org.jeecg.community.mapper.XyParkOwnerMapper;
import org.jeecg.community.service.IXyParkContractService;
import org.jeecg.modules.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class XyParkContractServiceImpl extends ServiceImpl<XyParkContractMapper,XyParkContract> implements IXyParkContractService {
	
	@Autowired
	private XyParkContractFeeMapper xyParkContractFeeMapper;
	
	@Autowired
	private XyParkContractHouseMapper xyParkContractHouseMapper;
	
	@Autowired
	private XyParkContractMapper xyParkContractMapper;
	
	@Autowired
	private XyParkOwnerMapper xyParkOwnerMapper;
	
	@Autowired
	private ISysUserService sysUserService;
	
	@Autowired
	private XyParkAccountMapper xyParkAccountMapper;

	@Override
	public void handleDetails(IPage<XyParkContract> pageList) {
		for(XyParkContract contract : pageList.getRecords()){
			contract.setXyParkContractFees(xyParkContractFeeMapper.selectList(new QueryWrapper<XyParkContractFee>().eq("contract_code", contract.getContractCode())));
			contract.setXyParkContractHouses(xyParkContractHouseMapper.selectList(new QueryWrapper<XyParkContractHouse>().eq("contract_code", contract.getContractCode())));
			contract.setXyParkOwner(xyParkOwnerMapper.selectOne(new QueryWrapper<XyParkOwner>().eq("owner_code", contract.getOwnerCode())));
			contract.setBizUserEntity(sysUserService.getUserByName(contract.getBizUser()));
			contract.setXyParkAccount(xyParkAccountMapper.selectOne(new QueryWrapper<XyParkAccount>().eq("park_number", contract.getAccount())));
		}
	}

	@Override
	public void approvalByIds(List<String> idList, String status) {
		List<XyParkContract> list = xyParkContractMapper.selectBatchIds(idList);
		if(list != null){
			for(XyParkContract contract : list){
				contract.setContractStatus(status);
				xyParkContractMapper.updateById(contract);
			}
		}
	}

}
