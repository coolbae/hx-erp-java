package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkFloor;
import org.jeecg.community.entity.XyParkBuildling;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 楼宇信息
 * @Author: jeecg-boot
 * @Date:   2021-06-14
 * @Version: V1.0
 */
public interface IXyParkBuildlingService extends IService<XyParkBuildling> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(XyParkBuildling xyParkBuildling,List<XyParkFloor> xyParkFloorList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(XyParkBuildling xyParkBuildling,List<XyParkFloor> xyParkFloorList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
