package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkAccount;
import org.jeecg.community.mapper.XyParkAccountMapper;
import org.jeecg.community.service.IXyParkAccountService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 账户管理
 * @Author: MxpIO
 * @Date:   2021-06-14
 * @Version: V1.0
 */
@Service
public class XyParkAccountServiceImpl extends ServiceImpl<XyParkAccountMapper, XyParkAccount> implements IXyParkAccountService {

}
