package org.jeecg.pms.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.pms.entity.PmsTask;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface PmsTaskMapper extends BaseMapper<PmsTask> {

	/**
	 * 编辑节点状态
	 * @param id
	 * @param status
	 */
	void updateTreeNodeStatus(@Param("id") String id,@Param("status") String status);
}
