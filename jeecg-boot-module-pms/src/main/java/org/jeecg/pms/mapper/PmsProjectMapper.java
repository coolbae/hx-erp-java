package org.jeecg.pms.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.pms.entity.PmsProject;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface PmsProjectMapper extends BaseMapper<PmsProject> {

}
