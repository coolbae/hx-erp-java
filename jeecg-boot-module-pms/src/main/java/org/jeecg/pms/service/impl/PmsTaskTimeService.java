package org.jeecg.pms.service.impl;

import org.jeecg.pms.entity.PmsTaskTime;
import org.jeecg.pms.mapper.PmsTaskTimeMapper;
import org.jeecg.pms.service.IPmsTaskTimeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class PmsTaskTimeService extends ServiceImpl<PmsTaskTimeMapper, PmsTaskTime> implements IPmsTaskTimeService {


}
