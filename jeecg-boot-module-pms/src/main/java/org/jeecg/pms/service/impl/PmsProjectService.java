package org.jeecg.pms.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.jeecg.pms.entity.PmsProject;
import org.jeecg.pms.entity.PmsTask;
import org.jeecg.pms.mapper.PmsProjectMapper;
import org.jeecg.pms.mapper.PmsTaskMapper;
import org.jeecg.pms.service.IPmsProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class PmsProjectService extends ServiceImpl<PmsProjectMapper, PmsProject> implements IPmsProjectService {
	
	@Autowired
	private PmsTaskMapper pmsTaskMapper;

	@Override
	public void handleTransientVariable(List<PmsProject> records) {
		for(PmsProject project : records){
			QueryWrapper<PmsTask> qw = new QueryWrapper<>();
			qw.eq("project_code", project.getProjectCode());
			qw.eq("has_children", "0");
			BigDecimal total = new BigDecimal(pmsTaskMapper.selectCount(qw));
			qw.ge("task_status", "2");
			BigDecimal complete = new BigDecimal(pmsTaskMapper.selectCount(qw));
			if(total.compareTo(BigDecimal.ZERO) != 0){
				project.setProjectProgress(complete.divide(total));
			}else{
				project.setProjectProgress(BigDecimal.ZERO);
			}
		}
	}

}
