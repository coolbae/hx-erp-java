package org.jeecg.pms.service;

import java.util.List;

import org.jeecg.common.system.base.service.JeecgService;
import org.jeecg.pms.entity.PmsProject;

public interface IPmsProjectService extends JeecgService<PmsProject> {

	void handleTransientVariable(List<PmsProject> records);

}
