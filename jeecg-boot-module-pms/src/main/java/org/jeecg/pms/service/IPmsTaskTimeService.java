package org.jeecg.pms.service;

import org.jeecg.common.system.base.service.JeecgService;
import org.jeecg.pms.entity.PmsTaskTime;

public interface IPmsTaskTimeService extends JeecgService<PmsTaskTime> {

}
