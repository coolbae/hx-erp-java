package org.jeecg.pms.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecg.common.system.base.entity.JeecgEntity;
import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("pms_task")
public class PmsTask extends JeecgEntity {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "任务编号")
	@Excel(name="任务编号",width=15)
	private String taskCode;
	
	@ApiModelProperty(value = "任务名称")
	@Excel(name="任务名称",width=15)
	private String taskName;
	
	@ApiModelProperty(value = "父任务编号")
	private String parentTaskCode;
	
	@ApiModelProperty(value = "项目编号")
	@Excel(name="项目编号",width=15)
	private String projectCode;
	
	@ApiModelProperty(value = "前置任务编号")
	private String preTaskCode;
	
	@ApiModelProperty(value = "序号")
	private Integer serialNumber;
	
	@ApiModelProperty(value = "开始时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@Excel(name="开始时间",width=15,format = "yyyy-MM-dd")
	private Date startTime;
	
	@ApiModelProperty(value = "结束时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@Excel(name="结束时间",width=15,format = "yyyy-MM-dd")
	private Date endTime;
	
	@ApiModelProperty(value = "实际开始时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@Excel(name="实际开始时间",width=15,format = "yyyy-MM-dd")
	private Date actualStartTime;
	
	@ApiModelProperty(value = "实际结束时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@Excel(name="实际结束时间",width=15,format = "yyyy-MM-dd")
	private Date actualEndTime;
	
	@ApiModelProperty(value = "任务进度（%）")
	private BigDecimal taskProgress;
	
	@ApiModelProperty(value = "任务负责人")
	@Dict(dicCode = "username",dictTable="sys_user",dicText="realname")
	private String taskDirector;
	
	@ApiModelProperty(value = "任务审核人")
	@Dict(dicCode = "username",dictTable="sys_user",dicText="realname")
	private String taskReviewer;
	
	@ApiModelProperty(value = "任务抄送人")
	@Dict(dicCode = "username",dictTable="sys_user",dicText="realname")
	private String taskCclist;
	
	@ApiModelProperty(value = "计划工时")
	private BigDecimal planManHours;
	
	@ApiModelProperty(value = "实际工时")
	@TableField(exist = false)
	private BigDecimal actualManHours;
	
	@ApiModelProperty(value = "备注")
	private String remarks;
	
	@ApiModelProperty(value = "标签")
	private String taskTags;
	
	@ApiModelProperty(value = "文件路径")
	private String filePath;
	
	@ApiModelProperty(value = "任务描述")
	private String taskDesc;
	
	@ApiModelProperty(value = "任务状态")
	@Dict(dicCode = "pms_task_status")
	private String taskStatus;
	
	@ApiModelProperty(value = "任务级别")
	@Dict(dicCode = "pms_task_level")
	private String taskLevel;
	
	private String hasChildren;
	
	@TableField(exist = false)
	private List<PmsTask> children;

}
