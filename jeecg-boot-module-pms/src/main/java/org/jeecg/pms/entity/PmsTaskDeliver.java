package org.jeecg.pms.entity;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("pms_task_deliver")
public class PmsTaskDeliver extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "任务编号")
	@Excel(name="任务编号",width=15)
	private String taskCode;
	
	@ApiModelProperty(value = "成果描述")
	private String resultDesc;
	
	@ApiModelProperty(value = "文件路径")
	private String filePath;

}
