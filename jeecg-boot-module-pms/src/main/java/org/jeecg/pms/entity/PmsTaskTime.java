package org.jeecg.pms.entity;

import java.math.BigDecimal;
import java.util.Date;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;
import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("pms_task_time")
public class PmsTaskTime extends JeecgEntity {

	private static final long serialVersionUID = -1448188873223090291L;
	
	@ApiModelProperty(value = "任务编号")
	@Excel(name="任务编号",width=15)
	private String taskCode;
	
	@ApiModelProperty(value = "开始时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@Excel(name="开始时间",width=15,format = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	
	@ApiModelProperty(value = "工时")
	private BigDecimal manHours;
	
	@ApiModelProperty(value = "备注")
	private String remarks;
	
}
