package org.jeecg.pms.aspect.annotation;

import org.jeecg.pms.aspect.enums.ProjectLogType;

import java.lang.annotation.*;


@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PmsLog {

	/**
	 * 日志类型 默认为PROJECT
	 * @return
	 */
	ProjectLogType projectLogType() default ProjectLogType.PROJECT;
}
